<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers;
use \Enska\Interfaces\IModel;
use \Enska\Services\Utils;

class Model implements IModel
{
	/**
	 * Model is loaded
	 * @type	Bool
	 */
	Public		$isLoaded = false;
	
	/**
	 * Model store
	 * @type	DataStore
	 */
	Private		$_store = null;
	
	/**
	 * Model raw errors
	 * @type	DataStore
	 */
	Private		$_errors = null;

	/**
	 * Model latest error
	 * @type	string
	 */
	Private		$_error = null;

	/**
	 * Model latest error code
	 * @type	string
	 */
	Private		$_code = 200;
	
	/**
	 * Model's id
	 * @type	string
	 */
	Private		$_id = null;
	
	Public
		/**
		 * Object constructor
		 *
		 * @param	string $id = null
		 * @param	array $datas = null
		 * @return	Model
		 * @access	public
		 */
		Function __construct($id=null, $datas=null)
		{
			$this->__init($id, $datas);
		}
		
		/**
		 * Set the model id
		 *
		 * @param	string id = null
		 * @return	void
		 * @access	public
		 */
		Function setId($id=null)
		{
			$_id = (!Utils::isEmpty($id, Utils::_STRING_)) ? $id : Utils::createId(DEFAULT_ID_SIZE);
			$this->_id = $_id;
		}
		
		/**
		 * Get the model id
		 *
		 * @return	string
		 * @access	public
		 */
		Function getId()
		{
			return ($this->_id);
		}
		
		/**
		 * Apply object datas to object store
		 * 
		 * @param	mixed $datas = null
		 * @return	string
		 * @access	public
		 */
		Function apply($datas=null)
		{
			$this->_store->apply($datas, true);
		}

		/**
		 * Trigger a model error
		 *
		 * @param	mixed $data = null
		 * @param	int $code = 0
		 * @return	FALSE
		 * @access	public
		 */
		Function error($datas=null, $code=0)
		{
			$this->_error = $datas;
			$this->_errors->push($datas);
			$this->_code = ($code > 0) ? (int)$code : $this->_code;
			return (false);
		}

		/**
		 * Trigger a model error code
		 *
		 * @param	int $code = 0
		 * @return	void
		 * @access	public
		 */
		Function code($code=0)
		{
			$this->_code = (int)$code;
		}

		/**
		 * Get all errors triggered by child class
		 *
		 * @return	array
		 * @access	public
		 */
		Function getErrors($err=null)
		{
			return ($this->_errors->gets());
		}

		/**
		 * Get the last model error
		 *
		 * @return	mixed
		 * @access	public
		 */
		Function getLastError()
		{
			return ($this->_error);
		}

		/**
		 * Get the last model error code
		 *
		 * @return	mixed
		 * @access	public
		 */
		Function getCode()
		{
			return ((int)$this->_code);
		}

		/**
		 * Get the store instance
		 * 
		 * @return	DataStore
		 * @access	public
		 */
		Function store()
		{
			return ($this->_store);
		}
		
	Private
		/**
		 * Init the model
		 *
		 * @param	string $id = null
		 * @param	array $datas = null
		 * @return	void
		 * @access	private
		 */
		Function __init($id=null, $datas=null)
		{
			$this->_store = new DataStore();
			$this->_errors = new DataStore();
			$this->_error = null;
			$this->setId($id);
			$this->apply($datas);
		}
}

?>