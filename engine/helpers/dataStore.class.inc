<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Helper
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers;
use \Enska\Interfaces\IDataStore;
use \Enska\Services\Utils;
 
class DataStore implements IDataStore
{
	/**
	 * Main datas
	 * @type	array
	 */
	Private	$_datas = array();

	/**
	 * Store ID
	 * @type	string
	 */
	Private	$_id = null;
	
	/**
	 * Store error
	 * @type	string
	 */
	Public	$_error = null;
	
	/**
	 * DataStore instance
	 * @type	DataStore
	 */
	Private	static	$_instance = null;
	
	Public
		/**
		 * Constructor
		 *
		 * @param	array $datas = null
		 * @return	DataStore
		 * @access	public
		 */
		Function __construct($datas=null)
		{
			if (Utils::isArray($datas)) { $this->_format($datas); }
			elseif (!Utils::isEmpty($datas, Utils::_STRING_)) {
				$datas = @json_decode($datas, true);
				$this->error(\debug::jsonErr());
				if (Utils::isEmpty($this->getLastError()) && Utils::isArray($datas)) { $this->sets($datas, true); }
				else { $this->setId($datas); }
			}
			elseif ($datas instanceof DataStore) { $this->sets($datas->gets(), true); }
		}
		
		/**
		 * Magic alias for set
		 *
		 * @param	mixed $attr
		 * @param	mixed $value = null
		 * @return	DataStore
		 * @access	public
		 */
		Function __set($attr, $value=null)
		{
			return ($this->set($attr, $value));
		}

		/**
		 * Set / Edit a store item
		 *
		 * @param	mixed $attr
		 * @param	mixed $value = null
		 * @return	DataStore
		 * @access	public
		 */
		Function set($attr, $value=null)
		{
			if (Utils::isEmpty($attr)) { return ($this); }
			$this->_datas[$attr] = $value;
			return ($this);
		}
		
		/**
		 * Push an object in store
		 *
		 * @param	mixed $attr
		 * @return	DataStore
		 * @access	public
		 */
		Function push($attr)
		{
			if (Utils::isEmpty($attr)) { return ($this); }
			$this->_datas []= $attr;
			return ($this);
		}
		
		/**
		 * Set / Edit store items
		 *
		 * @param	array $datas
		 * @param	bool $clr = false
		 * @return	bool
		 * @access	public
		 */
		Function sets($datas, $clr=false)
		{
			if ($datas instanceof DataStore) { $datas = $datas->gets(); }
			if (Utils::isArray($datas)) {
				if ($clr === true) { $this->clear(); }
				$this->_format($datas);
				return (true);
			}
			return (false);
		}
		
		/**
		 * Set / Edit store items
		 *
		 * @param	array $datas
		 * @param	bool $clr = false
		 * @return	bool
		 * @access	public
		 */
		Function apply($datas, $clr=false)
		{
			return ($this->sets($datas, $clr));
		}
		
		/**
		 * Magic alias for get
		 *
		 * @param	mixed $attr
		 * @return	mixed
		 * @access	public
		 */
		Function __get($attr)
		{
			return ($this->get($attr));
		}

		/**
		 * Get a store item
		 *
		 * @param	mixed $attr
		 * @return	mixed
		 * @access	public
		 */
		Function get($attr)
		{
			if ((!Utils::isEmpty($attr, Utils::_STRING_)) || (!Utils::isEmpty($attr, Utils::_NUMERIC_)) || $attr === 0) {
				if (isset($this->_datas[$attr])) { return ($this->_datas[$attr]); }
			}
			return (null);
		}
		
		/**
		 * Get store items
		 *
		 * @param	bool $jsEncode = false
		 * @param	bool $first = true
		 * @return	array
		 * @access	public
		 */
		Function gets($jsEncode=false, $first=true)
		{
			$res = array();
			if (Utils::isArray($this->_datas)) {
				foreach ($this->_datas as $key => $value) { $res[$key] = ($value instanceof DataStore) ? $value->gets(false, false) : $value; }
			}
			if ($first === true && $jsEncode === true) { $res = json_encode($res); }
			return ($res);
		}
		
		
		/**
		 * Convert input to DataStore format
		 * 
		 * @param	mixed = null
		 * @return	DataStore
		 * @access	public
		 */
		static Function create($datas=null)
		{
			if (($datas instanceof DataStore)) { return ($datas); }
			if (Utils::isEmpty($datas)) { $datas = '{}'; }
			self::$_instance = new self();
			
			if (Utils::isArray($datas)) { self::$_instance->sets($datas, true); }
			elseif (!Utils::isEmpty($datas)) {
				$datas = @json_decode($datas, true);
				$res = \debug::jsonErr();
				if ($res) { self::$_instance->error($res); }
				self::$_instance->sets($datas);
			}
			
			return (self::$_instance);	
		}
		
		/**
		 * Add store items
		 *
		 * @param	array $datas
		 * @param	int	$indexStart
		 * @return	DataStore
		 * @access	public
		 */
		Function adds($datas, $indexStart=1)
		{
			if (Utils::isArray($datas)) {
				$i = $indexStart; $j = 0;
				$nb = count($datas);
				while ($j < $nb) { $this->_datas[$i] = $datas[$j]; $j++; $i++; }
			}
			return ($this);
		}
		
		/**
		 * Delete a store item
		 *
		 * @param	mixed $attr
		 * @return	void
		 * @access	public
		 */
		Function delete($attr)
		{
			if (!Utils::isEmpty($attr)) {
				$tmp = explode(',', $attr);
				if (Utils::isArray($tmp)) {
					foreach($tmp as $attribute) { $this->_unsetData(trim($attribute)); }
				}
				else { return ($this->_unsetData($attr)); }
			}
		}

		/**
		 * Clear store items
		 *
		 * @return	void
		 * @access	public
		 */
		Function clear()
		{
			if (is_array($this->_datas)) {
				foreach($this->_datas as $key => $value) { $this->_unsetData($key); }
			}
		}

		/**
		 * Get store length
		 *
		 * @return	int
		 * @access	public
		 */
		Function count()
		{
			if (is_array($this->_datas)) { return (count($this->_datas)); }
			return (0);
		}
		
		/**
		 * Foreach iterator with callback
		 * 
		 * @param	Function $__callback($key = null, $value = null)
		 * @param	mixed	$obj = null
		 * @return	void
		 * @access	public
		 */
		Function each($__callback, $obj=null)
		{
			$res = array();
			$recs = $this->gets();
			foreach($recs as $key => $value) { $res []= $__callback($key, $value, $obj); }
			return ($res);
		}
		
		/**
		 * Set the store id information
		 *
		 * @param	string $id
		 * @return	void
		 * @access	public
		 */
		Function setId($id)
		{
			$this->_id = $id;
		}
		
		/**
		 * Get the store ID information
		 *
		 * @return	string
		 * @access	public
		 */
		Function getId()
		{
			return ($this->_id);
		}
		
		/**
		 * Set an error
		 * 
		 * @param	string 	$err
		 * @return	void
		 * @access	public
		 */
		Function error($err)
		{
			$this->_error = $err;
		}
		
		/**
		 * Get the latest error
		 * 
		 * @return	string
		 * @access	public
		 */
		Function getLastError()
		{
			return ($this->_error);
		}
		
	Protected
		/**
		 * Unset item
		 *
		 * @param	mixed $attr;
		 * @return	void
		 * @access	protected
		 */
		Function _unsetData($attr)
		{
			unset($this->_datas[$attr]);
		}
		
	Private
		/**
		 * Add array datas on recursif schema
		 * 
		 * @param	$datas
		 * @return	void
		 * @access	private
		 */
		Function _format($datas)
		{
			if (Utils::isArray($datas)) {
				foreach ($datas as $key => $value) {
					if ((is_string($key) && Utils::isArray($value)) || (is_int($key) && Utils::isArray($value))) {
						if (!isset($this->_datas[$key])) { $this->_datas[$key] = new DataStore($value); }
						else {
							if ($this->_datas[$key] instanceof DataStore) { $this->_datas[$key]->apply($value); }
							else { $this->_datas[$key] = $value; }
						}
					}
					elseif ((is_string($key) && !Utils::isArray($value)) || (is_int($key) && !Utils::isArray($value))) {
						//if (is_string($value)) { $this->_datas[$key] = utf8_encode($value); }
						if (is_string($value)) { $this->_datas[$key] = $value; }
						else { $this->_datas[$key] = $value; }
					}
				}
			}
		}
}

?>