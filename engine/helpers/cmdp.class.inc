<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Helper
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Helpers;

use	Enska\Services\Context;
use	Enska\Services\Utils;

class cmdp extends Model
{
	/**
	 * Route
	 * @type	DataStore
	 */
	Public		$cmd = null;
	 
	Public
		/**
		 * Constructor
		 * 
		 * @param	array 	$id = null
		 * @param	array	$datas = null
		 * @return	cmdp
		 * @access	public
		 */
		Function __construct($datas=null)
		{
			parent::__construct(null, $datas);
			$this->parse();
		}
		
		/**
		 * Parse the current CMD buffer
		 * 
		 * @param	array	$cmd = null
		 * @return	void
		 * @access	public
		 */ 
	 	Function parse($cmd=null)
	 	{
	 		$cmd = (!$cmd && Context::enska('ARGVS')) ? Context::enska('ARGVS') : $cmd;
			$this->cmd = new DataStore();
			$this->cmd->flags = new DataStore();
			$this->cmd->vars = new DataStore();
			$this->cmd->options = new DataStore();
			$this->cmd->arguments = new DataStore();
			
			$i = -1; $nb = count($cmd);
			while($i < $nb){
				$i++;
				if ($i >= $nb) { break; }
				if ($this->_nextFlag($cmd[$i])) { continue; }
				if ($this->_nextEnv($cmd[$i])) { continue; }
				if ($this->_nextOption($cmd[$i])) { continue; }
				$this->_nextArgument($cmd[$i]);
			}
	 	}
		
	Protected
		/**
		 * Parse the next flag "-f" / "-flag"
		 *
		 * @param	string	$argv
		 * @return	bool
		 * @access	protected
		 */
		Function _nextFlag($argv)
		{
			if (!$this->_isFlag($argv)) { return (false); }
			
			$buff = substr($argv, - (strlen($argv) - 1));
			if (!Utils::isEmpty($buff, Utils::_STRING_)) {
				$this->cmd->flags->set($buff,'enable');
		 		return (true);
			}
			return (false);
		}
		
	Protected
		/**
		 * Parse the next env "--key=value"
		 *
		 * @param	string	$argv
		 * @return	bool
		 * @access	protected
		 */
		Function _nextEnv($argv)
		{
			if (!$this->_isEnv($argv)) { return (false); }
					
			$buff = substr($argv, - (strlen($argv) - 2));
			$l = explode('=', $buff);
			if (!Utils::isEmpty($l[0]) && !Utils::isEmpty($l[1])) {
				$key = $l[0];
				$value = $l[1];
				$this->cmd->vars->$key = $value;
				return (true);
			}
			return (false);
		}
		
	Protected
		/**
		 * Parse the next option "--option"
		 *
		 * @param	string	$argv
		 * @return	bool
		 * @access	protected
		 */
		Function _nextOption($argv)
		{
			if (!$this->_isOption($argv)) { return (false); }
			
			$buff = substr($argv, - (strlen($argv) - 2));
			if (!Utils::isEmpty($buff, Utils::_STRING_)) {
				$this->cmd->options->$buff = 'enable';
		 		return (true);
			}
			return (false);
		}
		
	Protected
		/**
		 * Parse the next argument "builder.php argv1 argv2 argvN"
		 *
		 * @param	string	$argv
		 * @return	bool
		 * @access	protected
		 */
		Function _nextArgument($argv)
		{
			if (!Utils::isEmpty($argv, Utils::_STRING_)) {
		 		$this->cmd->arguments->push($argv);
		 		return (true);
			}
			return (false);
		}
		
	Protected
		/**
		 * Check if is flag "-f" / "-flag"
		 *
		 * @param	string	$str
		 * @return	bool
		 * @access	protected
		 */
		Function _isFlag($str)
		{
			if (!strcmp(substr($str, 0, 1), '-') && strcmp(substr($str, 0, 2), '--')) { return (true); }
			return (false);
		}
	
	Protected	
		/**
		 * Check if is env "--key=value"
		 *
		 * @param	string	$str
		 * @return	bool
		 * @access	protected
		 */
		Function _isEnv($str)
		{
			if (!strcmp(substr($str, 0, 2), '--') && strpos($str, '=')) { return (true); }
			return (false);
		}
		
	Protected
		/**
		 * Check if is option "--option"
		 *
		 * @param	string	$str
		 * @return	bool
		 * @access	protected
		 */
		Function _isOption($str)
		{
			if (!strcmp(substr($str, 0, 2), '--') && !strpos($str, '=')) { return (true); }
			return (false);
		}
}

?>