<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Helper
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Helpers;

use	\Enska\Drivers\MysqlDriver;

class DBSearch extends MysqlDriver
{
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id
		 * @param	mixed 	$search
		 * @return	DBSearch
		 * @access	public
		 */ 
		Function __construct($id, $search)
		{
			parent::__construct($id, $search);
		}
		
		Function receive($filters=null)
		{
			debug('RECEIVE :: DBSearch');
		}
		
		
}
