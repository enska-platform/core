<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers;
use	\Enska\Services\Sessions;
use	\Enska\Services\Context;
use	\Enska\Services\Utils;
use	\Enska\Services\DB;
use \Enska\IO\JSON;

class Login extends Model
{
	Public
		/**
		 * Constructor
		 * 
		 * @param	array 	$id = null
		 * @param	array	$datas = null
		 * @return	Login
		 * @access	public
		 */
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
		}
		
		/**
		 * Login the user
		 * 
		 * @return	bool
		 * @access	public
		 */
		 Function login()
		 {
			if ((is_int((int)Sessions::getSession('uid'))) && ((int)Sessions::getSession('uid') != 4) && ((int)Sessions::getSession('uid') > 0)) {
				if (!strcmp(strtolower(trim(Sessions::getToken())), 'null')) { Sessions::setToken(''); return (false); }
				$req = DB::access(Context::enska('authDSN'))->select('users_tokens', array('fields' => array('uid'), 'where' => array('uid' => Sessions::getSession('uid'), 'AND' => array('token' => Sessions::getToken()))));
				if (is_object($req) && ($req->count() > 0)) { return (true); }
			}

			if (!Utils::isEmpty(Sessions::getToken())) {
				$req = DB::access(Context::enska('authDSN'))->select('users_tokens',array('fields' => array('uid'), 'where' => array('token' => Sessions::getToken(), 'AND' => array('uid' => Sessions::getSession('uid')))));
				if (is_object($req) && ($req->count() > 0)) {
					Sessions::setSession('uid', $req->uid);
					return (true);
				}
			}
			
			return (false);
		 }
		 
		/**
		 * Authenticate the user
		 * 
		 * @return	void
		 * @access	public
		 */
		 Function authenticate($login, $password)
		 {
		 	if ((!Utils::isEmpty($login, Utils::_STRING_)) && (!Utils::isEmpty($password, Utils::_STRING_))) {
				$auth = DB::access(Context::enska('authDSN'))->select('users_authentification', array('fields' => array('uid', 'status'), 'where' => array('password' => $this->_encodePassword($password), 'AND' => array('mail' => $login, 'OR' => array('login' => $login)))));
				if (is_object($auth) && $auth->count() > 0) {
					if (strcmp($auth->status, 'enable') && strcmp($auth->status, 'waiting')) { return (false); }
					
					$signupDate = null;
					$req = DB::access(Context::enska('authDSN'))->select('users_trace', array('fields' => array('signupDate'), 'where' => array('uid' => $auth->uid)));
					$signupDate = $req->signupDate;
					
					// TODO : Work with sign in ...
					//if (!strcmp($auth->status, 'waiting') && !$this->_isLess2Hour($signupDate)) { return (false); }
					
					Sessions::setSession('uid', $auth->uid);
					DB::access(Context::enska('authDSN'))->update('users_tokens', array('fields' => array('token' => Sessions::getToken()), 'where' => array('uid' => Sessions::getSession('uid'))));					
					DB::access(Context::enska('authDSN'))->update('users_trace', array('fields' => array('lastConnect' => date('Y-m-d H:i:s')), 'where' => array('uid' => $auth->uid)));
					return (true);
				}
			}

			return (false);
		 }
		 
		/**
		 * Disconnect the user
		 * 
		 * @return	void
		 * @access	public
		 */
		 Function disconnect()
		 {
		 	DB::access(Context::enska('authDSN'))->update('users_tokens', array('fields' => array('token' => '-1'), 'where' => array('uid' => Sessions::getSession('uid'), 'token' => Sessions::getToken())));
			Sessions::delToken();
			Sessions::destroySession();
		 }
		 
	 Private
		/**
		 * Hash string to the EnsKa password format
		 *
		 * @param	string $str
		 * @return	string
		 * @access	private
		 */
		Function _encodePassword($str)
		{
			return (Utils::revStrMD5($str));
		}
}

?>