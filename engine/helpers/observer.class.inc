<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers;

use \Enska\Abstracts\Observable;
use \Enska\Services\Utils;
 
class Observer extends Observable
{
	/**
	 * Subjects instance
	 * @type	DataStore
	 */
	Protected	$subjects = null;
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null;
		 * @param	array 	$datas = null;
		 * @return	Observable
		 * @access	public
		 */
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
			$this->subjects = new DataStore();
		}
		
		/**
		 * Accept connection from subject
		 *
		 * @param	&Observable	$subject
		 * @return	bool
		 * @access	public
		 */
		Function accept(&$subject)
		{
			if (!($subject instanceof Subject)) {
				return (false);
			}
			
			$subjects = $this->subjects->gets();
			if (Utils::isArray($subjects)) {
				foreach($subjects as $sub) {
					if (!strcmp($sub['id'], $subject->getId())) {
						return (false);
					}
				}
			}
			
			$ds = new DataStore();
			$ds->instance = $subject;
			$ds->id = $subject->getId();
			$this->subjects->set($subject->getId(), $ds);
			
			return (true);
		}
		
		/**
		 * Close a subject connexion
		 *
		 * @param	mixed &$subject
		 * @return	void
		 * @access	public
		 */
		Function close(&$subject)
		{
			$subjects = $this->subjects->gets();
			if (!Utils::isEmpty($subject, Utils::_STRING_)) {
				$subject = $this->subjects->$subject;
			}
			if (Utils::isArray($subjects)) {
				foreach($subjects as $subId => $obj) {
					if ($obj['instance']->getId() === $subject->getId()) {
						$this->subjects->delete($subject->getId());
					}
				}
			}
		}
		
		/**
		 * Remove and close connection with all subjects
		 *
		 * @return	void
		 * @access	public
		 */
		Function clear()
		{
			$subjects = $this->subjects->gets();
			if (Utils::isArray($subjects)) {
				foreach($subjects as $subId => $obj) {
					$obj['instance']->close();
				}
			}
		}
		
		/**
		 * Send events to subject
		 *
		 * @param	DataStore	$evt = null
		 * @return	void
		 * @access	public
		 */
		Function fire($evt = null)
		{
			if ($evt instanceof DataStore) {
				if (!Utils::isEmpty($evt->subject)) {
					$subject = $evt->subject;
					if ($this->subjects->$subject && $this->subjects->$subject->instance && method_exists($this->subjects->$subject->instance, 'event')) {
						$this->subjects->$subject->instance->event($evt);
					}
					return;
				}
				$this->broadcast($evt);
			}
		}
		
		/**
		 * Broadcast events
		 *
		 * @param	DataStore	$evt = null
		 * @return	void
		 * @access	public
		 */
		Function broadcast($evt)
		{
			$subjects = $this->subjects->gets();
			if (Utils::isArray($subjects)) {
				foreach($subjects as $subId => $obj) { $obj['instance']->event($evt); }
			}
		}
		
		/**
		 * Receive events from subject
		 *
		 * @param	DataStore	$evt = null
		 * @return	void
		 * @access	public
		 */
		Function event($evt=null)
		{
			try {
				$subId = $evt->id;
				$subMtd = $evt->method;
				$evt->instance = $this->subjects->subId->instance;
				if (method_exists($evt->instance, $subMtd)) {
					$instance->subMtd($evt->argv);
				}
			}
			catch(Exception $e){}
			$this->fire($evt);
		}
}

?>