<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers;

use Enska\Interfaces\IBootstrap;
use \Enska\Services\Kernel;
use \Enska\Services\Config;
use \Enska\Services\Context;
use \Enska\Services\Utils;
use \Enska\Services\Logs;
use \Enska\Services\System;
use \Enska\Services\Sessions;
use \Enska\Services\Auth;
use \Enska\Services\DB;
use \Enska\Services\Redis;
 
class Bootstrap implements IBootstrap
{
	/**
	 * Bootstrap instance
	 * @type	Bootstrap
	 * @access	private
	 */
	Private	static	$_instance = null;
	
	/**
	 * Bootstrap number of usable public instance
	 * @type	int
	 * @access	private
	 */
	Private	static	$_safe = 1;
	
		/**
		 * System start
		 *
		 * @return	Bootstrap
		 * @access	public
		 */
		static Function start()
		{
			Kernel::kernelTime();
			Kernel::kernelTime('core');
			return (self::access());
		}
		
		/**
		 * System terminate
		 *
		 * @param	bool	$exit = SUCCESS
		 * @return	void
		 * @access	public
		 */
		static Function terminate($exit=SUCCESS)
		{
			Kernel::terminate($exit);
		}
		
		/**
		 * Platform Exception
		 *
		 * @param	Exception $e
		 * @return	void
		 * @access	public
		 */
		static Function Excep($e)
		{
			Logs::exceptionsHandler($e);
			return (FAILED);
		}
		
		/**
		 * System load
		 *
		 * @return	Bootstrap
		 * @access	public
		 */
		Function load()
		{
			$envs = array('dev', 'int', 'prd');
			
			try {

				$file = ENSKA_CONF.'settings.json';
				if (!file_exists(DOCUMENTROOT.$file)) {
					$file = ENSKA_CONF.'@settings.json';
					if (!file_exists(DOCUMENTROOT.$file)) { throw new \Enska\Exceptions\CoreException('The settings file does not exist'); }
				}
				Config::load('settings', $file);
				if (($err = Config::get('settings')->getLastError())) { throw new \Enska\Exceptions\CoreException('Error with settings\' file :: '.$err); }
				
				if (Context::enska('HTTP_REQUEST')) { Context::set('uri', $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); }
				$env = (Config::get('settings') && Config::get('settings')->default && Config::get('settings')->default->env && in_array(Config::get('settings')->default->env, $envs)) ? Config::get('settings')->default->env : 'dev';
				if(Config::get('settings')->default && !Config::get('settings')->default->login) { Config::get('settings')->default->login = "api"; }
				
				Context::set('env', $env);
				Context::set('pool', Kernel::getPool());
				Context::set('app', Kernel::getApplication());
				Context::set('subdomain', Kernel::getSubdomain());
				Context::set('poolPath', $this->getCurrentPoolPath());
				Context::set('appPath', $this->getCurrentAppPath());
				Context::set('requestID', Utils::createId(DEFAULT_ID_SIZE));
				
				Logs::load();
				Logs::add('bootstrap');		
				Redis::load();
				DB::load();
				\Imports::load();
				System::load();
				Sessions::load();
				Auth::load();

				
				
				Kernel::kernelTime('__end');
				Context::set('kernelTime_core', Kernel::kernelTime('__get'));
				return (self::access());
			}
			catch(Exception $e) { throw new \Enska\Exceptions\BootstrapException('Error on loading core !', 1, $e); }
		}
		
		/**
		 * System powerOn
		 *
		 * @param	array 	$argv = null
		 * @return	void
		 * @access	public
		 */
		static Function powerOn($argv=null) {}
		
		/**
		 * System powerOff
		 *
		 * @return	void
		 * @access	public
		 */
		static Function powerOff() {}
		
		/**
		 * System process
		 *
		 * @return	void
		 * @access	public
		 */
		static Function run() {}
		
	Protected
		/**
		 * Get the current used pool path
		 * 
		 * @return	string
		 * @access	private
		 */
		Function getCurrentPoolPath()
		{
			if (Utils::isEmpty(Context::enska('pool'), Utils::_STRING_)) { throw new \Enska\Exceptions\CoreException('Unknow pool'); }
			
			$env = Context::enska('env');
			$current = (Config::get('settings')->default) ? Config::get('settings')->default->pools.'/'.Context::enska('pool').'/' : null;
			if (is_dir(DOCUMENTROOT.$current) && $current) { return ($current); }
			
			throw new \Enska\Exceptions\CoreException('The pools path does not exist');
		}
		
	Protected
		/**
		 * Get the current used application path
		 * 
		 * @return	string
		 * @access	private
		 */
		Function getCurrentAppPath()
		{
			if (Utils::isEmpty(Context::enska('pool'), Utils::_STRING_)) { throw new \Enska\Exceptions\CoreException('Unknow pool'); }
			if (Utils::isEmpty(Context::enska('app'), Utils::_STRING_)) { throw new \Enska\Exceptions\CoreException('Unknow application'); }
			
			$env = Context::enska('env');
			$current = (Config::get('settings')->default) ? Config::get('settings')->default->pools.'/'.Context::enska('pool').'/'.Context::enska('app').'/' : null;
			
			if (!is_dir(DOCUMENTROOT.$current) && $current) {
				if (Context::enska('CLI_REQUEST')) { throw new \Enska\Exceptions\CoreException('The module '.Context::enska('app').' seems does not exist'); }
				Context::set('app', Config::get('settings')->default->application);
				$current = (Config::get('settings')->default) ? Config::get('settings')->default->pools.'/'.Context::enska('pool').'/'.Context::enska('app').'/' : null;
				if (!is_dir(DOCUMENTROOT.$current)) { throw new \Enska\Exceptions\CoreException('The application path does not exist :: '.$current); }
				Kernel::setDefaultApp();
			}
			else { Kernel::setDefaultApp(); }

			return ($current);
		}
		
	Private
		/**
		 * Constructor
		 * 
		 * @return	Bootstrap
		 * @access	private
		 */
		Function __construct() {}
		
	Private
		/**
		 * Clone constructor
		 * 
		 * @return	Bootstrap
		 * @access	private
		 */
		Function __clone() {}
		
	Protected
		/**
		 * Access to the class instance
		 * 
		 * @return	Bootstrap
		 * @access	protected
		 */
		static Function access()
		{
			if(self::$_instance == null) { self::$_instance = new self(); }
			if ((int)self::$_safe > 0) {
				(int)self::$_safe--;
				return (self::$_instance);
			}
		}
		
		/**
		 * Access to the class instance
		 * 
		 * @return	Bootstrap
		 * @access	protected
		 */
		static Function _access()
		{
			if(self::$_instance == null) { self::$_instance = new self(); }
			return (self::$_instance);
		}
}

?>