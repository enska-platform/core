<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	DBORM
 * @package		engine.helpers.orm.mysql
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Helpers\ORM\mysql;
use \Enska\Services\Utils;

class mysqlTable extends manager {
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null;
		 * @param	array 	$datas = null;
		 * @return	mysqlTable
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
		}
		
		/**
		 * Do a CREATE action
		 * 
		 * @param	array	$datas
		 * @return	void
		 * @access	public
		 */
		Function create($datas)
		{
			$this->formatInput($datas);
			
			if (isset($this->input['db'])) {
				$config = isset($this->input['__CONFIG']) ? $this->input['__CONFIG'] : false;
				$custom = isset($config['custom']) ? $config['custom'] : false;
				$this->queries->push('USE '.'`'.$this->input['db'].'`;');
				if (Utils::isArray($this->input['tables'])) {
					foreach ($this->input['tables'] as $table => $content) {
						$this->table($table, $content);
					}
				}
				if (Utils::isArray($this->input['__CONFIG']['fks'])) {
					$this->applyFKS($this->input['__CONFIG']['fks']);
				}
			}
		}
		
		/**
		 * Create a new table
		 * 
		 * @param	string $table
		 * @param	array $content
		 * @return	void
		 * @access	private
		 */
		Function table($table, $content)
		{	
			if (Utils::isEmpty($table, Utils::_STRING_) || !Utils::isArray($content)) {
				return;
			}
			
			$req = 'CREATE TABLE IF NOT EXISTS `'.$table.'` ( ';
			
			foreach ($content as $column => $infos) {
				if ((strcmp($column, '__CONFIG')) && ($res = $this->createColumn($column, $infos))) {	
					$req .= $res.', ';
				}
			}
			
			$req = trim($req, ', ').', '.$this->_getPrimaries();
			$req = trim($req, ', ').' ) ';
			
			if (isset($content['__CONFIG'])) {
				if (isset($content['__CONFIG']['custom'])) {
					$req .= $this->_getCustom($content['__CONFIG']['custom']);
				}
			}
			$req = trim($req).';';
			$this->queries->push($req);
		}
		
		/**
		 * Do a query
		 * 
		 * @param	string	$query
		 * @return	void
		 * @access	public
		 */
		Function query($query)
		{
			$this->queries->push($query);
		}
		
		/**
		 * Do a query
		 * 
		 * @param	string	$table
		 * @return	void
		 * @access	public
		 */
		Function truncate($table)
		{
			$this->queries->push('TRUNCATE TABLE '.$table);
		}
		
		/**
		 * Do a SELECT action
		 * 
		 * @param	string	$tableName
		 * @param	array	$datas
		 * @return	void
		 * @access	public
		 */
		Function select($table_name, $datas=null)
		{
			$this->formatInput($datas);
			$this->_refactoInput();
			
			if (Utils::isArray($this->input)) {
				foreach($this->input as $line){
					$fields = (isset($line['fields'])) ? $this->fieldsParser($line['fields']) : '*';
					$distinct = (isset($line['distinct'])) ? 'DISTINCT ' : '';
					$where = (isset($line['where'])) ? $this->whereParser($line['where']) : '1';
					$options = (isset($line['options'])) ? $this->optionsParser($line['options']) : '';
					$join = (isset($line['join'])) ? $this->joinParser($line['join']) : '';
					$search = (isset($line['search'])) ? $this->searchParser($line['search']) : '';
					$where = (!Utils::isEmpty($search)) ? $search : $where;
					
					$req = (isset($line['join'])) ? 'SELECT '.$distinct.$fields.' FROM `'.$table_name.'` '.$join.' '.$options.';' : 'SELECT '.$distinct.$fields.' FROM `'.$table_name.'` WHERE '.$where.' '.$options.';';
					$this->queries->push($req);
				}
			}
		}
		
		/**
		 * Do an INSERT action
		 * 
		 * @param	string	$tableName
		 * @param	array	$datas
		 * @return	void
		 * @access	public
		 */
		Function insert($table_name, $datas)
		{
			$this->formatInput($datas);
			$this->_refactoInput();
			
			if (Utils::isArray($this->input)) {
				foreach($this->input as $line){
					$colsvals = $this->_getColsVals($line);
					$this->queries->push('INSERT INTO '.'`'.$table_name.'` ('.implode(', ',$colsvals['cols']).') VALUES ('.implode(', ',$colsvals['vals']).');');
				}
			}
		}
		
		/**
		 * Do an UPDATE action
		 * 
		 * @param	string	$tableName
		 * @param	array	$datas
		 * @return	void
		 * @access	public
		 */
		Function update($table_name, $datas)
		{
			$this->formatInput($datas);
			$this->_refactoInput();
			
			if (Utils::isArray($this->input)) {
				foreach($this->input as $line){
					$colsvals = $this->_getColsVals($line['fields']);
					$cpt = count($colsvals['cols']);
					$columns = array();
					for ($i = 0; $i < $cpt; $i++) {
					    $columns []= $colsvals['cols'][$i].'='.$colsvals['vals'][$i];
					}
					$where = (isset($line['where'])) ? $this->whereParser($line['where']) : '1';
					$this->queries->push('UPDATE '.'`'.$table_name.'` SET '.implode(', ', $columns).' WHERE '.$where.';');
				}
			}
		}
		
		/**
		 * Do a DELETE action
		 * 
		 * @param	string	$tableName
		 * @param	array	$datas
		 * @return	void
		 * @access	public
		 */
		Function delete($table_name, $datas)
		{
			$this->formatInput($datas);
			$this->_refactoInput();
			
			if (Utils::isArray($this->input)) {
				foreach($this->input as $line){
					$where = $this->whereParser($line);
					$this->queries->push('DELETE FROM '.'`'.$table_name.'` WHERE '.$where.';');
				}
			}
		}
		
	Protected
		/**
		 * Create a create column request
		 * 
		 * @param	string 	$column
		 * @param	array 	$infos
		 * @return	void
		 * @access	private
		 */
		Function createColumn($column, $infos)
		{
			$req = '';
			if (Utils::isArray($infos)) {
				if (isset($infos['primary'])) {
					$this->_keys []= array(manager::_PRIMARY => $column);
					unset($infos['primary']);
				}
				if (isset($infos['unique'])) {
					$this->_keys []= array(manager::_UNIQUE => $column);
					unset($infos['unique']);
				}
				if (isset($infos['index'])) {
					$this->_keys []= array(manager::_INDEX => $column);
					unset($infos['index']);
				}
				$req .= '`'.$column.'` '.$this->_getInline($infos);
			}
			return (trim($req));
		}
}

?>