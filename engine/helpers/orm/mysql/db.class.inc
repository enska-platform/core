<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	DBORM
 * @package		engine.helpers.orm.mysql
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Helpers\ORM\mysql;
use \Enska\Services\Utils;

class mysqlDb extends manager {
	
	/**
	 * table manager instance
	 * @type	mysql_table
	 */
	Private		$table = null;
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null;
		 * @param	array 	$datas = null;
		 * @return	mysqlDb
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
			
			if (!($this->table instanceof mysqlTable)) {
				$this->table = new mysqlTable();
			}
			
		}
		
		/**
		 * Do a CREATE action
		 * 
		 * @param	array	$datas
		 * @return	void
		 * @access	public
		 */
		Function create($datas)
		{
			$this->formatInput($datas);
			
			if (isset($this->input['db'])) {
				$config = isset($this->input['__CONFIG']) ? $this->input['__CONFIG'] : false;
				$custom = isset($config['custom']) ? $config['custom'] : false;
				if (isset($config['force']) && $config['force'] === true) {
					$this->drop($this->input['db']);
					$this->_create($this->input['db'], $custom);
				}
				else {
					$this->_create($this->input['db'], $custom);
				}
				$this->queries->push('USE '.'`'.$this->input['db'].'`;');
				if (Utils::isArray($this->input['tables'])) {
					foreach ($this->input['tables'] as $table => $content) {
						$this->table->table($table, $content);
					}
					$queries = $this->table->getQueries();
					foreach ($queries as $query) {
						$this->queries->push($query);
					}
					if (Utils::isArray($this->input['__CONFIG']['fks'])) {
						
						$this->applyFKS($this->input['__CONFIG']['fks']);
					}
				}
			}
		}
	
		/**
		 * Drop a DB
		 * 
		 * @param	string $db
		 * @return	void
		 * @access	public
		 */
		Function drop($db)
		{
			$this->queries->push('DROP DATABASE IF EXISTS '.'`'.$db.'`;');
		}
		
	Protected
		/**
		 * Create a new DB query
		 * 
		 * @param	string $db
		 * @param	array $custom = null
		 * @return	void
		 * @access	protected
		 */
		Function _create($db, $custom=null)
		{	
			$this->queries->push(trim('CREATE DATABASE IF NOT EXISTS '.'`'.$db.'` '.$this->_getCustom($custom).';'));
		}
}

?>