<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	DBORM
 * @package		engine.helpers.orm.mysql
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Helpers\ORM\mysql;
use \Enska\Helpers\Model;
use \Enska\Helpers\DataStore;
use \Enska\Services\Utils;

class manager extends Model {
	
	/**
	 * Keys contants
	 */
	 const	_PRIMARY = 'PRI';
	 const	_UNIQUE = 'UNI';
	 const	_INDEX = 'IND';
	
	/**
	 * Queries stack
	 * @type	DataStore
	 */
	Protected	$queries = null;
	
	/**
	 * Input buffer
	 * @type	array
	 */
	Protected 	$input = array();
	
	/**
	 * TMP table keys
	 * @type	array
	 */
	Protected 	$_keys = array();
	
	/**
	 * WHERE CLAUSE indicators
	 * @type	array
	 */
	Protected	$_operand = array('AND', 'OR');
	
	/**
	 * JOIN CLAUSE type
	 * @type	array
	 */
	Protected	$_joinners = array('INNER', 'LEFT', 'RIGHT');
	
	/**
	 * is in WHERE CLAUSE indicators
	 * @type	bool
	 */
	Protected	$_inOperand = false;
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null;
		 * @param	array 	$datas = null;
		 * @return	mysql_manager
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
			$this->queries = new DataStore();
		}
		
		/**
		 * Get queries stack
		 * 
		 * @return	array
		 * @access	public
		 */
		Function getQueries()
		{
			return ($this->queries->gets());
		}
		
		/**
		 * Clean queries stack
		 * 
		 * @return	array
		 * @access	public
		 */
		Function clean()
		{
			return ($this->queries->clear());
		}
		
	Protected
		/**
		 * Format the input in array mode
		 * 
		 * @param	mixed
		 * @return	void
		 * @access	protected
		 */
		Function formatInput($datas)
		{
			$this->input = DataStore::create($datas);
			$this->input = $this->input->gets();
		}
		
	Protected
		/**
		 * Format the input in array mode
		 * 
		 * @return	void
		 * @access	protected
		 */
		Function _refactoInput()
		{
			if (!isset($this->input[0])) {
				$tmp = $this->input;
				$this->input = array();
				$this->input[0] = $tmp;
			}
		}
		
	Protected
		/**
		 * Get the fields clause
		 * 
		 * @param	array 	$datas
		 * @return	string
		 * @access	protected
		 */
		Function fieldsParser($datas) {
			$res = null;
			if (Utils::isArray($datas)) {
				$fields = '';
				foreach($datas as $line) {
					$tmp = explode('~~', $line);
					if (isset($tmp[1])) {
						$as = $tmp[1];
						$tmp = explode('.', $tmp[0]);
						if (isset($tmp[1])) { $fields .= '`'.$tmp[0].'`.`'.$tmp[1].'` AS '.$as.', '; }
					}
					else {
						if (strstr($line, '@')) { $fields .= "'".$line."'"; }
						else {
							$tmp = explode('.', $line);
							$fields .= (isset($tmp[0]) && isset($tmp[1])) ? '`'.$tmp[0].'`.`'.$tmp[1].'`, ' : '`'.$line.'`, ';
						}

					}
				}
				$res = trim($fields, ', ');
			}
			return ($res);
		}
		
	Protected
		/**
		 * Get the option clause
		 * 
		 * @param	array 	$datas
		 * @return	string
		 * @access	protected
		 */
		Function optionsParser($datas) {
			$res = null;
			if (Utils::isArray($datas)) {
				foreach ($datas as $key => $value) {
					$res .= strtoupper($key).' '.$value.' ';
				}
				$res = trim($res);
			}
			return ($res);
		}
		
	Protected
		/**
		 * Get the join clause
		 * 
		 * @param	array 	$datas
		 * @return	string
		 * @access	protected
		 */
		Function joinParser($datas) {
			$res ='';
			foreach($datas as $line) {
				if (!isset($line['type']) || !in_array(strtoupper($line['type']), $this->_joinners)) {
					$line['type'] = 'inner';
				}
				$res .= strtoupper($line['type']).' JOIN `'.$line['table'].'` ON ';
				$res .= $this->whereParser($line['on']).' ';
			}
			return (trim($res));
		}
		
	Protected
		/**
		 * Get the where clause
		 * 
		 * @param	array 	$datas
		 * @return	string
		 * @access	protected
		 */
		Function whereParser($datas) {
			if (!Utils::isArray($datas) || Utils::isEmpty(($res = $this->_whereParser($datas)))) {
				$res = '1';
			}
			return ($res);
		}

	Protected
		/**
		 * Get the where clause (by search)
		 *
		 * @param	array 	$datas
		 * @return	string
		 * @access	protected
		 */
		Function searchParser($datas) {
			if (!Utils::isArray($datas) || Utils::isEmpty(($res = $this->_searchParser($datas)))) {
				$res = '1';
			}
			return ($res);
		}

	Private
		/**
		 * Get the where clause
		 * 
		 * @param	array 	$datas
		 * @return	string
		 * @access	protected
		 */
		Function _whereParser($datas) {
			$res ='';
			foreach($datas as $key => $value) {
				if (in_array($key, $this->_operand) && Utils::isArray($value)) {
					$res .= $key.' ('.$this->_whereParser($value).')';
				}
				else {
					if (Utils::isArray($value)) {
						$columns = $this->_getColsVals(array($key => $value['value']));
						$res .= $columns['cols'][0].$value['operator'].$columns['vals'][0].' ';
					}
					else {
						$columns = $this->_getColsVals(array($key => $value));
						$res .= $columns['cols'][0].'='.$columns['vals'][0].' ';
					}
				}
			}
			return (trim($res));
		}
		
	Protected
		/**
		 * Get the custom config
		 * 
		 * @param	array 	$custom
		 * @param	bool 	$trimed = false
		 * @return	string
		 * @access	protected
		 */
		Function _getCustom($custom, $trimed=false)
		{
			$req = '';
			if (Utils::isArray($custom)) {
				foreach ($custom as $key => $value) {
					$req .= $key.'='.$value.' ';
				}
				if ($trimed === true) {
					$req = trim($req);
				}
			}
			return ($req);
		}
		
	Protected
		/**
		 * Add primaries key to buffer
		 * 
		 * @return	string
		 * @access	protected
		 */
		Function _getPrimaries()
		{
			$req = '';
			if (Utils::isArray($this->_keys)) {
				foreach ($this->_keys as $key => $value) {
					if (isset($value[self::_PRIMARY])) {
						$req .= 'PRIMARY KEY (`'.$value[self::_PRIMARY].'`), ';
					}
					elseif (isset($value[self::_UNIQUE])) {
						$req .= 'UNIQUE KEY `'.$value[self::_UNIQUE].'` (`'.$value[self::_UNIQUE].'`), ';
					}
					elseif (isset($value[self::_INDEX])) {
						$req .= 'KEY `'.$value[self::_INDEX].'` (`'.$value[self::_INDEX].'`), ';
					}
				}
				$req = trim($req, ', ').' ';
			}
			$this->_keys = array();
			return ($req);
		}
		
	Protected
		/**
		 * Get the custom config
		 * 
		 * @param	array 	$datas
		 * @param	bool 	$trimed = false
		 * @return	string
		 * @access	protected
		 */
		Function _getInline($datas, $trimed=false)
		{
			$req = '';
			if (Utils::isArray($datas)) {
				foreach ($datas as $key => $value) {
					if (Utils::isPositive($value)) {
						if (!strcmp($key, 'DEFAULT')) {
							$req .= $key.' \''.$value.'\' ';
						}
						else {
							if (!strcmp($key, 'LONGTEXT') || !strcmp($key, 'DATE') || !strcmp($key, 'DATETIME')) {
								$req .= $key.' ';
							}
							else {
								$req .= $key.'('.$value.') '; 
							}
						}
					}
					elseif (!Utils::isEmpty($value, Utils::_STRING_)) {
						if (!strcmp($key, 'CHARACTER')) {
							$req .= 'CHARACTER SET '.$value.' ';
						}
						elseif (!strcmp($key, 'COLLATE')) {
							$req .= 'COLLATE '.$value.' ';
						}
						else {
							$req .= $key.' \''.$value.'\' ';
						}
					}
					elseif (Utils::isArray($value)) {
						$req .= $key.'(';
						foreach ($value as $enum) {
							$req .= '\''.$enum.'\', ';
						}
						$req = trim($req, ', ');
						$req .= ') ';
					}
					else {
						$req .=  $key.' ';
					}
				}
				if ($trimed === true) {
					$req = trim($req);
				}
			}
			return ($req);
		}
		
	Protected
		/**
		 * Get the columns and values array
		 * 
		 * @param	array 	$datas
		 * @return	array
		 * @access	protected
		 */
		Function _getColsVals($datas) {
			$cols = array(); $vals = array();
			if (Utils::isArray($datas)) {
				foreach ($datas as $key => $value) {
					$cols []= $this->fieldsParser(array($key));
					if (count(explode('.', $value)) == 2) {
						$vals []= $this->fieldsParser(array($value));
					}
					else {
						$vals []= (!is_int($value)) ? "'".$value."'" : $value;
					}
					
				}
			}
			$res['cols'] = $cols;
			$res['vals'] = $vals;
			return ($res);
		}
		
	Protected
		/**
		 * Apply the temp FKS to current buffer
		 * 
		 * @param	array 	$fks
		 * @return	void
		 * @access	protected
		 */
		Function applyFKS($fks)
		{
			if (Utils::isArray($fks)) {
				foreach ($fks as $key => $value) {
					$fk = new DataStore($value);
					$fk->from = $key;
					$from = explode('.', $key);
					$bind = explode('.', $fk->bind);
					if (!isset($from[0]) || !isset($from[1]) || !isset($bind[0]) || !isset($bind[1])) { continue; }
					$fk->set('fromTable', $from[0])->set('fromId', $from[1])->set('bindTable', $bind[0])->set('bindId', $bind[1]);
					$fk->onDelete = (!$fk->onDelete) ? 'NO ACTION': $fk->onDelete;
					$fk->onUpdate = (!$fk->onUpdate) ? 'NO ACTION': $fk->onUpdate;
					$this->queries->push('ALTER TABLE `'.$fk->fromTable.'`'.' ADD CONSTRAINT `'.$fk->from.'__fk__'.$fk->bind.'` FOREIGN KEY (`'.$fk->fromId.'`) REFERENCES `'.$fk->bindTable.'` (`'.$fk->bindId.'`) ON DELETE '.$fk->onDelete.' ON UPDATE '.$fk->onUpdate.';');
				}
			}
		}
}

?>