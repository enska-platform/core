<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers;

use	\Enska\Services\Context;
use	\Enska\Services\Config;
use	\Enska\Services\Utils;
 
class APP extends Model
{
	/**
	 * Application title
	 * @type	String
	 */
	Private	$title = null;
	
	/**
	 * Application variables
	 * @type	DataStore
	 */
	Private	$vars = null;
	
	/**
	 * Views queue
	 * @type	array
	 */
	Private	$views = array();
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	array 	$id = null
		 * @param	array	$datas = null
		 * @return	APP
		 * @access	public
		 */
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
			if ($id) { $this->title = $id;}
			$this->vars = new DataStore();
		}
		
		/**
		 * Set the application title
		 * 
		 * @param	string	$title = null;
		 * @return	void
		 * @access	public
		 */
		Function setTitle($title=null)
		{
			$this->title = $title;
		}
		
		/**
		 * Get the application title
		 * 
		 * @return	string
		 * @access	public
		 */
		Function getTitle()
		{
			return ($this->title);
		}
		
		/**
		 * Assign a pair (key/value) item
		 *
		 * @param	$key = null
		 * @param	$value = null
		 * @return	$this
		 * @access	public
		 */
		Function assign($key=null, $value=null)
		{
			if (!Utils::isEmpty($key) && !Utils::isEmpty($value)) { $this->vars->set($key, $value); }
			return ($this);
		}
		
		/**
		 * Delete a pair item
		 *
		 * @param	$key = null
		 * @return	void
		 * @access	public
		 */
		Function delete($key=null)
		{
			if (!Utils::isEmpty($key) && $this->vars->$key) { $this->vars->delete($key); }
		}
		
		/**
		 * Get a pair item
		 *
		 * @param	$key
		 * @return	mixed
		 * @access	public
		 */
		Function get($key)
		{
			return ($this->vars->$key);
		}
		
		/**
		 * Get all pairs items
		 *
		 * @return	DataStore
		 * @access	public
		 */
		Function gets()
		{
			return ($this->vars->gets());
		}
		
		/**
		 * Add item to view queue
		 *
		 * @param	string	$view
		 * @return	void
		 * @access	public
		 */
		Function add($view)
		{
			if (!Utils::isEmpty($view, Utils::_STRING_)) { $this->views []= $view; }
		}
		
		/**
		 * Display the view queue
		 *
		 * @param	string	$appName = null
		 * @return	void
		 * @access	public
		 */
		Function display($appName="app")
		{
			if (Utils::isArray($this->views)) {
				foreach ($this->views as $view) {
					$file = Context::enska('appPath').'views/'.$view.'.phtml';
					if (file_exists(DOCUMENTROOT.$file)) {
						if ($this->store()->client) { $this->store()->client->write(file_get_contents($file)); }
						else { include_once($file); }
					}
					else { /* ERROR VIEW NOT EXISTS */ }
				}
			}
			else {
				$file = ENSKA_ENGINE.'/views/app.phtml';
				if (file_exists(DOCUMENTROOT.$file)) {
					if (!Utils::isEmpty($appName)) { $this->assign('appName', $appName); }
					if ($this->store()->client) { $this->store()->client->write(file_get_contents($file)); }
					else { include_once($file); }
				}
				else { /* ERROR APP VIEW NOT EXISTS */ }
			}
			$this->views = array();
		}
		
		Function url()
		{
			$env = Context::enska('env');
			$url = Config::get('settings')->$env->resources.'/webapps/'.Context::enska('app').'/resources/';
			return ($url);
		}
}

?>