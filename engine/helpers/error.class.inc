<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Helpers;
use	\Enska\Services\Utils;
use	\Enska\Services\Logs;
 
final class Error extends Subject
{
	/**
	 * Last throwed error
	 * @type	int
	 */
	Private	$throw = E_USER_ERROR;
	
	/**
	 * Lastest error's message
	 * @type	string
	 */
	Private	$message = 'Unknown error';
	
	/**
	 * Latest error's code
	 * @type	string
	 */
	Private	$code = '000x00000';
	
	/**
	 * Latest error's file
	 * @type	string
	 */
	Private	$file = '-';
	
	/**
	 * Latest error's line
	 * @type	int
	 */
	Private	$line = '-';
	
	/**
	 * Latest error's parent
	 * @type	string
	 */
	Private	$parent = '-';
	
	/**
	 * Errors store
	 * @type	DataStore
	 */
	Private	$errors = null;
	
	Public
		/**
		 * Constructor
		 *
		 * @param	string $name
		 * @param	Observable $observer = null
		 * @return	Errors
		 * @access	public
		 */
		Function __construct($name, $observer=null)
		{
			parent::__construct($name, $observer);
			$this->setId($name);
			$this->errors = new DataStore();
		}
		
		/**
		 * Get the fired error
		 *
		 * @param	DataStore $evt = null
		 * @return	void
		 * @access	public
		 */
		Function event($evt=null)
		{
			$this->throw = ($evt->argv->throw) ? $evt->argv->throw : E_USER_NOTICE;
			$this->message = ($evt->argv->message) ? $evt->argv->message : 'Unknown error';
			$this->code = ($evt->argv->code) ? $evt->argv->code : '000x00000';
			$this->file = ($evt->argv->file) ? $evt->argv->file : '-';
			$this->line = ($evt->argv->line) ? $evt->argv->line : '-';
			$this->parent = ($evt->argv->parent) ? $evt->argv->parent : '-';
			
			$evt->argv->delete('parent');
			$this->setError($evt->argv);
		}

		/**
		 * Set an error
		 * 
		 * @param	mixed $err
		 * @return	void
		 * @access	public
		 */
		Function setError($err)
		{
			
			$error = new DataStore($err);
			$this->errors->set($this->errors->count() + 1, $error);
			Logs::errorsHandler($this->throw, $this->message, $this->file, $this->line, $this->code, $this->parent);
			
			if ($error->throw == E_USER_ERROR) {
				$this->trigger();
			}
		}
		
		/**
		 * Get all the errors
		 *
		 * @params	bool	$toString = null
		 * @params	string	$parent = null
		 * @return	mixed
		 * @access	public
		 */
		Function getErrors($toString=false, $parent=null)
		{
			if ($toString === false) {
				return ($this->_errors);
			}
			
			$error = '';
			$i = 1;
			
			if (Utils::isArray($this->_errors->gets())) {
				foreach($this->_errors->gets() as $err) {
					$throw = Logs::getErrType($err->get('throw'));
					if (!Utils::isEmpty($parent)) {
					 	if (strcmp($err->get('parent'), $parent) == 0) {
							$error .= '['.$this->_errors->getId().' ['.$i++.'][*'.$throw.'*]]'.$err->message.'**'.$err->code.'**'.$err->file.'**'.$err->line."\n";
					 	}
					}
					else {
						$error .= '['.$this->_errors->getId().' ['.$i++.'][*'.$throw.'*]]'.$err->message.'**'.$err->code.'**'.$err->file.'**'.$err->line."\n";
					}
				}
			}
			return ($error);
		}
		
		/**
		 * Get the lastest throwed error
		 *
		 * @param	int 	$nb = 0
		 * @return	mixed
		 * @access	public
		 */
		Function getLastError($nb=0)
		{
			if (Utils::isPositive($nb)) {
				return ($this->_errors->get($nb));
			}
			return ($this->_errors->get(count($this->_errors->gets())));
		}
		
		/**
		 * Throw all the errors store
		 * 
		 * @return	void
		 * @access	public
		 */
		 Function trigger()
		 {
			trigger_error($this->getErrors(true), E_USER_ERROR);
		 }
}

?>