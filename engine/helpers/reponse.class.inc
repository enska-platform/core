<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Helpers;

use \Enska\Services\Server;
use	\Enska\Services\Utils;
use	\Enska\IO\JSON;

class Reponse extends Model
{
	/**
	 * Authorized renderer
	 * @type	array
	 */
	 Private	$authRender = array('json', 'raw', 'error');
	 
	 /**
	  * Render shema
	  * @type	DataStore
	  */
	 Private	$schema = null;
	 
	 /**
	  * Stream visibility
	  * @type	bool
	  */
	 Private	$visibility = true;
	 
	 /**
	  * Header messages
	  * @type	array
	  */
	 Private	$headerMessages = array(
		"100"	=> "Continue",
		"101"	=> "Switching Protocols",
		"200"	=> "OK",
		"201"	=> "Created",
		"202"	=> "Accepted",
		"203"	=> "Non-Authoritative Information",
		"204"	=> "No Content",
		"205"	=> "Reset Content",
		"206"	=> "Partial Content",
		"300"	=> "Multiple Choices",
		"301"	=> "Moved Permanently",
		"302"	=> "Found",
		"303"	=> "See Other",
		"304"	=> "Not Modified",
		"305"	=> "Use Proxy",
		"306"	=> "(Unused)",
		"307"	=> "Temporary Redirect",
		"400"	=> "Bad Request",
		"401"	=> "Unauthorized",
		"402"	=> "Payment Required",
		"403"	=> "Forbidden",
		"404"	=> "Not Found",
		"405"	=> "Method Not Allowed",
		"406"	=> "Not Acceptable",
		"407"	=> "Proxy Authentication Required",
		"408"	=> "Request Timeout",
		"409"	=> "Conflict",
		"410"	=> "Gone",
		"411"	=> "Length Required",
		"412"	=> "Precondition Failed",
		"413"	=> "Request Entity Too Large",
		"414"	=> "Request-URI Too Long",
		"415"	=> "Unsupported Media Type",
		"416"	=> "Requested Range Not Satisfiable",
		"417"	=> "Expectation Failed",
		"500"	=> "Internal Server Error",
		"501"	=> "Not Implemented",
		"502"	=> "Bad Gateway",
		"503"	=> "Service Unavailable",
		"504"	=> "Gateway Timeout",
		"505"	=> "HTTP Version Not Supported"
	 );
	 
	Public
		/**
		 * Object constructor
		 *
		 * @param	mixed	$id = null
		 * @param	mixed	$datas = null
		 * @return	Reponse
		 * @access	public
		 */
		Function __construct($id=null, $datas=null)
		{
			$id = ($id) ? $id : 'REQUEST_RESPONSE';
			parent::__construct($id, $datas);
			$this->initSchema();
		}
		
		/**
		 * Set the reponse
		 * 
		 * @param	mixed	$datas = null
		 * @param	string	$render = 'json';
		 * @return	void
		 * @access	public
		 */
		 Function write($datas=null, $render='json')
		 {
		 	if (in_array($render, $this->authRender) && method_exists($this, '_'.$render)) {
		 		$render = '_'.$render;
		 		$this->$render($datas);
		 	}
			else { /* TO LOGS */ }
		 }
		 
		/**
		 * Set the reponse error
		 * 
		 * @param	mixed	$err = null
		 * @return	void
		 * @access	public
		 */
		 Function setError($err=null)
		 {
		 	$this->schema->error = $err;
		 }
		 
		/**
		 * Set the reponse visibility
		 * 
		 * @param	mixed	$visibility = true
		 * @return	void
		 * @access	public
		 */
		 Function setVisible($visibility=true)
		 {
		 	$this->visibility = $visibility;
		 }
		 
		/**
		 * Push datas into the stream
		 * 
		 * @param	mixed	$datas = null
		 * @return	void
		 * @access	public
		 */
		 Function push($datas=null)
		 {
			if (is_string($datas) && (strlen(trim($datas)) > 0)) { /*echo 'DATA'; debug($datas);*/ $this->schema->meta->raw = $datas; return; }
			$datas = DataStore::create($datas);
			//@header("Content-Type:"."application/json");
			
			if (!Utils::isEmpty($datas->meta) || !Utils::isEmpty($datas->records) || !Utils::isEmpty($datas->error)) {
				if ($datas->meta instanceof DataStore) { $this->schema->meta->sets($datas->meta->gets()); }
				if ($datas->records instanceof DataStore) { $this->schema->records->sets($datas->records->gets()); }
				$this->schema->error = $datas->error;
			}
			elseif ($datas->count() > 0) { $this->schema->sets($datas->gets()); }
			if ($datas && $datas->headerCode) { http_response_code($datas->headerCode); }
		 }
		 
	Private
	 	/**
		 * JSON render
		 * 
		 * @param	mixed	$datas = null
		 * @return	void
		 * @access	private
		 */
		Function _json($datas=null)
		{
			$this->push($datas);
			$this->jsonRender();
		}
		
	Private
	 	/**
		 * Error render
		 * 
		 * @param	mixed	$datas = null
		 * @return	void
		 * @access	private
		 */
		Function _error($datas=null)
		{
			$this->schema->error = $datas;
			$this->jsonRender();
		}
		
	Private
	 	/**
		 * RAW render
		 * 
		 * @param	mixed	$datas = null
		 * @return	void
		 * @access	private
		 */
		Function _raw($datas=null)
		{
			return ($this->_write($datas));
		}
		
	Private
	 	/**
		 * JSON errors render
		 * 
		 * @param	mixed	$datas = null
		 * @return	void
		 * @access	private
		 */
		Function jsonError($datas=null)
		{
			$this->schema->error = $datas;
			$this->jsonRender();
		}
		
	Private
	 	/**
		 * JSON errors render
		 * 
		 * @param	mixed	$datas = null
		 * @return	void
		 * @access	private
		 */
		Function jsonRender()
		{
			$stream = null;
			if (!($this->schema->meta instanceof DataStore) || $this->schema->meta->count() == 0) { $this->schema->delete('meta'); }
			if (!($this->schema->records instanceof DataStore) || $this->schema->records->count() == 0) { $this->schema->delete('records'); }
			if (!($this->schema->error)) { $this->schema->delete('error'); }
			if ($this->schema->headerCode) { $this->schema->delete('headerCode'); }
			if ($this->schema && $this->schema->meta && $this->schema->meta->raw) { $stream = $this->schema->meta->raw; }
			else { $stream = $this->schema->gets(true); }
			return ($this->_write($stream));
		}
	
	Private
		/**
		 * Writer
		 * 
		 * @return	mixed
		 * @access	private
		 */
		Function _write($datas)
		{
			if ($this->visibility) {
				if ($this->store()->client) { Server::write($this->store()->client, $datas); }
				else { echo $datas; }
				return;
			}
			return ($datas);
		}
		
	Private
	 	/**
		 * Init JSON schema
		 * 
		 * @return	void
		 * @access	private
		 */
		Function initSchema()
		{
			$this->schema = new DataStore();
			$this->schema->sets(array(
				'error'		=> null,
				'records'	=> new DataStore(),
				'meta'		=> new DataStore()
			));
		}
		  
}

?>