<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Helpers;
 
class Handler extends Model
{	
	/**
	 * Subject intance
	 * @type	{subjectInstance}
	 */
	Protected	$instance = null;
	
	Public
		/**
		 * Object constructor
		 *
		 * @param	string	$class
		 * @param	string	$id = null
		 * @param	mixed	$context = null
		 * @param	array 	$handler = null
		 * @return	Handler
		 * @access	public
		 */
		Function __construct($class, $id=null, $context=null, $handler=null)
		{
			parent::__construct($id, array('class' => $class, 'handler' => new DataStore($handler), 'context' => $context));
			$_class = new ReflectionClass($class);
			$this->instance = $_class->newInstance($id);
		}
		
		/**
		 * Magic class caller
		 * 
		 * @param	string	$method
		 * @param	array 	$argv = null
		 * @return	void
		 * @access	public
		 */
		Function __call($method, $argv=null)
		{
			if (($this->instance instanceof DataStore) && method_exists($this->instance, $method)) {
				$events = $this->store()->handler;
				$this->store()->method = $method;
				if ($events->$method) {
					$e = $events->$method;
					$argv = $this->__callback($e, 'before', $argv);
					Event::event($this->store(), $argv, 'before');
				}
				
				
				Event::event($this->store(), $argv, 'launch');
				$argv = $this->instance->$method($argv);
				Event::event($this->store(), $argv, 'terminate');
				
				if ($events->$method) {
					$e = $events->$method;
					$argv = $this->__callback($e, 'after', $argv);
					Event::event($this->store(), $argv, 'after');
				}
			}
		}
		
		/**
		 * Magic class callbacker
		 * 
		 * @param	array 	$evt
		 * @param	string	$_switch = 'before'
		 * @param	array 	$argv = null
		 * @return	void
		 * @access	public
		 */
		Function __callback($evt, $_switch='before', $argv=null)
		{
			if (isset($evt[$_switch])) {
				if (Utils::isArray($evt[$_switch])) {
					$nb = count($evt[$_switch]);
					$i = 0;
					while ($i < $nb) { $evt[$_switch][$i++]($argv); }
				}
				else {
					$evt[$_switch]($argv);
				}
			}
		}
}

?>