<?php

/**
 *Copyright (C) 2015 Appcooker.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers.protocol
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers\Protocols;
 
use	\Enska\Abstracts\Protocol;
 
class ERTP extends Protocol
{
	/**
	 * Client is Handshaked
	 * @var	bool
	 */
	Public	$isHandshaked = true;
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	array 	$id = null
		 * @param	array	$datas = null
		 * @return	ERTP
		 * @access	public
		 */
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
		}
		
	/************/
	/* ABSTRACT */
	/************/
	
		/**
		 * Do the socket protocol handshake
		 * 
		 * @param	Socket	$socket
		 * @param	String	$buffer
		 * @return	bool
		 * @access	public
		 */
		Function doHandshake($socket, $buffer) { $this->isHandshaked = true; return (true); }
	
		/**
		 * Encode object for socket outgoing transmition
		 * 
		 * @param	mixed	$payload
		 * @return	string
		 */
		Function encode($payload) { return ($payload); }
		
		/**
		 * Decode object for socket incoming transmition
		 * 
	  	 * @param	mixed	$payload
		 * @return	string
		 * @access	public
		 */
		Function decode($payload) { return ($payload); }
}

?>