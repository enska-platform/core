<?php

/**
 *Copyright (C) 2015 Appcooker.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers.protocols
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers\Protocols;
 
use	\Enska\Abstracts\Protocol;
use	\Enska\Services\Utils;
use	\Enska\Services\Context;
 
class WS extends Protocol
{
	/**
	 * Client is Handshaked
	 * @var	bool
	 */
	Public	$isHandshaked = false;
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	array 	$id = null
		 * @param	array	$datas = null
		 * @return	WS
		 * @access	public
		 */
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
		}
		
	/************/
	/* ABSTRACT */
	/************/
	
		/**
		 * Do the websocket protocol handshake
		 * 
		 * @param	Socket	$socket
		 * @param	String	$buffer
		 * @return	bool
		 * @access	public
		 */
		Function doHandshake($socket, $buffer)
		{
			if(preg_match("/Sec-WebSocket-Version: (.*)\r\n/", $buffer, $match)) { $version = $match[1]; }
		    if(isset($version) && $version == 13) {
		        if(preg_match("/GET (.*) HTTP/", $buffer, $match)) { $root = $match[1]; }
		        if(preg_match("/Host: (.*)\r\n/", $buffer, $match)) { $host = $match[1]; }
		        if(preg_match("/Origin: (.*)\r\n/", $buffer, $match)) { $origin = $match[1]; }
		        if(preg_match("/Sec-WebSocket-Key: (.*)\r\n/", $buffer, $match)) { $key = $match[1]; }
				$userOrigin = ((Context::enska('rt') instanceof DataStore) && Context::enska('rt')->origin) ? Context::enska('rt')->origin : '*';
				if (!Utils::isEmpty($userOrigin, Utils::_STRING_) && strcmp($userOrigin, '*')) {
					if (strcmp($origin, $userOrigin)) { $this->error('Bad client origin !'); return (false); }
				}
				$rfcString = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'; // Defined in RFC6455 / RFC4648
				$acceptKey = $key.$rfcString;
		        $acceptKey = base64_encode(sha1($acceptKey, true));
		        $upgrade = "HTTP/1.1 101 Switching Protocols\r\n" . "Upgrade: websocket\r\n" . "Connection: Upgrade\r\n" . "Sec-WebSocket-Accept: $acceptKey\r\n" . "Server: RTEd/1.1" . "\r\n\r\n";
				socket_write($socket, $upgrade, strlen($upgrade));
		        $this->isHandshaked = true;
				return (true);
		    }
		    $this->error('The client WEBSockets\' version is not supported by Enska\'s RealTimeEngineDaemon (v13 required)');
			return (false);
		}
	
		/**
		 * Encode object for websocket outgoing transmition
		 * 
		 * @param	mixed	$payload
		 * @return	string
		 */
		Function encode($payload)
		{
			$frameHead = array();
			$frame = '';
			$payloadLength = strlen($payload);
			$frameHead[0] = 129;
			if($payloadLength > 65535) {
				$payloadLengthBin = str_split(sprintf('%064b', $payloadLength), 8);
				$frameHead[1] = 127;
				for($i = 0; $i < 8; $i++) { $frameHead[$i+2] = bindec($payloadLengthBin[$i]); }	
				if($frameHead[2] > 127) { return null; }
			}
			elseif($payloadLength > 125) {
				$payloadLengthBin = str_split(sprintf('%016b', $payloadLength), 8);
				$frameHead[1] = 126;
				$frameHead[2] = bindec($payloadLengthBin[0]);
				$frameHead[3] = bindec($payloadLengthBin[1]);
			}
			else { $frameHead[1] = $payloadLength; }
			foreach(array_keys($frameHead) as $i) { $frameHead[$i] = chr($frameHead[$i]); }
			$frame = implode('', $frameHead);
			for($i = 0; $i < $payloadLength; $i++) { $frame .= $payload[$i]; }
			return ($frame);
		}
		
		/**
		 * Decode object for websocket incoming transmition
		 * 
	  	 * @param	mixed	$payload
		 * @return	string
		 * @access	public
		 */
		Function decode($payload)
		{
			$length = ord($payload[1]) & 127; $str = '';
			$masks = ($length == 126) ? substr($payload, 4, 4) : (($length == 127) ? substr($payload, 10, 4) : substr($payload, 2, 4));
			$data = ($length == 126) ? substr($payload, 8) : (($length == 127) ? substr($payload, 14) : substr($payload, 6));
			for ($i = 0; $i < strlen($data); ++$i) { $str .= $data[$i] ^ $masks[$i%4]; }
			return ($str);
		}
}

?>