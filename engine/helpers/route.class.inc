<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Helpers;

use	\Enska\Services\Utils;
use	\Enska\Services\Context;
use	\Enska\Services\System;
use	\Enska\IO\JSON;

class Route extends Model
{
	/**
	 * Route
	 * @type	DataStore
	 */
	 Public		$route = null;
	 
	/**
	 * Requested URI
	 * @type	string
	 */
	 Protected	$uri = null;
	 
	Public
		/**
		 * Constructor
		 * 
		 * @param	array 	$id = null
		 * @param	array	$datas = null
		 * @return	Route
		 * @access	public
		 */
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
			$this->route = new DataStore();
			
		}
		
		/**
		 * Get the requested URI
		 * 
		 * @param	string	$uri
		 * @return	void
		 * @access	public
		 */ 
	 	Function route($uri=null)
	 	{
	 		if (Utils::isEmpty(($uri = trim($uri, '/')), Utils::_STRING_)) {
	 			$uri = ($this->store()->uri) ? $this->store()->uri : Context::enska('uri');
			}
			$tmp = explode('/', $uri);
			unset($tmp[0]);
			$uri = implode('/', $tmp);
			
			$this->uri = trim($uri, '/');
			
			$this->route->sets(array('method' => 'Main', 'argv' => $this->_formatArgv($tmp), 'acl' => null), true);
			
			if (Utils::isArray(($loginRoute = $this->_getLoginRoute()))) {
				$this->route->sets($loginRoute, true);
			}
			elseif (Utils::isArray(($customRoute = $this->_getCustomRoute()))) {
				$this->route->sets($customRoute, true);
			}
			elseif (Utils::isArray(($CRUDRoute = $this->_getCRUDRoute()))) {
				$this->route->sets($CRUDRoute, true);
			}
			elseif (Utils::isArray(($route = $this->_getRoute()))) {
				$this->route->sets($route, true);
			}
	 	}
		
		/**
		 * Get sanitized $_ variables
		 * 
		 * @return	array
		 * @access	public
		 */ 
	 	Function sanitize()
	 	{
	 		if ($this->route->class) {
				$this->route->class = Utils::filter($this->route->class, array(Utils::_HTML_ENT_QUOTE, Utils::_HTML_SPE));
			}
			
			if ($this->route->method) {
				$this->route->method = Utils::filter($this->route->method, array(Utils::_HTML_ENT_QUOTE, Utils::_HTML_SPE));
			}
			
			if ($this->route->object) {
				$this->route->object = Utils::filter($this->route->object, array(Utils::_HTML_ENT_QUOTE, Utils::_HTML_SPE));
			}
			
			if ($this->route->id) {
				$this->route->id = Utils::filter($this->route->id, array(Utils::_HTML_ENT_QUOTE, Utils::_HTML_SPE));
			}
			
			if ($this->route->acl) {
				$this->route->acl = Utils::filter($this->route->acl, array(Utils::_HTML_ENT_QUOTE, Utils::_HTML_SPE));
			}
			
			$post = $get = $request = $files = array();
			if (Utils::isArray($_GET)){
				foreach ($_GET as $key => $value) {
					$key = Utils::filter($key, array(Utils::_HTML_ENT_QUOTE, Utils::_HTML_SPE));
					$value = Utils::filter($value, array(Utils::_HTML_ENT_QUOTE, Utils::_HTML_SPE));
					$get[$key] = $value;
				}
			}
			
			if (Utils::isArray($_POST)){
				foreach ($_POST as $key => $value) {
					$key = Utils::filter($key, array(Utils::_HTML_ENT_QUOTE, Utils::_HTML_SPE));
					$value = Utils::filter($value, array(Utils::_HTML_ENT_QUOTE, Utils::_HTML_SPE));
					$post[$key] = $value;
				}
			}
			
			if (Utils::isArray($_REQUEST)){
				foreach ($_REQUEST as $key => $value) {
					$key = Utils::filter($key, array(Utils::_HTML_ENT_QUOTE, Utils::_HTML_SPE));
					$value = Utils::filter($value, array(Utils::_HTML_ENT_QUOTE, Utils::_HTML_SPE));
					$request[$key] = $value;
				}
			}
			
			unset($_GET); unset($_POST); unset($_REQUEST);
			$this->route->post = new DataStore($post);
			$this->route->get = new DataStore($get);
			$this->route->request = new DataStore($request);
			$this->route->callMethod = $_SERVER[REQUEST_METHOD];
			$this->route->login = false;
			$this->route->logout = false;
			
			if (!strcmp($this->route->class, 'Login')) {
				$this->route->login = (!strcmp($this->route->method, 'authenticate')) ? true : false;
				$this->route->logout = (!strcmp($this->route->method, 'disconnect')) ? true : false;
				$this->route->islogged = (!strcmp($this->route->method, 'check')) ? true : false;
			}
			
			if (!trim($this->route->method)) {
				$this->route->method = 'Main';
			}
	 	}
		
	Private
		/**
		 * Get login route
		 * 
		 * @return	array
		 * @access	private
		 */
		Function _getLoginRoute()
		{
			$res = null;
			$nb = count(($tmp = explode('/', $this->uri)));
			
			if (isset($tmp[0])) {
				$act = $tmp[0];
			}
			
			if (!strcmp($act, 'login')) {
				$res = array('class' => 'Login', 'method' => 'authenticate');
			}
			elseif (!strcmp($act, 'logout')) {
				$res = array('class' => 'Login', 'method' => 'disconnect');
			}
			elseif (!strcmp($act, 'islogged')) {
				$res = array('class' => 'Login', 'method' => 'check');
			}
			
			return ($res);
		}
		
	Private
		/**
		 * Get custom route
		 * 
		 * @return	array
		 * @access	private
		 */
		Function _getCustomRoute()
		{
			$res = null;
			$routes = new JSON(Context::enska('poolPath')."routes.json", array('connector' => 'FS'));
			
			if (Utils::isArray(($routes = json_decode($routes->receive(), true)))) {
				foreach ($routes as $route => $infos) {
					if (!strcmp($this->uri, $route)) {
						if (isset($infos['subdomain']) && strcmp($infos['subdomain'], Context::enska('subdomain'))) {
							continue;
						}
						$res = $infos;
					}
				}
			}
			return ($res);
		}
		
	Private
		/**
		 * Get CRUD route
		 * 
		 * @return	array
		 * @access	private
		 */
		Function _getCRUDRoute()
		{
			$res = $obj = $id = $chk = null;
			$crudifier = array('select', 'update', 'delete', 'insert');
			$nb = count(($tmp = explode('/', $this->uri)));
			
			if (isset($tmp[0]) && isset($tmp[1]) && in_array(strtolower($tmp[0]), $crudifier) && System::isDefaultApp()) {
				$chk = $tmp[0];
				$obj = $tmp[1];
				if (isset($tmp[2])) {
					$id = $tmp[2];
				}
				unset($tmp[0], $tmp[1], $tmp[2]);
			}
			else if (isset($tmp[1]) && isset($tmp[2]) && in_array(strtolower($tmp[1]), $crudifier) && !strcmp(Context::enska('app'), $tmp[0])) {
				$chk = $tmp[1];
				$obj = $tmp[2];
				if (isset($tmp[3])) {
					$id = $tmp[3];
				}
				unset($tmp[0], $tmp[1], $tmp[2], $tmp[3]);
			}
			if (in_array(strtolower($chk), $crudifier)) {
				if (isset($tmp[$nb - 1]) && !strncmp($tmp[$nb - 1], '?', 1)) {
					unset($tmp[$nb - 1]);
				}
				$res = array('class' => 'crud', 'method' => $chk, 'object' => $obj, 'id' => $id, 'argv' => $this->_formatArgv($tmp), 'acl' => $chk.'_'.$obj);
			}
			return ($res);
		}
		
	Private
		/**
		 * Get route
		 * 
		 * @return	array
		 * @access	private
		 */
		Function _getRoute()
		{
			$res = $class = $method = $acl = null;
			$nb = count(($tmp = explode('/', $this->uri)));
			if (!strncmp($tmp[$nb - 1], '?', 1)) {
				unset($tmp[$nb - 1]);	
			}
			$nb = count($tmp);
			
			if ($nb >= 2 || $nb >= 3) {
				if ((System::isDefaultApp() || $tmp[0] === Context::enska('app')) && isset($tmp[2])) {
					$class = $tmp[1];
					$method = $tmp[2];
					unset($tmp[2]);
				}
				else {
					$class = $tmp[0];
					$method = $tmp[1];
				}
				unset($tmp[0], $tmp[1]);
				
				$res = array('class' => $class, 'method' => $method, 'argv' => $this->_formatArgv($tmp), 'acl' => Context::enska('app').'_'.$class.'_'.$method);
			}
			elseif ($nb == 1 && System::isDefaultApp()) {
				$res = array('method' => $tmp[0], 'acl' => Context::enska('app').'_'.$tmp[0]);
			}
			return ($res);
		}
		
	Private
		/**
		 * Format ARGV
		 * 
		 * @param	array	$datas
		 * @return	array
		 * @access	private
		 */
		Function _formatArgv($datas)
		{
			$res = new DataStore();
			if (Utils::isArray($datas)) {
				foreach ($datas as $data) {
					$res->push(Utils::filter($data, array(Utils::_HTML_ENT_QUOTE, Utils::_HTML_SPE)));
				}
			}
			return ($res->gets());
		}
}

?>