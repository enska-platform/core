<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers;

use \Enska\Abstracts\Observable;
 
class Subject extends Observable
{
	/**
	 * Observer instance
	 * @type	Observable
	 */
	Private	$observer = null;
	
	/**
	 * Last event received
	 * @type	DataStore
	 */
	Private	$lastEvent = null;
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	array 	$id = null
		 * @param	array	$datas = null
		 * @return	Observable
		 * @access	public
		 */
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
			if ($this->store()->observer) {
				$observer = $this->store()->observer;
				$this->connect($observer);
			}
		}
		
		/**
		 * Connect to the observer
		 *
		 * @param	&Observable	$observer
		 * @return	void
		 * @access	public
		 */
		Function connect(&$observer)
		{
			$this->observer = null;
			$instance = $this->instance();
			if ($observer->accept($instance)) {
				$this->observer = &$observer;
			}
		}
		
		/**
		 * Disconnect from the observer
		 *
		 * @return	void
		 * @access	public
		 */
		Function disconnect()
		{
			$this->observer->close($this);
			$this->observer = null;
		}
		
		/**
		 * Send event to observer
		 *
		 * @param	DataStore	$evt = null
		 * @return	bool
		 * @access	public
		 */
		Function fire($evt=null)
		{
			try { $this->observer->event($evt); return (true); }
			catch(Exception $e) { return (false); }
		}
		
		/**
		 * Receive events from observer
		 *
		 * @param	DataStore	$evt = null
		 * @return	void
		 * @access	public
		 */
		Function event($evt=null)
		{
			$evt = DataStore::create($evt);
			if ($evt->api) {
				$mtd = '_'.$evt->api;
				if (method_exists($this, $mtd)) { $this->$mtd($evt); return; }
			}
			// TODO : LOG API FAILURES
		}
}

?>