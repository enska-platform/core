<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers;

use \Enska\Services\Server;

final class Client extends Subject
{
	Protected	$protocol = 'ERTP';
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	array 	$id = null
		 * @param	array	$data = null
		 * @return	Client
		 * @access	public
		 */
		Function __construct($id=null, $data=null)
		{
			$data = new DataStore($data);
			$data->protocol = ($data->protocol) ? $data->protocol : 'ERTP';
			$this->protocol = $data->protocol;
			parent::__construct($id, $data);
			$this->loadProtocol();
		}
		
		/**
		 * Destructor
		 * 
		 * @return	void
		 * @access	public
		 */
		Function __desctruct() {}
		
	Private
		/**
		 * Write to the socket client
		 * 
		 * @param	string	$data = null
		 * @param	bool	printRaw = false
		 * @return	void
		 * @access	public
		 */
		Function write($data=null, $printRaw=false)
		{
			if ($printRaw !== true) { $data = $this->protocol->encode($data); }
			socket_write($this->store()->socket, $data, strlen($data));
		}
		
	Private
		/**
		 * Load the client protocol
		 * 
		 * @return	void
		 * @access	public
		 */
		Function loadProtocol()
		{
			try{ $p = 'Enska\\Helpers\\Protocols\\'.$this->protocol; $this->protocol = new $p(); }
			catch(Exception $e){ $this->error('Client :: loadProtocol() :: '.$e->getMessage()); return; }
		}
		
	Protected
		/**
		 * Prepare to writing
		 * 
		 * @param	DataStore	$datas = null
		 * @return	void
		 * @access	public
		 */
		Function _write($datas=null)
		{
			if ($datas->socket === $this->store()->socket) {
				$this->write($datas->buffer);
			}
		}
		
	Protected
		/**
		 * Handle request
		 * 
		 * @param	DataStore	$datas = null
		 * @return	void
		 * @access	public
		 */
		Function _handler($datas=null)
		{
			if ($datas->socket === $this->store()->socket) {
				if (($this->protocol instanceof \Enska\Abstracts\Protocol) && !$this->protocol->isHandshaked) {
					if (!$this->protocol->doHandshake($this->store()->socket, $datas->buffer)) { $this->write($this->getLastError(), true); }
				}
				else { Server::handler($this->store()->socket, $this->protocol->decode($datas->buffer), true); }
			}
		}
		
	Protected
		/**
		 * Disconnect the client
		 * 
		 * @param	DataStore	$datas = null
		 * @return	void
		 * @access	public
		 */
		Function _disconnect($datas=null)
		{
			if ($datas->socket === $this->store()->socket) {
				Server::disconnect($datas->socket, $this->getId());
			}
			else if (!$datas->socket) { Server::disconnect($this->store()->socket, $this->getId()); }
		}
	
	Protected
		/**
		 * Broadcast a message
		 * 
		 * @param	DataStore	$datas = null
		 * @return	void
		 * @access	public
		 */
		Function _broadcast($datas=null)
		{
			$this->write($datas->buffer);
		}
}

?>