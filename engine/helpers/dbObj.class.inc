<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers
 * @deprecated	/!\important/!\
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers;
use \Enska\Services\DB;
use \Enska\Services\Utils;

class DBObj extends Subject
{
	/**
	 * Object store
	 * @type	DataStore
	 */	
	Private 	$_datas = null;
	
	/**
	 * Object store id
	 * @type	mixed
	 */	
	Private 	$_id = null;
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	array 	$id = null
		 * @param	array	$datas = null
		 * @return	DBObj
		 * @access	public
		 */
		Function __construct($id, $datas=null)
		{
			parent::__construct($id, $datas);
			$this->init();
		}
		
		/**
		 * Magic alias for set
		 *
		 * @param	mixed $attr
		 * @param	mixed $value = null
		 * @return	DataStore
		 * @access	public
		 */
		Function __set($attr, $value=null)
		{
			return ($this->_datas->set($attr, $value));
		}
		
		/**
		 * Sets result
		 *
		 * @param	mixed $obj
		 * @param	bool clear = false
		 * @return	void
		 * @access	public
		 */
		Function sets($obj, $clear=false)
		{
			$this->_datas->sets($obj, $clear);
		}
		
		/**
		 * Gets result
		 *
		 * @param	bool	$jsonEncode = false
		 * @return	mixed
		 * @access	public
		 */
		Function gets($jsonEncode=false)
		{
			return ($this->_datas->gets($jsonEncode));
		}
		
		/**
		 * Magic alias for get
		 *
		 * @param	mixed $attr
		 * @return	mixed
		 * @access	public
		 */
		Function __get($attr)
		{
			return ($this->_datas->get($attr));
		}
		
		/**
		 * Find the object
		 *
		 * @param	mixed 	$attr
		 * @param	array 	$where = null
		 * @return	mixed
		 * @access	public
		 */
		Function find($table, $where=null)
		{
			
			$where = ($where instanceof DataStore) ? $where->gets() : null;
			
			if (Utils::isArray($table) && Utils::isEmpty($where)) {
				$this->store()->where = $table;
			}
			elseif ((!Utils::isEmpty($table) && !Utils::isArray($table)) && (!Utils::isEmpty($where) && Utils::isArray($where))) {
				$this->store()->where = $where;
				$this->store()->table = $table;
			}
			else {
				$this->error('Error on query format');
				return ($this);
			}
			
			$query = array('fields' => $this->store()->fields, 'where' => $this->store()->where);
			$res = DB::access($this->getId())->select($this->store()->table, $query);
			$this->sets($res->gets(), true);
			return($this);
		}
		
		/**
		 * Create a new object
		 *
		 * @param	string	$table
		 * @param	array 	$datas
		 * @return	bool
		 * @access	public
		 */
		Function insert($table, $datas=null)
		{
			if (Utils::isArray($table) && Utils::isEmpty($datas) && !Utils::isEmpty($this->store()->table)) {
				$datas = $table;
				$table = $this->store()->table;
			}
			elseif (!((!Utils::isEmpty($table) && !Utils::isArray($table)) && (!Utils::isEmpty($where) && Utils::isArray($where)))) {
				$this->error('Error on query format');
				return (false);
			}
			
			$res = DB::access($this->getId())->insert($table, $datas);
			$this->error(DB::access($this->getId())->getLastError());
			$this->_id = $res;
			return ($res);
		}
		
		/**
		 * Save the object
		 *
		 * @return	void
		 * @access	public
		 */
		Function save($where=null)
		{
			if (!Utils::isEmpty($where)) {
				$this->store()->where = $where;
			}
			
			$res = DB::access($this->getId())->update($this->store()->table, array('fields' => $this->gets(), 'where' => $this->store()->where));
			$this->error(DB::access($this->getId())->getLastError());
		}
		
		/**
		 * Delete the object
		 *
		 * @return	void
		 * @access	public
		 */
		Function delete()
		{
			DB::access($this->getId())->delete($this->store()->table, array('where' => $this->store()->where));	
		}
		
	Private
		/**
		 * Init the object
		 * 
		 * @return	void
		 * @access	private
		 */
		Function init()
		{
			$this->_datas = new DataStore();
			$this->store()->fields = (Utils::isArray($this->store()->fields)) ? $this->store()->fields : null;
			
			if ($this->getId() && $this->store()->table && $this->store()->where) {
				$this->find($this->store()->table, $this->store()->where);
			}
		}
		
}

?>