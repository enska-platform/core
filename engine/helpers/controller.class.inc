<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers;

use	\Enska\IO\JSON;
use \Enska\Connectors\CMD;
use	\Enska\Services\Utils;
use	\Enska\Services\Context;
use	\Enska\Services\Sessions;

class ModelController extends Model
{
	/**
	 * Input datas
	 * @type    DataStore
	 */
	Protected $input = null;

	/**
	 * Model results
	 * @type    DataStore
	 */
	Public $result = null;

	Public
		/**
		 * Constructor
		 *
		 * @param    string $id = null
		 * @param    array $datas = null
		 * @return   Model Controller
		 * @access    public
		 */
		Function __construct($id = null, $datas = null)
		{
			parent::__construct($id, $datas);
			$this->result = new DataStore(array('meta' => new DataStore(), 'records' => new DataStore(), 'error' => null));
			$this->decodeInput();
		}

		/**
		 * Methods loader
		 *
		 * @param    string $method
		 * @param    array $argv = null
		 * @return    mixed
		 * @access    public
		 */
		Function __call($method, $argv = null)
		{
			if (method_exists($this, $method)) {
				$datas = new DataStore($argv);
				$this->$method($datas);
			} else {
				$this->error('Method not exists');
				$this->results->error('Action unknown');
			}
			return ($this->result);
		}

	Private
		/**
		 * Decode input
		 *
		 * @return    void
		 * @access    private
		 */
		Function decodeInput()
		{
			if (Context::enska('CLI_REQUEST')) {
				$this->input = new cmdp();
				$this->input = $this->input->cmd;
			} elseif ($this->store()->request && $this->store()->request->input) {
				//$this->input = DataStore::create(htmlspecialchars_decode(Utils::filter($this->store()->request->input, array(Utils::_HTML_DECODE, Utils::_HTML_SPE_DECODE))));
				$this->input = DataStore::create(html_entity_decode(htmlspecialchars_decode($this->store()->request->input)));
			}
		}

	Protected
		Function notif($data)
		{
			$data = new DataStore($data);
			if (!$this->input->flags->S || !$this->input->options->standalone) {
				$this->console('> NOTIF::DASHBOARD');
			}
		}

	Protected
		Function progress($data)
		{
			$data = new DataStore($data);
			if (Context::enska('RTServer') && (!$this->input->flags->S || !$this->input->options->standalone)) {
				$this->console('> SYNC::APPCOOKERIO');
			}
		}

	Protected
		Function console($str)
		{
			if (Context::enska('CLI_REQUEST') && ($this->input->flags->d || $this->input->options->debug)) {
				CMD::console("$str");
			}
		}

	Protected
		Function usage($str)
		{
			if (Context::enska('CLI_REQUEST') && (!$this->input->flags->q && !$this->input->options->quiet)) {
				CMD::usage("$str");
			}
		}

	Protected
		Function openJson($file = null)
		{
			if (!($file = (!empty($file)) ? $file : (($this->input->vars && $this->input->vars->file) ? $this->input->vars->file : null))) {
				$argv = $this->input->arguments->gets();
				$file = (isset($argv[1])) ? $argv[1] : null;
			}

			$custom = null;
			if (!empty($file) && ($custom = new JSON($file, array('connector' => 'FS'))) && ($err = $custom->getLastError())) {
				$pwd = CMD::run('pwd')->result->gets();
				$file = $pwd[0] . '/' . $file;
				$err = null;
				if (($custom = new JSON($file, array('connector' => 'FS'))) && ($err = $custom->getLastError())) {
					$this->error($err);
					return (null);
				}
			}

			return ($custom);
		}

	Protected
		Function auth()
		{
			if ($this->input && $this->input->token) {
				Sessions::setToken($this->input->token);
				Sessions::load();
			}
			$auth = new Login();
			$auth->login();
		}

	Protected
		/**
		 * Apply error
		 *
		 * @param    string $error
		 * @param    int $headerCode
		 * @param    Reponse $stream
		 * @return    void
		 * @access    protected
		 */
		Function responseError($error, $headerCode, $stream)
		{
			$this->result->error = $error;
			$this->result->headerCode = $headerCode;
			$stream->push($this->result);
			return (false);
		}
}
?>