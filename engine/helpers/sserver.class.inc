<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers;

use	\Enska\Abstracts\Server;
use	\Enska\Services\Context;
use	\Enska\Services\Config;
use	\Enska\Services\Utils;

class SServer extends Server
{
	Public
		/**
		 * Constructor
		 * 
		 * @param	array 	$id = null
		 * @param	array	$datas = null
		 * @return	SServer
		 * @access	public
		 */
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
		}
		
		/**
		 * Server main handler
		 * 
		 * @param	Client	$client
		 * @param	string	$buffer
		 * @return	void
		 * @access	public
		 */
		Function handler($client, $buffer)
		{
			$buffer = trim($buffer);
			
			$cmd = new cmdp();
			$cmd->parse(explode(' ', $buffer));
			$cmd = $cmd->cmd;
			$cmd->input = DataStore::create($buffer);
			$argv = array();
			if ($cmd->input->uri) { $argv[0] = $cmd->input->uri; }
			else { $argv = $cmd->arguments->gets(); }
			$app = Context::enska('app');
			
			if (!strcmp($buffer, 'quit')) { \Enska\Services\Server::disconnect($client);  return; }
			
			if (!Utils::isEmpty($argv[0])) {
				$argv = explode('/', $argv[0]); $len = count($argv); $i = 0;
				if ($len > 1) {
					while ($i < $len) {
						if ($i == 0) { $cmd->vars->app = $argv[0]; }
						elseif($i == 1) { $cmd->vars->mod = $argv[1]; }
						elseif($i == 2) { $cmd->vars->target = $argv[2]; }
						$i++;
					}
				}
			}
			
			// TODO: Load readers + refacto
			
			$env = Context::enska('env');
			$cmd->vars->app = ($cmd->vars->app) ? $cmd->vars->app : $app;
			Context::set('app', $cmd->vars->app);
			$path = Config::get('settings')->$env->pools.'/'.Context::enska('pool').'/'.$cmd->vars->app.'/controller.php';
			$ns = (Config::get('settings')->$env->namespace) ? Config::get('settings')->$env->namespace : 'Applications';
			$controller = $ns.'\\'.Context::enska('pool').'\\'.$cmd->vars->app.'\\ApplicationController';
			$application = null;
			
			try {
				if (include_once($path)) {
					$cmd->uri = rtrim(Context::enska('pool').'.'.Config::get('settings')->default->domain.'/'.$cmd->vars->app.'/'.$cmd->vars->mod.'/'.$cmd->vars->target, '/').'/';
					$cmd->client = $client;
					$cmd->buffer = $buffer;
					$application = new $controller('rt', $cmd);
					unset($application);
				}
				else { throw new \Exception('Application path not found :: '.$path); }
			}
			catch (\Exception $e) { unset($application); \Enska\Services\Server::write($client, "ERROR::00000x01::".$e->getMessage()."\n"); return (false); }
		}
}

?>