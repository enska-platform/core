<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers.sessions
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers\Sessions;
use \Enska\Abstracts\Sessions;
use Enska\Helpers\DataStore;
use \Enska\Services\Utils;
use \Enska\Services\Context;
use \Enska\IO\JSON;
 
class PHP extends Sessions
{
	Public
		/**
		 * Constructor
		 * 
		 * @param	array 	$id = null
		 * @param	array	$datas = null
		 * @return	PHP
		 * @access	public
		 */
		Function __construct()
		{
			parent::__construct('PHP_SESSIONS');
		}
		
	/************/
	/* ABSTRACT */
	/************/
		
		/**
		 * Session starter
		 * 
		 * @return	void
		 * @access	private
		 */
		Function start()
		{
			if (!Context::enska('CLI_REQUEST')) {
				session_start(); }
			if (!Utils::isPositive($this->getSession('uid'))) { $this->setSession('uid', 4); }
		}
		
		/**
		 * Set a session variable
		 * 
		 * @param	string	$attr
		 * @param	mixed	$value = null
		 * @return	void
		 * @access	public
		 */
		Function setSession($attr, $value=null)
		{
			if (Context::enska('CLI_REQUEST')) {
				$sessions = new JSON(CONF_PATH.'sessions.json', array('connector' => 'FS', 'flag' => 'a+'));
				$sessions->{$attr} = $value; $sessions->write();
			}
			$_SESSION[$attr] = $value;
		}
		
		/**
		 * Get a session variable
		 * 
		 * @param	string	$attr
		 * @return	mixed
		 * @access	public
		 */
		Function getSession($attr)
		{
			if (Context::enska('CLI_REQUEST')) {
				$sessions = new JSON(CONF_PATH.'sessions.json', array('connector' => 'FS', 'flag' => 'a+'));
				return ($sessions->{$attr});
			}
			elseif (isset($_SESSION[$attr])) { return ($_SESSION[$attr]); }
			return (null);
		}
		
		/**
		 * Get all session variables
		 * 
		 * @return	mixed
		 * @access	public
		 */
		Function gets()
		{
			if (Context::enska('CLI_REQUEST')) {
				$sessions = new JSON(CONF_PATH.'sessions.json', array('connector' => 'FS', 'flag' => 'a+'));
				return ($sessions->read()->gets());
			}
			if (isset($_SESSION)) { return ($_SESSION); }
			return (null);
		}
		
		/**
		 * Delete a session variable
		 * 
		 * @param	string	$attr
		 * @return	mixed
		 * @access	public
		 */
		Function delete($attr)
		{
			if (Context::enska('CLI_REQUEST')) {
				$sessions = new JSON(CONF_PATH.'sessions.json', array('connector' => 'FS', 'flag' => 'a+'));
				if ($sessions->{$attr}) { $sessions->{$attr} = null; }
				$sessions->write();
			}
			unset($_SESSION[$attr]);
		}
		
		/**
		 * Destroy the current session
		 * 
		 * @return	void
		 * @access	public
		 */
		Function destroy()
		{
			session_destroy();
		}
}

?>