<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Object
 * @package		engine.helpers.sessions
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Helpers\Sessions;
use	\Enska\Helpers\DataStore;
use \Enska\Services\Utils;
use \Enska\Services\Sessions;
use \Enska\Services\Context;
use \Enska\Services\Config;
use \Enska\Helpers\dbObj;
 
class Shared extends \Enska\Abstracts\Sessions
{
	/**
	 * Session DBObject
	 * @type	dbObj
	 */
	Private		$session = null;
	
	/**
	 * Session object
	 * @type	DataStore
	 */
	Private		$datas = null;
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	array 	$id = null
		 * @param	array	$datas = null
		 * @return	Shared
		 * @access	public
		 */
		Function __construct()
		{
			parent::__construct('SHARED_SESSIONS');
		}
			
	/************/
	/* ABSTRACT */
	/************/
	
		/**
		 * Session starter
		 * 
		 * @return	void
		 * @access	private
		 */
		Function start()
		{
			$this->datas = new DataStore();
			if (Utils::isEmpty(Sessions::getToken()) || ((int)Sessions::getToken() == -1)) { Sessions::setToken('switched'); }
			else { Sessions::setToken($this->wakeup()); }
		}
		
	Private
		/**
		 * Session creation
		 * 
		 * @param	String	$token
		 * @param	Bool isNew = false
		 * @return	void
		 * @access	private
		 */
		Function create($token=null, $isNew=false)
		{
			$env = Context::enska('env');
			$token = ($token && $isNew) ? $token : Utils::createId(6).'-'.Utils::createId(6).'-'.Utils::createId(6).'-'.Utils::createId(3);
			$db = (Config::get('settings')->$env->sessions && Config::get('settings')->$env->sessions->cookie->db) ? Config::get('settings')->$env->sessions->cookie->db : null;
			if (!$db) { throw new \Exception("Sessions::SHARED::UnknowDB", 1); }
			$this->session = new dbObj($db, array('table' => 'sessions'));
			$res = $this->session->insert(array(
				'token'		=> $token,
				'locate'	=> md5($_SERVER['REMOTE_ADDR']), 
				'emitDate'	=> Utils::timestampToDatetime(Utils::timestamp(0, Utils::_ML)), 
				'validDate'	=> Utils::timestampToDatetime(Utils::timestamp(1, Utils::_ML, Utils::_ADD)),
				'datas'		=> '{}'));
			$this->session->datas = new DataStore();
			return ($token);
		}
		
	Private
		/**
		 * Session wakeup
		 * 
		 * @return	string
		 * @access	private
		 */
		Function wakeup()
		{
			$env = Context::enska('env');
			$db = (Config::get('settings')->$env->sessions && Config::get('settings')->$env->sessions->cookie->db) ? Config::get('settings')->$env->sessions->cookie->db : null;
			if (!$db) { throw new \Exception("Sessions::SHARED::UnknowDB", 1); }
			
			$search = array(
				'table' => 'sessions',
				'where' => array(
					'token' => Sessions::getToken(), 
					"AND" 	=> array(
						'locate'	=> md5($_SERVER['REMOTE_ADDR']))));
						/*, 
						"AND" 		=> array( 'validDate'	=> array('operator' => '>=', 'value' => Utils::timestampToDatetime(Utils::timestamp(0, Utils::_ML, Utils::_NOW)))))));*/
							
			$this->session = new DBObj($db, $search);
			if (!Utils::isPositive($this->session->id)) { Sessions::setToken($this->create(Sessions::getToken(), true)); $this->setSession('uid', 4); }
			$this->session->datas = new DataStore($this->session->datas);
			return ($this->session->token);
		}
		
		/**
		 * Set a session variable
		 * 
		 * @param	string	$attr
		 * @param	mixed	$value = null
		 * @return	void
		 * @access	public
		 */
		Function setSession($attr, $value=null)
		{
			$this->session->datas->set($attr, $value);
			$this->save();
		}
		
		/**
		 * Get a session variable
		 * 
		 * @param	string	$attr
		 * @return	mixed
		 * @access	public
		 */
		Function getSession($attr)
		{
			if ($this->session && $this->session->datas) {
				return ($this->session->datas->get($attr));
			}
		}
		
		/**
		 * Get all session variables
		 * 
		 * @return	mixed
		 * @access	public
		 */
		Function gets($jsonEncode=false)
		{
			return ($this->session->datas->gets($jsonEncode));
		}
		
		/**
		 * Delete a session variable
		 * 
		 * @param	string	$attr
		 * @return	mixed
		 * @access	public
		 */
		Function delete($attr)
		{
			$this->session->datas->delete($attr);
			$this->save();
		}
		
		/**
		 * Destroy the current session
		 * 
		 * @return	void
		 * @access	public
		 */
		Function destroy()
		{
			$this->session->datas->clear();
			$this->save();
			$this->remove();
		}
		
	Private
		/**
		 * Save the current session
		 * 
		 * @return	void
		 * @access	private
		 */
		Function save()
		{
			$this->session->datas = $this->session->datas->gets(true);
			$this->session->save(array('token' => Sessions::getToken(), "AND" => array('locate' => md5($_SERVER['REMOTE_ADDR']))));
			$this->session->datas = DataStore::create($this->session->datas);
		}
		
	Private
		/**
		 * Remove from DB the current session
		 * 
		 * @return	void
		 * @access	private
		 */
		Function remove()
		{
			$this->session->delete();
		}
}

?>