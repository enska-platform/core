<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Connector
 * @package		core.engine.connectors
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 * @todo		Implements errors
 */

namespace Enska\Connectors;

use Agent\app\main\services\BUS;
use \Enska\Abstracts\Connector;
use \Enska\Helpers\DataStore;
use \Enska\Services\Utils;
use \Enska\Services\Context;

class Docker extends Connector
{
	Protected	$cmd = null;
	Protected	$_cmd = null;
			
	Public
		/**
		 * Constructor
		 * 
		 * @param	string	$id = null
		 * @param	array 	$datas = null
		 * @return	Docker
		 * @access	public
		 */
		Function __construct($id = null, $datas = null)
		{
			parent::__construct($id, $datas);
			return ($this->open());
		}

		/**
		 * Open the connexion
		 *
		 * @return	bool
		 * @access	public
		 */
		Function open()
		{
			$this->cmd = new CMD();
			$this->meta = DataStore::create(array(
				'image' => '', 'name' => '', 'volumes' => '', 'ports' => '', 'links' => '', 'deamon' => '', 'rm' => ''
			));

			$this->setName($this->getId());
			$this->setImage($this->getId());

			if ($this->store()->registry && $this->store()->username) {
				return ($this->login()); }

			return (true);
		}

		/**
		 * Login to a docker registry
		 */
		Function login()
		{
			if (($cache = Context::enska('dockerLogin-'.$this->store()->registry)))
			{ return(true); }

			if ($this->_send('login', array(
					'-u', $this->store()->username,
					'-p', $this->store()->password,
					'-e', $this->store()->email,
					'https://'.$this->store()->registry))) {

				Context::set('dockerLogin-'.$this->store()->registry, true);
				return (true);
			}

			Context::set('dockerLogin-'.$this->store()->registry, false);
			return (false);
		}
		
		/**
		 * Close the connexion
		 *
		 * @return	void
		 * @access	public
		 */
		Function close() {}
		
		/**
		 * Set the buffer value
		 * 
		 * @param	string	$buffer = null
		 * @param	array 	$options = null
		 * @return	DataStore
		 * @access	public
		 */
		Function send($buffer=null, $options=null)
		{
			if ($buffer) {
				return($this->cmd->usr('docker/'.$buffer, implode(' ', $options))->receive()); }

			return (DataStore::create());
		}

		/**
		 * Set the buffer value
		 *
		 * @param	string	$buffer = null
		 * @param	array 	$options = null
		 * @param	bool	$isFile = false
		 * @return	bool
		 * @access	public
		 */
		Function _send($buffer=null, $options=array(), $isFile=false)
		{
			if ($isFile === true) {
				$this->_cmd = $this->send($buffer, $options);
				$this->_cmd->result = $this->_cmd->stdout;
				$this->_cmd->exit = $this->_cmd->status;
			}
			else {
				//debug('docker '.$buffer.' '.implode(' ', $options));
				$this->_cmd = CMD::run('docker '.$buffer.' '.implode(' ', $options));
			}

			if ($this->_cmd->exit != 0) {
				return ($this->error('Docker-Connector::'.\debug::pr($this->_cmd->gets(), true)));
			}

			return (true);
		}

		/**
		 * Get the buffer value
		 * 
		 * @param	array $options = null
		 * @return	string
		 * @access	public
		 * @todo 	Set filter when get the buffer
		 */
		Function receive($options=null) {}

		/**
		 * Clean the container instance
		 *
		 * @return	bool
		 * @access	public
		 */
		Function clean()
		{
			return ($this->open());
		}

		/**
		 * Set the container volumes
		 *
		 * @param	string	$volumes = null
		 * @return	void
		 * @access	public
		 */
		Function setVolumes($volumes = null, $parent)
		{
			$this->meta->volumes = ($volumes instanceof DataStore) ?
					$this->_setVolumesFlow($volumes, $parent) :
					(($volumes) ? $this->_parseOptions($volumes, '-v') : '');
		}

		/**
		 * Set the container links
		 *
		 * @param	string	$links = null
		 * @return	void
		 * @access	public
		 */
		Function setLinks($links = null)
		{
			$this->meta->links = ($links) ? $this->_parseOptions($links, '--link') : '';
		}

		/**
		 * Set the container ports
		 *
		 * @param	string	$ports = null
		 * @return	void
		 * @access	public
		 */
		Function setPorts($ports = null)
		{
			$this->meta->ports = ($ports) ? $this->_parseOptions($ports, '-p') : '';
		}

		/**
		 * Set the container name
		 *
		 * @param	string	$name = null
		 * @return	void
		 * @access	public
		 */
		Function setName($name = null)
		{
			$this->meta->name = ($name) ? $name : 'mc-'.Utils::createId(5);
		}

		/**
		 * Set the used image name
		 *
		 * @param	string	$image
		 * @return	void
		 * @access	public
		 */
		Function setImage($image)
		{
			$this->meta->image = ($this->store()->registry) ? rtrim($this->store()->registry, '/').'/'.$image : $image;
		}

		/**
		 * Set the container as a deamon
		 *
		 * @return	void
		 * @access	public
		 */
		Function setAsDeamon()
		{
			$this->meta->deamon = '-d';
		}

		/**
		 * Set the container as temporary
		 *
		 * @return	void
		 * @access	public
		 */
		Function setAsTemp()
		{
			$this->meta->rm = '--rm';
		}

		/**
		 * Set the Dockerfile path
		 *
		 * @param	string	$dfPath = '.'
		 * @return	void
		 * @access	public
		 */
		Function dockerfile($dfPath = '.')
		{
			$this->meta->dockerfile = $dfPath;
		}

		/**
		 * Remove a container from stack
		 *
		 * @param	bool	$force = true
		 * @param	bool	$volume = true
		 * @return	bool
		 * @access	public
		 */
		Function remove($force = true, $volume = false)
		{
			$force = ($force === true) ? '-f' : '';
			$volume = ($volume === true) ? '-v' : '';

			return ($this->_send('rm', array($force, $volume, $this->meta->name)));
		}

		/**
		 * Delete an image from stack
		 *
		 * @param	bool	$force = true
		 * @param	bool	$volume = true
		 * @return	bool
		 * @access	public
		 */
		Function removeImage($force = true)
		{
			$force = ($force === true) ? '-f' : '';

			return ($this->_send('rmi', array($force, $this->meta->image)));
		}

		/**
		 * Build an image from Dockerfile
		 *
		 * @param	bool	$force = true
		 * @return	bool
		 * @access	public
		 */
		Function build($force = true)
		{
			if ($force === true) { $this->removeImage(true); }
			return ($this->_send('build', $this->generate()));
		}

		/**
		 * Pull an image from repository
		 *
		 * @return	bool
		 * @access	public
		 */
		Function pull()
		{
			return ($this->_send('pull', array($this->meta->image)));
		}

		/**
		 * Push an image to repository
		 *
		 * @return	bool
		 * @access	public
		 */
		Function push()
		{
			return ($this->_send('push', array($this->meta->image)));
		}

		/**
		 * Commit a container to the image
		 *
		 * @return	bool
		 * @access	public
		 */
		Function commit($container)
		{
			return ($this->_send('commit', array($container, $this->meta->image)));
		}

		/**
		 * Tag the image
		 *
		 * @return	bool
		 * @access	public
		 */
		Function tag($tag)
		{
			return ($this->_send('tag', array($this->meta->image, $tag)));
		}

		/**
		 * Run an image from stack
		 *
		 * @param	bool	$force = true
		 * @return	bool
		 * @access	public
		 */
		Function run($force = true)
		{
			if ($force === true) { $this->remove(true); }
			return ($this->_send('run', $this->compile()));
		}

		/**
		 * Start a container
		 *
		 * @param	bool	$force = true
		 * @return	bool
		 * @access	public
		 */
		Function start($force = true)
		{
			if ($force === true) { $this->stop(); }
			return ($this->_send('start', $this->meta->name));
		}

		/**
		 * Stop a container
		 *
		 * @param	bool	$force = true
		 * @return	bool
		 * @access	public
		 */
		Function stop($force = true)
		{
			if ($force === true) { $this->kill(); }
			return ($this->_send('stop', $this->meta->name));
		}

		/**
		 * Kill a container
		 *
		 * @return	bool
		 * @access	public
		 */
		Function kill()
		{
			return ($this->_send('kill', $this->meta->name));
		}

	Protected
		/**
		 * Transform configuration cmd to docker cmd
		 *
		 * @return	array
		 * @access	protected
		 */
		Function compile()
		{
			return (array(
				$this->meta->rm,
				$this->meta->deamon,
				'--name '.$this->meta->name,
				$this->meta->volumes,
				$this->meta->links,
				$this->meta->ports,
				$this->meta->image
			));
		}

	Protected
		/**
		 * Transform configuration cmd to docker cmd
		 *
		 * @return	array
		 * @access	protected
		 */
		Function generate()
		{
			return (array(
				'-t',
				$this->meta->image,
				$this->meta->dockerfile
			));
		}

	Protected
		/**
		 * Parse cmd vars to docker cmd
		 *
		 * @param	array $store
		 * @param	string $delimiter
		 * @return	string
		 * @access	protected
		 */
		Function _parseOptions($store, $delimiter)
		{
			$len = count(($options = explode(',', $store)));
			$result = '';
			for ($i = 0; $i < $len; $i++) {
				$option = explode(':',$options[$i]);
				if (!isset($option[1])) { $option[1] = $option[0]; }
				if (!strcmp($option[0], '?')) { $result .= ' '.$delimiter.' '.$option[1]; }
				else { $result .= ' '.$delimiter.' '.$option[0].':'.$option[1]; }
			}
			$options = trim($result);
			return ($options);
		}

	Protected
		/**
		 * Parse volumes array to docker cmd
		 *
		 * @param	array $store
		 * @param	string $delimiter
		 * @return	string
		 * @access	protected
		 */
		Function _setVolumesFlow($store, $parent)
		{
			$flow = $parent->volumes;
			$flow->apply($store->gets());

			$flow->flowConfig = BUS::get($flow->config.'-flow', true);
			if ($flow->flowConfig->count() < 1) { return (false); }
			$flow->gitConfig = BUS::get($flow->flowConfig->origin, true);
			if ($flow->gitConfig->count() < 1) { return (false); }

			$vols = explode(':', $flow->volume);
			if (!isset($vols[0])) { return (false); }
			$volPath = $vols[0];

			$git = new GIT($volPath, $flow->gitConfig);
			if (!$git->localClone(true)) { return ($this->error($git->getLastError())); }

			$git->fetch(); $git->checkout($flow->flow);
			if ($flow->flowConfig->submodule) {
				$git->submoduleInit(); $git->submoduleUpdate();
			}

			if ($flow->after) { CMD::_($flow->after); }

			return ($this->_parseOptions($flow->volume, '-v'));
		}
}

?>