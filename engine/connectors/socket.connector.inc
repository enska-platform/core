<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Abstract
 * @package		engine.abstracts
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Connectors;

use \Enska\Abstracts\Connector;
use	\Enska\Services\Utils;
use \Enska\Services\Server;

class Socket extends Connector
{
	const	HEADER_LEN = 5; // FOR SEND PAQUET WITH WEBSOCKET, TODO ...
	const	BUFF_SIZE = 512;
	
	/**
	 * Socket handler
	 * @var	socket
	 */
	Private	$sock = 0;
	
	/**
	 * Server pool sockets
	 * @var	array
	 */
	Private	$socks = array();
	
	/**
	 * Server queue
	 * @var	int
	 */
	Private	$queue = 20;
	
	/**
	 * Client is connected
	 * @var	bool
	 */
	Private	$isConnected = false;
		
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null
		 * @param	mixed 	$datas = null
		 * @return	Socket
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
			return ($this->open());
		}

		/**
		 * Open the connexion
		 *
		 * @return	bool
		 * @access	public
		 */
		Function open()
		{
			$this->sock = ($this->store()->socketPath) ? socket_create(AF_UNIX, SOCK_STREAM, SOL_TCP) : socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
			$this->store()->host = ($this->store()->socketPath) ? $this->store()->socketPath : $this->store()->host;
			if (!strcmp(trim($this->store()->host), 'public') || !trim($this->store()->host)) { $this->store()->host = $this->getHostIp(); }
			
			if ($this->sock === false || !is_resource($this->sock)) {
				$this->error('Socket creation error: '.socket_strerror(socket_last_error()));
				return (false);
			}
			
			return (true);
		}
		
		/**
		 * Close stream
		 *
		 * @return	void
		 * @access	public
		 */
		Function close()
		{
			if (Utils::isArray($this->socks)) {
				$i = 0; $len = count($this->socks);
				while ($i < $len) { @socket_close($this->socks[$i++]); }
				@socket_close($this->sock);
			}
			
			$this->sock = null;
			$this->socks = array();
		}
		
		/**
		 * Listen to input stream datas
		 *
		 * @param	mixed	$filters = null
		 * @return	void
		 * @access	public
		 */
		Function receive($filters=null)
		{
			socket_set_option($this->sock, SOL_SOCKET, SO_REUSEADDR, 1);
			if (socket_bind($this->sock, $this->store()->host, $this->store()->port) == false) { throw new \Exception('Unable to bind the socket server :: '.socket_strerror(socket_last_error())); }
			if (socket_listen($this->sock, $this->queue) == false) { throw new \Exception('Unable to listen with the socket server :: '.socket_strerror(socket_last_error())); }
			set_time_limit(0);
			ob_implicit_flush();
			$this->socks = array($this->sock);
		}
		
		/**
		 * Write datas to stream
		 *
		 * @param	string	$buffer = null
		 * @param	array 	$options = null
		 * @return	bool
		 * @access	public
		 */
		Function send($buffer=null, $options=null)
		{
			if (!$this->isConnected) {
				if (($this->addr = socket_connect($this->sock, $this->store()->host, $this->store()->port)) == false) {
					$this->error('Unable to connect server: '.socket_strerror(socket_last_error()));
					return (false);
				}
				return (true);
			}
			
			// --> FOR WEBSOCKET 
			/*$this->_paqt = $this->_head.$this->_mesg;
			$this->_wlen = strlen($this->_paqt) + self::HEADER_LEN;*/
		}
		
		/**
		 * Socket select
		 * 
		 * @return	void
		 * @access	public
		 */
		Function select()
		{
			$readers = $this->socks;
			socket_select($readers, $writers = null, $except, 0, 1000000);
			foreach($readers as $socket) {
				if($socket == $this->sock) {
					if(($socketClient = socket_accept($this->sock)) < 0) { continue; }
					else {
						socket_set_option($socketClient, SOL_SOCKET, SO_KEEPALIVE, 1);
						$cb = $this->store()->api->onConnect;
						$cb($socketClient);
					}
				}
				else {
					$bytes = @socket_recv($socket , $buffer, self::BUFF_SIZE, 0); 
					if($bytes == 0) {
						$cb = $this->store()->api->onDisconnect;
						$cb($socket);
					}
					else {
						$cb = $this->store()->api->handler;
						$cb($socket, $buffer); 
					}
				}
			}
		}
		
		/**
		 * Add a new client socket
		 * 
		 * @param	resource	$socket
		 * @return 	void
		 * @access	public
		 */
		Function addSocket($socket)
		{
			array_push($this->socks, $socket);
		}
		
		/**
		 * Delete a client socket
		 * 
		 * @param	resource	$socket
		 * @return 	void
		 * @access	public
		 */
		Function remSocket($socket)
		{
			if(($index = array_search($socket, $this->socks)) && $index >= 0) { array_splice($this->socks, $index, 1); }
			@socket_close($socket);
		}
		
		/**
		 * Get the host IP
		 */
		Function getHostIp()
		{
			preg_match_all('/inet'.($withV6 ? '6?' : '').' addr: ?([^ ]+)/', `ifconfig`, $ips);
			if (isset($ips[1][1])) { return ($ips[1][1]); }
		    return (gethostbyname(gethostname()));
		}
}

?>