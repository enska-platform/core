<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Connector
 * @package		engine.connectors
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Connectors;

use \Enska\Abstracts\Connector;
use \Enska\Helpers\DataStore;
use \Enska\Services\Context;
use \Enska\Services\Config;
use \Enska\Services\Utils;

class DB extends Connector
{
	/**
	 * PDO instance
	 * @type	Object PDO
	 */
	Protected	$_PDO = null;
	
	/**
	 * Query result
	 * @type	DataStore
	 */
	Protected	$results = null;
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	string	$id = null
		 * @param	array 	$datas = null
		 * @return	DB
		 * @access	public
		 */
		Function __construct($id = null, $datas = null)
		{
			parent::__construct($id, $datas);
			$file = ENSKA_CONF.'db/'.$id.'.json';
			Config::load($id.'.db', $file);
			
			$env = Context::enska('env');
			$conf = Config::get($id.'.db')->$env;
			
			if (!$conf instanceof DataStore) { throw new \Exception("Error on reading DB configuration", 1); }
			
			$conf->name = (!$conf->name) ? $conf->name = '' : $conf->name;
			$conf->port = (!$conf->port) ? $conf->port = '' : $conf->port;
			$this->store()->apply($conf->gets());
			$this->open();
		}
		
		/**
		 * Open the connexion
		 *
		 * @return	bool
		 * @access	public
		 * @todo	Implements autolad adapter
		 */
		Function open()
		{
			if (!$this->store()->dsn) {
				return (false);
			}
			$dsn = Utils::strReplace($this->store()->dsn, $this->store()->gets());
			$this->_PDO = new \PDO($dsn, $this->store()->login, $this->store()->password);
			$this->_PDO->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			$this->_PDO->exec("SET CHARACTER SET utf8");
			return (true);
		}
		
		/**
		 * Close the connexion
		 *
		 * @return	void
		 * @access	public
		 */
		Function close() {}
		
		/**
		 * Set the buffer value
		 * 
		 * @param	string	$buffer = null
		 * @param	array 	$options = null
		 * @return	bool
		 * @access	public
		 */
		Function send($buffer=null, $options=null)
		{
			if (Utils::isArray($buffer) && count($buffer) == 1) {
				$i = 0;
				$nb = 10000;
				while ($i < $nb) {
					if (isset($buffer[$i])) { $buffer = $buffer[$i]; break; }
					$i++;
				}
			}
			
			if (Utils::isArray($buffer)) {
				try {
					$this->_PDO->beginTransaction(); $id = null;
					foreach ($buffer as $request) {
						$this->_PDO->exec($request);
						if (strcmp(substr(trim(strtolower($request)), 0, 6), 'insert') == 0) { $id = $this->_PDO->lastInsertId(); }
					}
					$this->_PDO->commit();
					return (true);
				}
				catch(Exception $e) {
					$this->error($e->getMessage().' :: '.$request);
					$this->_PDO->rollBack();
				}
			}
			elseif (!Utils::isEmpty($buffer, Utils::_STRING_)) {
				if (strcmp(substr(trim(strtolower($buffer)), 0, 6), 'select') == 0) { return ($this->receive($buffer)); }
				try {
					$this->_PDO->beginTransaction();
					$this->_PDO->exec($buffer);
					$id = $this->_PDO->lastInsertId();
					$this->_PDO->commit();
					if (strcmp(substr(trim(strtolower($buffer)), 0, 6), 'insert') == 0) { return($id); }
					return (true);
				}
				catch(\Exception $e) {
					$this->error($e->getMessage(). ' :: '.$buffer);
					$this->_PDO->rollBack();
				}
			}
			return (false);
		}
		
		/**
		 * Get the buffer value
		 * 
		 * @param	array $options = null
		 * @return	string
		 * @access	public
		 * @todo 	Set filter when get the buffer
		 */
		Function receive($options=null)
		{
			try {
				$req = $this->_PDO->query($options);
				if ($req) {
					$req = $req->fetchAll();
					if (count($req) == 0) {
						$this->error('No result found');
						return (null);
					}
					else {
						$this->results = new DataStore();
						$i = 0; $nb = count($req); $this->results->clear();
						if ($nb > 0) {
							foreach ($req as $res) {
								$i++;
								$ds = new DataStore();
								foreach ($res as $key => $data) {
									if (!is_numeric($key)) { $ds->$key = $data; }
								}
								$this->results->$i = $ds;
							}
							$req = $this->results->gets();
						}
					}
				}
					
				if (($req === null) || ($req === false)) {			
					$this->error('Error on request :: '.$options);
					return (null);
				}
					
				return ($req);
			}
			catch(Exception $e) {
				$this->error($options.' :: '." -- :: ".$e->getMessage());
				return (null);
			}
		}
}

?>