<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Connector
 * @package		core.engine.connectors
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Connectors;

use \Enska\Abstracts\Connector;
use Enska\Helpers\DataStore;

class GIT extends Connector
{
	CONST	_CHECKOUT = 1;
	CONST	_CREATE = 2;
	CONST	_DELETE_LOCAL = 3;
	CONST	_DELETE_REMOTE = 4;

	Protected	$dsn = null;
	Protected	$cmd = null;
	Protected	$_cmd = null;
			
	Public
		/**
		 * Constructor
		 * 
		 * @param	string	$id = null
		 * @param	array 	$datas = null
		 * @return	GIT
		 * @access	public
		 */
		Function __construct($id = null, $datas = null)
		{
			parent::__construct($id, $datas);
			$this->setId(rtrim($this->getId(), '/'));
			return ($this->open());
		}

		/**
		 * Open the connexion
		 *
		 * @return	bool
		 * @access	public
		 */
		Function open()
		{
			$this->cmd = new CMD();

			if ($this->store()->origin) {
				$this->dsn = $this->store()->origin; $origin = null; $len = strlen($this->store()->origin);
				if ($this->store()->login && $this->store()->password) {

					if (!strncmp($this->store()->origin, 'https://', 8)) {
						$origin = substr($this->store()->origin, 8, ($len - 8));
						$protocol = 'https';
					}
					elseif (!strncmp($this->store()->origin, 'http://', 7)) {
						$origin = substr($this->store()->origin, 7, ($len - 7));
						$protocol = 'http';
					}
					else { return ($this->error('git.open.badOrigin')); }

					if (($origin = strstr($origin, '@'))) {
						$this->dsn = $protocol.'://'.$this->store()->login.':'.$this->store()->password.'@'.ltrim($origin, '@');
					}
					else { return ($this->error('git.open.badOrigin')); }

				}
				return (true);
			}
			return (false);
		}
		
		/**
		 * Close the connexion
		 *
		 * @return	void
		 * @access	public
		 */
		Function close() {}
		
		/**
		 * Set the buffer value
		 * 
		 * @param	string	$buffer = null
		 * @param	array 	$options = null
		 * @return	DataStore
		 * @access	public
		 */
		Function send($buffer=null, $options=null)
		{
			if ($buffer) {
				return($this->cmd->usr('git/'.$buffer, implode(' ', $options))->receive()); }

			return (DataStore::create());
		}

		/**
		 * Set the buffer value
		 *
		 * @param	string	$buffer = null
		 * @param	array 	$options = null
		 * @param	bool	$isFile = false
		 * @return	bool
		 * @access	public
		 */
		Function _send($buffer=null, $options=array(), $isFile=false)
		{
			if ($isFile === true) {
				$this->_cmd = $this->send($buffer, $options);
				$this->_cmd->result = $this->_cmd->stdout;
				$this->_cmd->exit = $res->status;
			}
			else {
				$this->_cmd = CMD::run('cd '.$this->getId().' && git '.$buffer.' '.implode(' ', $options)); }

			if ($this->_cmd->exit != 0) {
				return ($this->error('GIT-Connector::'.\debug::pr($this->_cmd->gets(), true)));
			}

			return (true);
		}

		/**
		 * Get the buffer value
		 * 
		 * @param	array $options = null
		 * @return	string
		 * @access	public
		 * @todo 	Set filter when get the buffer
		 */
		Function receive($options=null) {}

		/**
		 * Init a blank repository
		 *
		 * @param	string	$message = "Initial repository commit"
		 * @param	bool	$force = false
		 * @param	bool	$removeAfterInit = false
		 * @return	bool
		 * @access	public
		 */
		Function initBlank($message = 'Initial repository commit', $force = false, $removeAfterInit = false)
		{
			if (is_dir($this->getId().'/.git')) {
				return ($this->error('git.init.exists')); }
			if (is_dir($this->getId()) && !$force) {
				return ($this->error('git.init.exists')); }

			$message = '"'.$message.'"';
			$init = array($this->getId(), $this->dsn, '1', $message);

			if (is_dir($this->getId()) && $force) {
				$init = array($this->getId(), $this->dsn, '0', $message); }

			return ((
				$this->_send('init', $init, true)
				&& $this->init()
				&& $this->addOrigin()
				&& $this->add()
				&& $this->commit($message)
				&& $this->push('master', 'origin', false, true)
				&& $this->clean($removeAfterInit)
			));
		}

		/**
		 * Clean the local system
		 *
		 * @param	$clean = true
		 * @return	bool
		 * @access	public
		 */
		Function clean($clean = true)
		{
			if ($clean === true) {
				return ($this->_send('removeDir', array($this->getId()), true)); }

			return (true);
		}

		/**
		 * Git init command
		 *
		 * @return	bool
		 * @access	public
		 */
		Function init()
		{
			if (is_dir($this->getId().'/.git')) {
				return ($this->error('git.init.exists')); }

			return ($this->_send('init'));
		}

		/**
		 * Clone the current repository origin url on local system
		 *
		 * @param	bool	$force = false
		 * @return	bool
		 * @access	public
		 */
		Function localClone($force = false)
		{
			if (is_dir($this->getId()) && !$force) {
				return ($this->error('git.localClone.exists')); }

			if (is_dir($this->getId())) {
				$this->_send('removeDir', array($this->getId()), true); }

			CMD::run('mkdir -p '.$this->getId());
			return ($this->_send('clone', array($this->dsn, $this->getId())));
		}

		/**
		 * Push on remote origin source
		 *
		 * @param	string	$branch
		 * @param	string	$origin = 'origin'
		 * @param	bool	$force = false
		 * @param	bool	$first = false
		 * @param	bool	$tags = false
		 * @return	bool
		 * @access	public
		 */
		Function push($branch, $origin = 'origin', $force = false, $first = false, $tags = false)
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			if ($tags === true) {
				return ($this->_send('push', array('--tags'))); }

			$force = ($force === true) ? '-f' : '';
			$first = ($first === true) ? '-u' : '';

			return ($this->_send('push', array($force, $first, $origin, $branch)));
		}

		/**
		 * Pull from remote origin source
		 *
		 * @param	string	$branch
		 * @param	string	$origin = 'origin'
		 * @return	bool
		 * @access	public
		 */
		Function pull($branch, $origin = 'origin')
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			return ($this->_send('pull', array($origin, $branch)));
		}

		/**
		 * Merge branches
		 *
		 * @param	string	$branch
		 * @param	string	$origin = 'origin'
		 * @return	bool
		 * @access	public
		 */
		Function merge($branch, $origin = 'origin')
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			return ($this->_send('merge', array($origin.'/'.$branch)));
		}

		/**
		 * Tag a branch
		 *
		 * @param	string	$tag
		 * @param	string	$message = "Release version tag: $VERSION"
		 * @param	string	$origin = 'origin'
		 * @return	bool
		 * @access	public
		 */
		Function tag($tag, $message=null, $branch = 'master')
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			$message = ($message && !strncmp($message, '"', 1)) ? $message : (($message) ? '"'.$message.'"' : '"Release version tag: '.$tag.'"');

			return ($this->_send('tag', array('-a', $tag, '-m', $message, $branch)));
		}

		/**
		 * Save changes
		 *
		 * @param	string	$path = '.'
		 * @return	bool
		 * @access	public
		 */
		Function add($path = '.')
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			return ($this->_send('add', array($path)));
		}

		/**
		 * Commit changes
		 *
		 * @param	string	$message = "Committed with MitchIO"
		 * @return	bool
		 * @access	public
		 */
		Function commit($message=null, $amend=false, $a=false)
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			$message = ($message && !strncmp($message, '"', 1)) ? $message : (($message) ? '"'.$message.'"' : '"Committed with MitchIO"');
			$amend = ($amend === true) ? '--amend' : '';
			$a = ($a === true) ? '-a' : '';

			return ($this->_send('commit', array($a, $amend, '-m', $message)));
		}

		/**
		 * Reset the repo
		 *
		 * @param	string	$type = 'hard'
		 * @param	string	$version = 'HEAD'
		 * @return	bool
		 * @access	public
		 */
		Function reset($type = 'hard', $version = 'HEAD')
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			return ($this->_send('reset', array('--'.$type, $version)));
		}

		/**
		 * Checkout the requested branch
		 *
		 * @param	string	$branch = 'master'
		 * @param	int		$chkdo = self::_CHECKOUT
		 * @param	string	$origin = 'origin'
		 * @return	bool
		 * @access	public
		 */
		Function checkout($branch = 'master', $chkdo = self::_CHECKOUT, $origin = 'origin')
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			switch($chkdo) {
				case self::_CHECKOUT:
					return ($this->_send('checkout', array($branch)));
				case self::_CREATE:
					return ($this->_send('checkout', array('-b', $branch)));
				case self::_DELETE_LOCAL:
					return ($this->_send('branch', array('-D', $branch)));
				case self::_DELETE_REMOTE:
					return ($this->_send('push', array('-f', $origin, '--delete', $branch)));
				default:
					return (false);
			}
		}

		/**
		 * Fetch the repository
		 *
		 * @return	bool
		 * @access	public
		 */
		Function fetch()
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			return ($this->_send('fetch', array()));
		}

		/**
		 * Add a new git origin
		 *
		 * @param	string	$origin
		 * @param	string	$alias = 'origin'
		 * @return	bool
		 * @access	public
		 */
		Function addOrigin($origin=null, $alias = 'origin')
		{
			$origin = ($origin) ? $origin : $this->dsn;

			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			return ($this->_send('remote add', array($alias, $origin)));
		}

		/**
		 * Remove a git origin
		 *
		 * @param	string	$alias
		 * @return	bool
		 * @access	public
		 */
		Function delOrigin($alias)
		{
			$alias = ($alias) ? $alias : 'origin';

			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			return ($this->_send('remote remove', array($alias)));
		}

		/**
		 * Init the submodules
		 *
		 * @return	bool
		 * @access	public
		 */
		Function submoduleInit()
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			return ($this->_send('submodule', array('init')));
		}

		/**
		 * Update submodules
		 *
		 * @return	bool
		 * @access	public
		 */
		Function submoduleUpdate()
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			return ($this->_send('submodule', array('update')));
		}

		/**
		 * Add a new submodule to repository
		 *
		 * @param	mixed	$config
		 * @param	string	$path = ''
		 * @return	bool
		 * @access	public
		 * @todo	Implement config options(login...)
		 */
		Function submoduleAdd($config, $path = '')
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			return ($this->_send('submodule', array('add', $config, $path)));
		}

		/**
		 * De-Init a submodules
		 *
		 * @param	string	$module
		 * @return	bool
		 * @access	public
		 */
		Function submoduleDeInit($module)
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			return ($this->_send('submodule', array('deinit', $module)));
		}

		/**
		 * Remove a submodule from repository
		 *
		 * @param	string	$module
		 * @return	bool
		 * @access	public
		 */
		Function submoduleRem($module)
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			return ((
				$this->submoduleDeInit($module)
				&& $this->rm($module)
				&& $this->_send('removeDir', array($this->getId().'.git/modules/'.$module), true)
			));
		}

		/**
		 * RM files/folders
		 *
		 * @param	string	$path
		 * @param	bool	$force
		 * @return	bool
		 * @access	public
		 */
		Function rm($path, $force=true)
		{
			if (!is_dir($this->getId().'/.git')) {
				return ($this->error('git.repository.notExists')); }

			$force = ($force === true) ? '-rf' : '';
			return ($this->_send('rm', array($force, $path)));
		}

		/**
		 * Add a new README.md
		 *
		 * @param	string	$text
		 * @return	bool
		 * @access	public
		 */
		Function readme($text)
		{
			return ($this->_send('createREADME', array($this->getId(), '"'.$text.'"'), true));
		}
}

?>