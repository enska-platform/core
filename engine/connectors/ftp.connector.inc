<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Connector
 * @package		core.engine.connectors
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 * @todo		Implements errors, ls, rm, meta, cp, archive transfert
 */

namespace Enska\Connectors;
use \Enska\Abstracts\Connector;
 
class FTP extends Connector
{
	/**
	 * SSH coonexion
	 * @type	ftpd
	 */
	Protected	$_connection = null;
			
	Public
		/**
		 * Constructor
		 * 
		 * @param	string	$id = null
		 * @param	array 	$datas = null
		 * @return	FTP
		 * @access	public
		 */
		Function __construct($id = null, $datas = null)
		{
			parent::__construct($id, $datas);
			return ($this->open());
		}
		
		/**
		 * Open the connexion
		 *
		 * @return	bool
		 * @access	public
		 */
		Function open()
		{
			$hostname = (!Utils::isEmpty($this->store()->hostname)) ? $this->store()->hostname : 'localhost';
			$port = (!Utils::isEmpty($this->store()->port)) ? $this->store()->port : 21;
			$timeout = (!Utils::isEmpty($this->store()->timeout)) ? $this->store()->timeout : 30;
			$ssl = (!Utils::isEmpty($this->store()->ssl)) ? $this->store()->sll : false;
			
			
			if ($ssl) {
				$this->_connection = ftp_ssl_connect($this->store()->hostname, $port, $timeout);
			}
			else {
				$this->_connection = ftp_connect($this->store()->hostname, $port, $timeout);
			}
			
			if ($this->_connection && ftp_login($this->_connection, $this->store()->login, $this->store()->password)) {
				ftp_pasv($this->_connection, true);
				return (true);
			}
			return (false);
		}
		
		/**
		 * Close the connexion
		 *
		 * @return	void
		 * @access	public
		 */
		Function close()
		{
			@ftp_close($this->_connection);
		}
		
		/**
		 * Set the buffer value
		 * 
		 * @param	string	$buffer = null
		 * @param	array 	$options = null
		 * @return	bool
		 * @access	public
		 */
		Function send($buffer=null, $options=null)
		{
			$tmpId = TMP.Utils::createId(10);
			$tmp = new FS($tmpId, array('flag'=>'w'));
			if (!Utils::isArray($options)) {
				$options = array('save'=>true);
			}
			$tmp->send($buffer, $options);
			
			$res = true;
			if ($this->upload($tmpId, $this->getId()) != FTP_FINISHED) {
			   $res = false;
			}
			$tmp->delete();
			
			return ($res);
		}
		
		/**
		 * Get the buffer value
		 * 
		 * @param	array $options = null
		 * @return	string
		 * @access	public
		 * @todo 	Set filter when get the buffer
		 */
		Function receive($options=null)
		{
			$tmpId = TMP.Utils::createId(10);
			
			if (!Files::touch(DOCUMENTROOT.$tmpId)) {
				return (false);
			}
			
			$res = null;
			$tmp = null;
			if ($this->download($this->getId(), $tmpId) == FTP_FINISHED) {
				$tmp = new FS($tmpId);
				$res = $tmp->receive($options);
			}
			else {
				$tmp = new FS($tmpId);
			}
			
			$tmp->delete();
			return ($res);
		}
		
		/**
		 * Send file to remote server over FTP
		 * 
		 * @param	string	$source
		 * @param	string	$target
		 * @return	int
		 * @access	public
		 */
		Function upload($source, $target)
	    {
	    	$pipe = ftp_nb_put($this->_connection, $target, DOCUMENTROOT.$source, FTP_BINARY, FTP_AUTORESUME);
			while ($pipe == FTP_MOREDATA) {
				$pipe = ftp_nb_continue($this->_connection);
			}
			return ($pipe);
	    }
		
		/**
		 * Get remote file over FTP
		 * 
		 * @param	string	$source
		 * @param	string	$target
		 * @return	int
		 * @access	public
		 */
		Function download($source, $target)
	    {
	    	$target = DOCUMENTROOT.$target;
			unlink($target);
			Files::touch($target);
			$pipe = ftp_nb_get($this->_connection, $target, $source, FTP_BINARY, FTP_AUTORESUME);
			while ($pipe == FTP_MOREDATA) {
				$pipe = ftp_nb_continue($this->_connection);
			}
			return ($pipe);
	    }
		
		/**
		 * Execute FTP raw command
		 * 
		 * @param	string $cmd
		 * @return	string
		 * @access	public
		 */
		Function raw($cmd)
		{
			
		}
}

?>