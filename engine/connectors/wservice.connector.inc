<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Connector
 * @package		core.engine.connectors
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Connectors;
use \Enska\Abstracts\Connector;
use	\Enska\Services\Utils;
use	\Enska\Helpers\DataStore;
 
class WSERVICE extends Connector
{
	/**
	 * Modes
	 * @type	string
	 */
	const		_SET = 'set';
	const		_RESET = 'reset';

	/**
	 * Web service instance
	 * @type	ressource
	 */
	Private		$_curl;
	
	/**
	 * Web service post variables
	 * @type	string
	 */
	Private		$_post;
	
	/**
	  * Header messages
	  * @type	array
	  */
	 Private	$headerMessages = array(
		"100"	=> "Continue",
		"101"	=> "Switching Protocols",
		
		"200"	=> "OK",
		"201"	=> "Created",
		"202"	=> "Accepted",
		"203"	=> "Non-Authoritative Information",
		"204"	=> "No Content",
		"205"	=> "Reset Content",
		"206"	=> "Partial Content",
		
		"300"	=> "Multiple Choices",
		"301"	=> "Moved Permanently",
		"302"	=> "Found",
		"303"	=> "See Other",
		"304"	=> "Not Modified",
		"305"	=> "Use Proxy",
		"306"	=> "(Unused)",
		"307"	=> "Temporary Redirect",
		
		"400"	=> "Bad Request",
		"401"	=> "Unauthorized",
		"402"	=> "Payment Required",
		"403"	=> "Forbidden",
		"404"	=> "Not Found",
		"405"	=> "Method Not Allowed",
		"406"	=> "Not Acceptable",
		"407"	=> "Proxy Authentication Required",
		"408"	=> "Request Timeout",
		"409"	=> "Conflict",
		"410"	=> "Gone",
		"411"	=> "Length Required",
		"412"	=> "Precondition Failed",
		"413"	=> "Request Entity Too Large",
		"414"	=> "Request-URI Too Long",
		"415"	=> "Unsupported Media Type",
		"416"	=> "Requested Range Not Satisfiable",
		"417"	=> "Expectation Failed",
		
		"500"	=> "Internal Server Error",
		"501"	=> "Not Implemented",
		"502"	=> "Bad Gateway",
		"503"	=> "Service Unavailable",
		"504"	=> "Gateway Timeout",
		"505"	=> "HTTP Version Not Supported"
	 );
		
	Public
		/**
		 * Constructor
		 * 
		 * @param	string	$id = null
		 * @param	array 	$datas = null
		 * @return	WSERVICE
		 * @access	public
		 */
		Function __construct($id = null, $datas = null)
		{
			parent::__construct($id, $datas);
			$this->open();
		}
		
		/**
		 * Open the connexion
		 *
		 * @return	bool
		 * @access	public
		 */
		Function open()
		{
			$this->_init();
			if (!Utils::isEmpty($this->getId(), Utils::_STRING_)) {
				$this->store()->url = $this->getId();
				$this->setUrl($this->getId());
			}
			
			$this->store()->_infos_ = new DataStore();
			$this->store()->_post_ = new DataStore();
			$this->store()->_isAuth = false;

			$this->setOption(CURLOPT_FOLLOWLOCATION, 1);
			
			//Logs::add('WSERVICE');
		}
		
		/**
		 * Close the connexion
		 *
		 * @return	void
		 * @access	public
		 */
		Function close()
		{
			if ($this->_curl) {
				curl_close($this->_curl);
			}
		}
		
		/**
		 * Set the buffer value
		 * 
		 * @param	string	$buffer = null
		 * @param	array 	$options = null
		 * @return	array
		 * @access	public
		 */
		Function send($buffer=null, $options=null)
		{
			if (!Utils::isEmpty($buffer, Utils::_STRING_)) {
				$this->addPost('__buffer', $buffer);
			}
			
			$this->setCredentials(WSERVICE::_RESET);
			if (isset($options['credentials'])) {
				$this->setCredentials(WSERVICE::_SET, $filters['credentials']);
				unset($options['credentials']);
			}

			if (isset($options['post'])) {
				$this->addPost($options['post']);
				$this->deleteOption(CURLOPT_FOLLOWLOCATION);
			}
			
			if (isset($options['file'])) {
				$this->addFile(($options['file']));
			}

			return ($this->run());
			//return ($this->_exec());
		}
		
		/**
		 * Get the buffer value
		 * 
		 * @param	array $filter = null
		 * @return	array
		 * @access	public
		 * @todo 	Set filter when get the buffer
		 */
		Function receive($filters=null)
		{
			$this->setCredentials(WSERVICE::_RESET);
			if (isset($filters['credentials'])) {
				$this->setCredentials(WSERVICE::_SET, $filters['credentials']);
				unset($options['credentials']);
			}
			return ($this->_exec());
		}
		
		/**
		 * Set resource url
		 *
		 * @param	string $url
		 * @return	void
		 * @access	public
		 */
		Function setUrl($url)
		{
			$this->store()->url = $url;
			$this->setId($this->store()->url);
			$this->setOption(CURLOPT_URL, $this->getId());
		}
		
		/**
		 * Set a resource option
		 *
		 * @param	int	$attr
		 * @param	mixed $value
		 * @return	void
		 * @access	public
		 */
		Function setOption($attr, $value)
		{
			if (!curl_setopt($this->_curl, $attr, $value)) {
				Logs::error(array('throw' => E_USER_NOTICE, 'message' => __METHOD__.': '.curl_error($this->_curl), 'file' => __FILE__, 'line' => (__LINE__ - 1), 'code' => '003x20001', 'setOption'), __CLASS__);
			}
		}

		/**
		 * Delete a resource option
		 *
		 * @param	int $attr
		 * @return 	void
		 * @access	public
		 */
		Function deleteOption($attr)
		{
			if (!curl_setopt($this->_curl, $attr, '')) {
				Logs::error(array('throw' => E_USER_NOTICE, 'message' => __METHOD__.': '.curl_error($this->_curl), 'file' => __FILE__, 'line' => (__LINE__ - 1), 'code' => '003x20003', 'deleteOption'), __CLASS__);
			}
		}
		
		/**
		 * Add a POST item
		 *
		 * @param	mixed	$datas
		 * @param	string	$value = null
		 * @return 	void
		 * @access	public
		 */
		Function addPost($datas, $value=null)
		{
			if ((!is_array($datas)) && (!Utils::isEmpty($value))) {
				$this->store()->_post_->$datas = $value;
			}
			elseif (is_array($datas)) {
				$this->store()->_post_->sets($datas);
			}
			elseif (is_object($datas, 'DataStore')) {
				$this->store()->_post_->sets($datas->gets());
			}

			if (Utils::isArray(($p = $this->store()->_post_->gets()))) {
				$this->_post = '';
				$nb = count($p);
				foreach($p as $key => $value) { $this->_post .= $key.'='.$value.'&'; }
				$this->_post = rtrim($this->_post, '&');
				$this->setOption(CURLOPT_POST, $nb);
				$this->setOption(CURLOPT_POSTFIELDS, $this->_post);
			}
		}

		/**
		 * Clear the POST array
		 *
		 * @return 	void
		 * @access	public
		 */
		Function resetPost()
		{
			$this->store()->_post_->clear();
			$this->deleteOption(CURLOPT_POST);
			$this->deleteOption(CURLOPT_POSTFIELDS);
		}

		/**
		 * Add a POST File
		 *
		 * @param	array 	$file // array(file=>'filePath', name=>'Filename');
		 * @return 	bool
		 * @access	public
		 */
		Function addFile($file)
		{
			if (is_array($file)) {
				if (!is_file($file['file'])) {
					$file['file'] = DOCUMENTROOT.$file['file'];
				}
				else {
					$this->store()->_files_->set($file['name'], '@'.realpath($file['file']));
					return (true);
				}
			}
			return (false);
		}

		/**
		 * Clear the POST File
		 *
		 * @param	string $filename = null
		 * @return 	void
		 * @access	public
		 */
		Function resetFiles($filename=null)
		{
			if (!Utils::isEmpty($filename)) {
				$this->store()->_files_->delete($filename);
			}
			else {
				$this->store()->_files_->clear();
			}
		}

		/**
		 * Set / Reset the authentification informations
		 *
		 * @param	string $auth = WSERVICE::_SET_
		 * @param	string $credentials = null
		 * @return 	void
		 * @access	public
		 */
		Function setCredentials($mode=WSERVICE::_SET, $credentials=null)
		{
			if (strcmp($mode, WSERVICE::_SET) == 0) {
				if (Utils::isCURLCredentials($credentials)) {
					$this->setOption(CURLOPT_USERPWD, $credentials);
					$this->store()->set('_isAuth', true);
					return;
				}
				Logs::error(array('throw' => E_USER_NOTICE, 'message' => __METHOD__.': Credentials have bad format', 'file' => __FILE__, 'line' => (__LINE__ - 1), 'code' => '003x20004', 'setCredentials'), __CLASS__);
			}
			$this->store()->set('_isAuth', false);
			$this->deleteOption(CURLOPT_USERPWD);
		}
		
		/**
		 * Get resource executed content
		 *
		 * @param	$out = TRUE
		 * @return	mixed
		 * @access	public
		 */
		function getContent($out=TRUE)
		{
			$this->setOption(CURLOPT_NOBODY, FALSE);
			$this->setOption(CURLOPT_HEADER, FALSE);
			$this->setOption(CURLOPT_RETURNTRANSFER, $out);

			return ($this->run());
		}

		/**
		 * Get a remote file
		 *
		 * @param	string $target
		 * @return 	bool
		 * @access	public
		 */
		Function getFile($target)
		{
			$fd = @fopen(DOCUMENTROOT.$target, 'w');
			if (!$fd) {
				return (false);
			}

			$this->setOption(CURLOPT_NOBODY, FALSE);
			$this->setOption(CURLOPT_HEADER, FALSE);
			$this->setOption(CURLOPT_RETURNTRANSFER, TRUE);
			$this->setOption(CURLOPT_FILE, $fd);
			$this->run();
			fclose($fd);

			if ($this->_isExists($this->getMeta('http_code'))) {
				return (true);
			}
			return (false);
		}

		/**
		 * Check if url exist
		 *
		 * @param	int $code
		 * @return 	bool
		 * @access	public
		 */
		Function _isExists($code)
		{
			if ($code == 200) {
				return (true);
			}
			return (false);
		}
		
		/**
		 * Check if url respond
		 *
		 * @param	int $code
		 * @return 	bool
		 * @access	public
		 */
		Function _isAvailable($code)
		{
			if (($code >= 200 && $code <=206) && ($code >= 300 && $code <=307)) {
				return (true);
			}
			return (false);
		}

		/**
		 * Get headers
		 *
		 * @return 	bool
		 * @access	public
		 */
		Function getHeader()
		{
			$this->setOption(CURLOPT_NOBODY, TRUE);
			$this->setOption(CURLOPT_HEADER, TRUE);
			$this->setOption(CURLOPT_RETURNTRANSFER, TRUE);
			$this->run();

			if ($this->_isAvailable($this->getMeta('http_code'))) {
				return (true);
			}
			return (false);
		}
		
		/**
		 * Execute the resource request
		 *
		 * @return	mixed
		 * @access	public
		 */
		Function run()
		{
			if (!($this->store()->_res = curl_exec($this->_curl))) {
				//Logs::error(array('throw' => E_USER_NOTICE, 'message' => __METHOD__.': '.curl_error($this->_curl), 'file' => __FILE__, 'line' => (__LINE__ - 1), 'code' => '003x20002', 'run'), __CLASS__);
			}
			$this->_setMeta();
			return ($this->store()->_res);
		}
		
		/**
		 * Get resource infos
		 *
		 * @param	string $attr
		 * @return	mixed
		 * @access	public
		 */
		Function getMeta($attr=null)
		{
			if (!Utils::isEmpty($attr, Utils::_STRING_)) {
				return ($this->store()->_infos_->get($attr));
			}
			else {
				return ($this->store()->_infos_->gets());
			}
		}
		
	Protected
		/**
		 * Exec abstract
		 * 
		 * @return	array
		 * @access	protected
		 */
		Function _exec()
		{
			$res = array(
				'header' => $this->getHeader(),
				'content' => $this->getContent(),
				'meta' => $this->getMeta()
			);

			return ($res);
		}	
	
	Private
		/**
		 * Set the last request informations
		 *
		 * @return	void
		 * @access	private
		 */
		Function _setMeta()
		{
			$this->store()->_infos_->sets(curl_getinfo($this->_curl));
		}
		
	Private
		/**
		 * Init the CURL component
		 *
		 * @return	void
		 * @access	private
		 */
		Function _init()
		{
			$this->_curl = curl_init();

			if (!Utils::isPositive($this->store()->timeout)) {
				$this->store()->timeout = 10;
			}

			if (!Utils::isPositive($this->store()->connection_timeout)) {
				$this->store()->connection_timeout = 10;
			}

			$this->setOption(CURLOPT_TIMEOUT, $this->store()->timeout);
			$this->setOption(CURLOPT_CONNECTTIMEOUT, $this->store()->connection_timeout);
		}
}

?>
