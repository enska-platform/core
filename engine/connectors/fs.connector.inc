<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Connector
 * @package		core.engine.connectors
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 * @todo		Implements Mutex, errors, force reload
 */
 
namespace Enska\Connectors;
use \Enska\Abstracts\Connector;
use \Enska\Services\Utils;
use \Enska\Services\Logs;

class FS extends Connector
{	
	/**
	 * Mode constante
	 * @type	CONSTANTE
	 */
	const		_ADD = 0;
	const		_AFTER = 1;
	const		_BEFORE = 2;
	const		_NEW = 3;
	
	/**
	 * Filename
	 * @type	string
	 */
	Protected	$_file = null;
	
	/**
	 * Remote file descriptor
	 * @type	resource
	 */
	Protected	$_rfd = null;
	
	/**
	 * File mutex
	 * @type	string
	 */
	Protected	$_mutex = null;
	
	/**
	 * File flag
	 * @type	string
	 */
	Protected	$_flag = 'r';
	
	/**
	 * File mode
	 * @type	bool
	 */
	Protected	$_remote = FALSE;
	
	/**
	 * Backuped file buffer
	 * @type	string
	 */
	Protected	$_rollback = null;
	
	/**
	 * Reader flags
	 * @type	array
	 */
	Protected	$_arrayReadFlag = array ('r', 'x');

	/**
	 * Writer flags
	 * @type	array
	 */
	Protected	$_arrayWriteFlag = array ('r+', 'w', 'w+', 'a', 'a+', 'x+', 'c', 'c+');
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	string	$file
		 * @param	array 	$datas = null
		 * @return	FS
		 * @access	public
		 */
		Function __construct($file, $datas=null)
		{	
			if (is_string($file)) {
				$this->_file = $file;
				$flag = isset($datas['flag']) ? $datas['flag'] : 'r';
				$mutex = isset($datas['mutex']) ? $datas['mutex'] : 0; //Mutex::_LOCK;
				$remote = isset($datas['remote']) ? $datas['remote'] : FALSE;
				$raw = isset($datas['raw']) ? $datas['raw'] : FALSE;
				$I = isset($datas['I']) ? $datas['I'] : FALSE;
				
				if ($remote !== TRUE) {
					if (!Utils::isEmpty($I) && $I !== false) {
						$I = trim($I);
						$I = rtrim($I, '/');
						$I = rtrim($I, '\\');
						$this->_file = $I.'/'.$file;
					}
				}
				
				$this->_flag = $flag;
				$this->_mutex = $mutex;
				$this->_remote = $remote;
				$this->_raw = $raw;
				parent::__construct($file, $datas);
				$this->open();
			}
		}
		
		/**
		 * Open the file
		 *
		 * @return	bool
		 * @access	public
		 */
		Function open()
		{
			if ($this->_remote !== TRUE) {
				if ((!file_exists($this->_file))) { //(!in_array($this->_flag, $this->_arrayWriteFlag))
					$this->_file = DOCUMENTROOT.$this->_file;
					if (!file_exists($this->_file)) {
						//Logs::errors(E_USER_WARNING, 'The file: '.$this->_file.', does not exist', __FILE__, 0, '002x20005', 'file');
						$this->error('The file: '.$this->_file.', does not exist');
						if (strcmp($this->_flag, 'w')) { return (false); }
					}
				}
			}
			
			$this->_rfd = fopen($this->_file, $this->_flag);
			if (!$this->_rfd) {
				//Logs::errors(E_USER_WARNING, 'Unable to open the file: '.$this->_file.' - flag: '.$this->_flag, __FILE__, 0, '002x20001', 'file');
				$this->error('Unable to open the file: '.$this->_file.' - flag: '.$this->_flag);
				return (false);
			}
			
			
			
			$this->_buffer = file_get_contents($this->_file);
			$this->_rollback = $this->_buffer;
			$this->close();
			return (true);
		}
		
		/**
		 * Close the file
		 *
		 * @return	void
		 * @access	public
		 */
		Function close()
		{
			fclose($this->_rfd);
		}
		
		/**
		 * Get the buffer value
		 * 
		 * @param	array $options = null
		 * @return	string
		 * @access	public
		 * @todo 	Set filter when get the file buffer
		 */
		Function receive($options=null)
		{
			return ($this->_buffer);
		}
		
		/**
		 * Set the buffer value
		 * 
		 * @param	string	$buffer = null
		 * @param	array 	$options = null
		 * @return	bool
		 * @access	public
		 * @todo	Implements options: errors
		 */
		Function send($buffer=null, $options=null)
		{
			if ($this->_remote === TRUE && $this->_raw === FALSE) {
				if (!file_exists($buffer)) {
					$buffer = DOCUMENTROOT.$buffer;
					if (!file_exists($buffer)) {
						//Logs::errors(E_USER_WARNING, 'The file: '.$this->_file.', does not exist', __FILE__, 0, '002x20005', 'file');
						return (false);
					}
				}
				$buffer = file_get_contents($buffer);
			}
			
			$force = isset($options['force']) ? $options['force'] : FALSE;
			$mode = isset($options['mode']) ? $options['mode'] : FS::_ADD;
			$save = isset($options['save']) ? $options['save'] : FALSE;
			$filters = isset($options['filters']) ? $options['filters'] : null;	
			if (is_string($buffer)) {
				$backupBuffer = null;
				if ($mode == FS::_ADD) { return ($this->buffer($this->_buffer.$buffer, $save, $force, $filters)); }
				elseif ($mode == FS::_AFTER) { return ($this->buffer($this->_buffer.$buffer, $save, $force, $filters)); }
				elseif ($mode == FS::_BEFORE) { $backupBuffer = $this->_buffer; return ($this->buffer($buffer.$backupBuffer, $save, $force, $filters)); }
				elseif ($mode == FS::_NEW) { return ($this->buffer($buffer, $save, $force, $filters)); }
			}
			return (false);
		}
		
		/**
		 * Get file meta
		 * 
		 * @return	DataStore
		 * @access	public
		 * @todo	implements
		 */
		Function meta()
		{
			return (null);
		}
		
		/**
		 * Reload the current file buffer
		 *
		 * @return	void
		 * @access	public
		 */
		Function rollback()
		{
			$this->_buffer = $this->_rollback;
			return ($this);
		}
		
		/**
		 * Delete the file
		 *
		 * @return	int
		 * @access	public
		 */
		Function delete()
		{
			return (unlink($this->_file));
		}
		
	Protected
		/**
		 * Set the buffer value
		 * 
		 * @param	string $buffer = null;
		 * @param	bool $save = false
		 * @param	bool $force = false
		 * @param	array $filters = null
		 * @return	bool
		 * @access	protected
		 * @todo	Filters
		 */
		Function buffer($buffer=null, $save=false, $force=false, $filters=null)
		{
			//unset($this->_buffer);
			//$this->_buffer = Filters::filter($buffer, $filters);
			
			$this->_buffer = $buffer;
			if (!Utils::isEmpty($save, Utils::_BOOL_)) { return ($this->save($force)); }
			return (true);
		}
		
	Protected
		/**
		 * Save the current opened file
		 *
		 * @param	bool $force = false
		 * @return	bool
		 * @access	protected
		 */
		Function save($force=false)
		{		
			if (!Utils::isEmpty($force, Utils::_BOOL_)) { $this->_flag = 'w'; }
			
			// Check the flag
			if (!in_array($this->_flag, $this->_arrayWriteFlag)) {
				Logs::errors(E_USER_NOTICE, 'Can not save the file: '.$this->_file.', with the current flag: '.$this->_flag, __FILE__, 0, '002x20002', 'file');
				return (false);
			}
			
			if ($this->_remote !== TRUE) {
				// Check if the file exists
				if ((!in_array($this->_flag, $this->_arrayWriteFlag)) && (!file_exists($this->_file))) {
					if (!file_exists(ltrim($this->_file, '../'))) {
						Logs::errors(E_USER_WARNING, 'The file does not exist: '.$this->_file.' - flag: '.$this->_flag, __FILE__, 0, '002x20010', 'file');
						return (false);
					}
				}
				
				// Check if target is writable
				if ((!in_array($this->_flag, $this->_arrayWriteFlag)) && (!is_writable($this->_file))) {
					Logs::errors(E_USER_WARNING, 'You don\'t  have enought rights to write on: '.$this->_file.' - flag: '.$this->_flag, __FILE__, 0, '002x20005', 'file');
					return (false);
				}
			}
			
			// Try to open the file
			$this->_rfd = fopen($this->_file, $this->_flag);
			if (!$this->_rfd) {
				Logs::error(E_USER_WARNING, 'Unable to open the file: '.$this->_file.' - flag: '.$this->_flag, __FILE__, 0, '002x20003', 'file');
				return (false);
			}
			
			// Try to write in file
			if (!fputs($this->_rfd, $this->_buffer)) {
				//Logs::errors(E_USER_WARNING, 'Unable to save the file: '.$this->_file.' - flag: '.$this->_flag, __FILE__, 0, '002x20004', 'file');
				$this->error('Unable to save the file');
				return (false);
			}
			
			$this->close();
			return (true);
		}
}

?>