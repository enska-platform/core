<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Connector
 * @package		core.engine.connectors
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Connectors;
use \Enska\Abstracts\Connector;
 
class SSH extends Connector
{
	/**
	 * SSH coonexion
	 * @type	sshd
	 */
	Protected	$_connection = null;
	
	/**
	 * SSH finger print
	 * @type	string
	 */
	Protected	$_fp = null;
	
	/**
	 * SSH authentification
	 * @type	sshdh
	 */
	Protected	$_auth = null;
	
	/**
	 * SSH command
	 * @type	sshdc
	 */
	Protected	$_cmd = '';
	
	/**
	 * SSH returned error
	 * @type	string
	 */
	Protected	$__stderr = null;
		
	Public
		/**
		 * Constructor
		 * 
		 * @param	string	$id = null
		 * @param	array 	$datas = null
		 * @return	SSH
		 * @access	public
		 */
		Function __construct($id = null, $datas = null)
		{
			parent::__construct($id, $datas);
			return ($this->open());
		}
		
		/**
		 * Open the connexion
		 *
		 * @return	bool
		 * @access	public
		 */
		Function open()
		{
			if (($this->_connection = ssh2_connect($this->store()->hostname, $this->store()->port))) {
				return ($this->authenticate());
			}
			$this->_error = 'Error on connexion: '.debug::pr($this->_connection, true);
			return(false);
		}
		
		/**
		 * Close the connexion
		 *
		 * @return	void
		 * @access	public
		 */
		Function close()
		{
			if ($this->_connection) {
				@fclose($this->_cmd);
				@fclose($this->__stderr);
			}
		}
		
		/**
		 * Set the buffer value
		 * 
		 * @param	string	$buffer = null
		 * @param	array 	$options = null
		 * @return	bool
		 * @access	public
		 */
		Function send($buffer=null, $options=null)
		{
			$this->_error = 'Any connexion open';
			if ($this->_connection) {
				$this->_error = null;
				if (!Utils::isEmpty(($this->_cmd = ssh2_exec($this->_connection, $buffer)), Utils::_BOOL_)) {
					return ($this->handler());
				}
				$this->_error = 'Error on CMD: '.debug::pr($buffer, true);
			}
			return (false);
		}
		
		/**
		 * Get the buffer value
		 * 
		 * @param	array $filter = null
		 * @return	string
		 * @access	public
		 * @todo 	Set filter when get the buffer
		 */
		Function receive($filters=null)
		{
			return (array('stderr' => $this->store()->stderr, 'stdout' => $this->store()->stdout));
		}
		
		/**
		 * Check the server FingerPrint
		 * 
		 * @return	bool
		 * @access	public
		 */
		Function isValidFP()
		{
			$this->_error = 'Any connexion open';
			if ($this->_connection) {
				$this->_fp = ssh2_fingerprint($this->_connection, SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX);
				if ($this->store()->fingerPrint) {
					$this->_error = 'FingerPrint not match';
					if (strcmp($this->store()->fingerPrint, $this->_fp) == 0) {
						$this->_error = null;
						return(true);
					}
				}
			}
			return (false);
		}
		
	Protected
		/**
		 * Authenticate the user
		 * 
		 * @return	bool
		 * @access	protected
		 */
		Function authenticate()
		{
			$this->_error = 'Any connexion open';
			if ($this->_connection) {
				$this->_error = 'Any authentification information is set';
				if ($this->store()->login && $this->store()->password) {
					$this->_error = null;
					if (!Utils::isEmpty(($this->_auth = ssh2_auth_password($this->_connection, $this->store()->login, $this->store()->password)), Utils::_BOOL_)) {
						return (true);
					}
					$this->_error = 'Error on authentification. Bad login/password';
				}
			}
			return (false);
		}
		
	Protected
		/**
		 * Retreive STDIO
		 * 
		 * @return	bool
		 * @access	protected
		 */
		Function handler()
		{
			$this->_error = 'Any connexion open';
			if ($this->_connection && $this->_cmd) {
				$this->_error = null;
				$this->_stdout();
				$this->_stderr();
				$this->close();
				return (true);
			}
			return (false);
		}
		
	Protected
		/**
		 * Retreive STDOUT
		 *
		 * @return	void
		 * @access	protected
		 */
		Function _stdout()
		{
			$this->store()->stdout = '';
			stream_set_blocking($this->_cmd, true);
			while($stdin = fgets($this->_cmd)) {
				$this->store()->stdout = $this->store()->stdout.$stdin."\n";
			}
		}
		
	Protected
		/**
		 * Retreive STDERR
		 *
		 * @return	void
		 * @access	protected
		 */
		Function _stderr()
		{
			$this->__stderr = ssh2_fetch_stream($this->_cmd, SSH2_STREAM_STDERR);
			stream_set_blocking($this->__stderr, true);
			$this->store()->stderr = '';
			while($stderr = fgets($this->__stderr)) {
				$this->store()->stderr = $this->store()->stderr.$stderr."\n";
			}
		}
}

?>