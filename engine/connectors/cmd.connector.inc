<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Connector
 * @package		core.engine.connectors
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Connectors;

use \Enska\Abstracts\Connector;
use	\Enska\Helpers\DataStore;
use	\Enska\Services\Utils;
 
class CMD extends Connector
{
	/**
	 * CMD standard status
	 * @type	int
	 */
	Protected	$_status = SUCCESS;
	
	/**
	 * CMD standard output
	 * @type	array
	 */
	Protected	$_stdout = null;
	
	/**
	 * CMD command
	 * @type	string
	 */
	Protected	$_stdin = '';
	
	/**
	 * Shell instance
	 * @type	mixed
	 */
	Protected	$_cmd = '';
		
	Public
		/**
		 * Constructor
		 * 
		 * @param	string	$id = null
		 * @param	array 	$datas = null
		 * @return	SSH
		 * @access	public
		 */
		Function __construct($id = null, $datas = null)
		{
			parent::__construct($id, $datas);
			return ($this->open());
		}
		
		/**
		 * Open the connexion
		 *
		 * @return	this
		 * @access	public
		 */
		Function open()
		{
			$this->_status = 0;
			$this->_stdin = '';
			$this->_stdout = array();
			return ($this);
		}
		
		/**
		 * Close the connexion
		 *
		 * @return	void
		 * @access	public
		 */
		Function close() {}
		
		/**
		 * Set the buffer value
		 * 
		 * @param	string	$buffer = null
		 * @param	array 	$options = null
		 * @return	this
		 * @access	public
		 */
		Function send($buffer=null, $options=null)
		{
			$this->_stdin = $buffer;
			// $this->_cmd = exec(DOCUMENTROOT . 'bin/src/bin/' .$this->_stdin, $this->_stdout, $this->_status);
			$this->_cmd = system(DOCUMENTROOT . 'bin/src/bin/' .$this->_stdin, $this->_status);
			$this->_stdout = $this->_cmd;
			return ($this);
		}
		
		/**
		 * Execute an user script
		 * 
		 * @param	string	$sh
		 * @param	string 	$params = null
		 * @return	this
		 * @access	public
		 */
		Function usr($sh, $params=null)
		{
			return ($this->send('usr/'.$sh.' '.$params));
		}
		
		/**
		 * Execute an install script
		 * 
		 * @param	string	$sh
		 * @param	string 	$params = null
		 * @return	this
		 * @access	public
		 */
		Function install($sh, $params=null)
		{
			return ($this->send('libs/install_'.$sh.' '.$params));
		}
		
		/**
		 * Execute a deploy script
		 * 
		 * @param	string	$sh
		 * @param	string 	$params = null
		 * @return	this
		 * @access	public
		 */
		Function deploy($sh, $params=null)
		{
			return ($this->send('libs/deploy_'.$sh.' '.$params));
		}
		
		/**
		 * Execute an uninstall script
		 * 
		 * @param	string	$sh
		 * @param	array 	$params = null
		 * @return	this
		 * @access	public
		 */
		Function uninstall($sh, $params=null)
		{
			return ($this->send('libs/uninstall_'.$sh.' '.$params));
		}
		
		/**
		 * Execute a set script
		 * 
		 * @param	string	$sh
		 * @param	string 	$params = null
		 * @return	this
		 * @access	public
		 */
		Function set($sh, $params=null)
		{
			return ($this->send('libs/set_'.$sh.' '.$params));
		}
		
		/**
		 * Get the buffer value
		 * 
		 * @param	array $filter = null
		 * @return	string
		 * @access	public
		 * @todo 	Set filter when get the buffer
		 */
		Function receive($filters=null)
		{
			return (DataStore::create(array('stdout' => $this->_stdout, 'status' => $this->_status)));
		}
		
		static Function usage($path)
		{
			$cmd = new self();
			$cmd->send('usages/'.$path);
			if (($cmd->receive()->stdout instanceof DataStore) && Utils::isArray($cmd->receive()->stdout->gets())) {
				$path = '';
				foreach ($cmd->receive()->stdout->gets() as $out) { $path .= $out."\n"; }
			}
		}
		
		/**
		 * Reboot the current local machine
		 * 
		 * @return	void
		 * @access	public
		 */
		static Function reboot() { exec('shutdown -r now'); }
		
		/**
		 * Execute and get result from command
		 * 
		 * @param	string	$cmd = null
		 * @param	bool	$visible = true
		 * @return	DataStore
		 * @access	public
		 */
		static Function run($cmd=null, $visible=true)
		{
			$result = null; $exit = 0;
			//if ($visible === false) { $cmd = 'exec > /dev/null 2>&1 && '.$cmd; }
			@exec($cmd, $result, $exit);
			return (DataStore::create(array('cmd' => $cmd, 'result' => $result, 'exit' => $exit)));
		}
		
		/**
		 * Execute and display result from command
		 * 
		 * @param	string	$cmd = null
		 * @param	bool	$visible = true
		 * @return	DataStore
		 * @access	public
		 */
		static Function _($cmd=null, $visible=true)
		{
			$exit = 0;
			@system($cmd, $exit);
		}
		
		static Function bin($cmd, $visible=true)
		{
			$cmd = DOCUMENTROOT.'bin/src/bin/'.$cmd; $result = null; $exit = 0;
			@system($cmd.$result, $exit);
			return (DataStore::create(array('cmd' => $cmd, 'result' => $result, 'exit' => $exit)));
		}
		
		/**
		 * Write to STDOUT
		 * 
		 * @param	string	$str
		 * @param	bool	$echap = true
		 * @return	void
		 * @access	public
		 */
		static Function console($str, $echap=true)
		{
			if ($echap) { echo "$str"."\n"; }
			else { echo "$str"; }
		}
}

?>