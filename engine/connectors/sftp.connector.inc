<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Connector
 * @package		core.engine.connectors
 * @copyright	Copyright (c) 2008 - 2016 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 * @todo		Implements errors, ls, rm, meta, cp, archive transfert
 */

namespace Enska\Connectors;

class SFTP extends SSH
{
	/**
	 * SFTP coonexion
	 * @type	sftpd
	 */
	Protected	$_sconnection = null;
		
	Public
		/**
		 * Constructor
		 * 
		 * @param	string	$id = null
		 * @param	array 	$datas = null
		 * @return	SFTP
		 * @access	public
		 */
		Function __construct($id = null, $datas = null)
		{
			if (parent::__construct($id, $datas)) {
				return ($this->_open());
			}
		}
		
		/**
		 * Send file to remote server over SSH
		 * 
		 * @param	string	$source
		 * @param	string	$target
		 * @return	bool
		 * @access	public
		 */
		Function upload($source, $target)
	    {
	    	$pipe = new FS('ssh2.sftp://'.$this->_sconnection.$target, array('remote'=>true, 'flag'=>'w'));
			return ($pipe->send($source, array('save'=>true, 'force'=>true, 'mode'=>FS::_NEW)));
	    }
		
		/**
		 * Get remote file over SSH
		 * 
		 * @param	string	$source
		 * @param	string	$target
		 * @return	bool
		 * @access	public
		 */
		Function download($source, $target)
	    {
	    	$pipe = new FS('ssh2.sftp://'.$this->_sconnection.$source, array('remote'=>true, 'flag'=>'r'));
			$local = new FS($target, array('flag'=>'w'));
			$local->send($pipe->receive(), array('force'=>true, 'mode'=>FS::_NEW, 'save'=>true));
	    }
		
		/**
		 * Read remote file over SSH
		 * 
		 * @param	string	$source
		 * @return	string
		 * @access	public
		 */
		Function cat($source)
	    {
	    	$pipe = new FS('ssh2.sftp://'.$this->_sconnection.$source, array('remote'=>true, 'flag'=>'r'));
			return ($pipe->receive());
	    }
		
		/**
		 * Write remote file over SSH
		 * 
		 * @param	string	$buffer
		 * @param	array	$options = null
		 * @return	void
		 * @access	public
		 */
		Function wecho($buffer, $options=null)
	    {
	    	$pipe = new FS('ssh2.sftp://'.$this->_sconnection.$this->getId(), array('remote'=>true, 'raw'=>true, 'flag'=>'w'));
			return ($pipe->send($buffer, $options));
	    }
		
		/**
		 * Close tunnel
		 * 
		 * @return	void
		 * @access	public
		 */
		Function quit()
	    {
			$this->close();
	    }
		
	Private
		/**
		 * Open the SFTP pipe
		 * 
		 * @return	bool
		 * @access	public
		 */
		Function _open()
		{
        	if (($this->_sconnection = @ssh2_sftp($this->_connection))) {
        		return (true);
        	}
			return (false);
		}
}

?>