<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Service
 * @package		engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Services;

use \Enska\Helpers\DataStore;
use	\Enska\Helpers\Observer;
use	\Enska\Helpers\Client;
use \Enska\Helpers\SServer;
use \Enska\Helpers\WSServer;

final class Server
{
	/**
	 * Server instance
	 * @type	Server
	 */
	Private	static	$_instance;
	
	/**
	 * Thread
	 * @var	WSServer | Server
	 */
	Private			$server = null;
	
	/**
	 * Clients instances
	 * @var	Observer
	 */
	Private			$clients = null;
	
	Public
		/**
		 * Start the Enska server
		 *
		 * @return	void
		 * @access	public
		 */
		static Function start()
		{
			$rt = Context::enska('rt');
			if (!$rt || (strcmp(strtolower($rt->server), 'sserver') && strcmp(strtolower($rt->server), 'wsserver'))) { throw new \Exception('Unable to found the server service :: '.$rt->server, 1); }
			if (self::access()->server) { $this->error('The server service are already started'); return; }
			$server = '\Enska\\Helpers\\'.$rt->server;
			self::access()->clients = new Observer('RTClients');
			try {
				self::access()->server = new $server(Utils::createId(50, true, true), $rt);
				self::access()->server->start()->listen();
			}
			catch (\Exception $e) {
				unset(self::access()->server);
				throw new \Exception('Unable to start the server service :: '.$rt->server.':: '.$e->getMessage(), 1);
			}
		}
		
		/**
		 * Stop the Enska server
		 *
		 * @return	void
		 * @access	public
		 */
		static Function stop()
		{
			if ((self::access()->server instanceof WSServer) || (self::access()->server instanceof SServer)|| (self::access()->server instanceof Server)) {
				self::access()->clients->broadcast(DataStore::create(array(
					'api'	=> 'disconnect'
				)));
				self::access()->server->stop();
			}
		}
		
		/**
		 * Connect a new client to server
		 *
		 * @param	socket	$socket
		 * @return	void
		 * @access	public
		 */
		static Function connect($socket)
		{
			self::access()->server->connect($socket);
			$clients = self::access()->clients;
			new Client(Utils::createId(50, true, true), array(
				'socket'	=> $socket,
				'timestamp'	=> Utils::timestamp(0, Utils::_ML),
				'observer'	=> &$clients
			));
		}
		
		/**
		 * End a client session
		 * 
		 * @param	socket	$socket = null
		 * @param	string	$clientId = null
		 * @return	void
		 * @access	public
		 */
		static Function disconnect($socket, $clientId=null)
		{
			if (!empty($clientId)) {
				self::access()->clients->broadcast(DataStore::create(array(
					'socket'	=> $socket,
					'api'		=> 'disconnect'
				)));
			}
			else if($socket && is_string($clientId) && strlen($clientId) === 32) {
				self::access()->server->connect($socket);
				self::access()->clients->close($clientId);
			}
		}

		/**
		 * Launch server handler
		 *
		 * @param	mixed		$client
		 * @param	string		$buffer = null
		 * @param	bool		$callback = false
		 * @return	void
		 * @access	public
		 */
		static Function handler($socket, $buffer=null, $callback=false)
		{
			if ($callback !== true) {
				self::access()->clients->broadcast(DataStore::create(array(
					'socket'	=> $socket,
					'buffer'	=> $buffer,
					'api'		=> 'handler'
				)));
				return;
			}
			self::access()->server->handler($socket, $buffer);
		}
		
		/**
		 * Broadcast to all clients
		 *
		 * @param	string	$buffer
		 * @return	void
		 * @access	public
		 */
		static Function broadcast($buffer)
		{
			self::access()->clients->broadcast(DataStore::create(array(
				'buffer'	=> $buffer,
				'api'		=> 'broadcast'
			)));
		}

	Private
		/**
		 * Constructor
		 *
		 * @return		
		 * @access	private
		 */
		Function __construct() {}
	
	Private
		/**
		 * Clone constructor
		 *
		 * @return	Server
		 * @access	private
		 */
		Function __clone() {}
		
	Private
		/**
		 * Get the Server instance
		 *
		 * @return	Server
		 * @access	private
		 */
		static Function access()
		{
			if (self::$_instance == null) { self::$_instance = new self(); }
			return (self::$_instance);
		}
}
 
?>