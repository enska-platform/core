<?php

/**
 *Copyright (C) 2008 - 2014  EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Helper
 * @package		core.helpers.enska
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Services;
use	\Enska\Helpers\DataStore;
 
final class Headers
{
	/**
	 * Header instance
	 * @type	Header
	 */
	Private	static	$_instance = null;
	
	/**
	 * Header items
	 * @type	DataStore
	 */
	Private	$_headers = null;
	
	Public
		/**
		 * Add a HTTP header
		 *
		 * @param	mixed $attr
		 * @param	mixed $val
		 * @return	void
		 */
		static Function add($attr, $val)
		{
			if (!Utils::isEmpty($attr)) {
				self::access()->_headers->$attr = $val;
			}
		}
		
		/**
		 * Get a HTTP header
		 *
		 * @param	mixed $attr
		 * @return	void
		 */
		static Function get($attr)
		{
			if (!Utils::isEmpty($attr)) {
				return (self::access()->_headers->$attr);
			}
		}
		
		/**
		 * Process adding header into the request
		 *
		 * @return	void
		 */
		static Function process()
		{
			$headers = self::access()->_headers->gets();
			if (Utils::isArray($headers)) {
				foreach ($headers as $key => $value) {
					header("$key".':'."$value");
				}
				self::access()->_headers->clear();
			}
		}

	Private
		/**
		 * Constructor
		 *
		 * @return	Headers
		 * @access	private
		 */
		Function __construct()
		{
			$this->_headers = new DataStore();
		}

	Private
		/**
		 * Clone constructor
		 *
		 * @return	Headers
		 * @access	private
		 */
		Function __clone()
		{
			$this->_headers = new DataStore();
		}
		
	Private
		/**
		 * Get the Header instance
		 *
		 * @return	Header
		 * @access	private
		 */
		static Function access()
		{
			if(self::$_instance == null) {
				self::$_instance = new self();
			}
			return (self::$_instance);
		}
}

?>