<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Service
 * @package		core.engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Services;
use \Enska\Interfaces\ISystem;
 
final class Auth implements ISystem
{
	Public
		/**
		 * Class loader
		 *
		 * @return	void
		 * @access	public
		 */
		static Function load()
		{
			Logs::add('auth');
			if (Config::get('settings') && Config::get('settings')->default && Config::get('settings')->default->auth !== true) { return; }
			$subDomain = Context::enska('subdomain');
			Context::set('authDSN', 'authentification');
			if ((!Utils::isEmpty($subDomain)) && file_exists(DOCUMENTROOT.ENSKA_CONF.'db/'.$subDomain.'.ini')) { Context::set('authDSN', $subDomain); }
		}

	Private
		/**
		 * Constructor
		 *
		 * @return	Auth
		 * @access	private
		 */
		Function __construct() {}
	
	Private
		/**
		 * Clone onstructor
		 *
		 * @return	Auth
		 * @access	private
		 */
		Function __clone() {}
		
	Private
		/**
		 * Get the Auth instance
		 *
		 * @return	Auth
		 * @access	private
		 */
		static Function access()
		{
			if(self::$_instance == null) { self::$_instance = new self(); }
			return (self::$_instance);
		}
}
 
 ?>