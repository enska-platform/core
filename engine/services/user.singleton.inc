<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Service
 * @package		engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Services;

final class User
{
	/**
	 * Users instance
	 * @type	Users
	 */
	Private	static	$_instance;
	
	/**
	 * Users last error
	 * @type	string
	 */
	Private			$_error = null;
	
	Public
		/**
		 * Check if a user is a root user
		 *
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function isRoot($access=null) { return(Users::isRoot(Sessions::getSession('uid'))); }
		
		/**
		 * Check if a user own a group
		 *
		 * @param	mixed 	$gid
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function isOwner($gid, $access=null) { return(Users::isOwner(Sessions::getSession('uid'), $gid, $access)); }
		
		/**
		 * Get user uid
		 *
		 * @param	string	$access = null
		 * @return	int
		 * @access	public
		 */
		static Function uid($access=null) { return(Users::getUid(Sessions::getSession('uid'), $access)); }
		
		/**
		 * Get user platformId
		 *
		 * @param	string	$access = null
		 * @return	int
		 * @access	public
		 * @todo	Get the right information
		 */
		static Function platformId($access=null) { return(self::uid($access)); }
		
		/**
		 * Get user login
		 *
		 * @param	string	$access = null
		 * @return	string
		 * @access	public
		 */
		static Function login($access=null) { return(Users::getLogin(Sessions::getSession('uid'), $access)); }
		
		/**
		 * Check if the user is in a specific group
		 * 
		 * @param	mixed	$gid
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function inGroup($gid, $access=null) { return(Users::inGroup(Sessions::getSession('uid'), $gid, $access)); }
		
		/**
		 * Get the latest users error
		 *
		 * @param	$error = null
		 * @return	string
		 * @access	public
		 */
		static Function error($error=null)
		{
			self::access()->_error = $error;
		}
	
		/**
		 * Get the latest users error
		 *
		 * @return	string
		 * @access	public
		 */
		static Function getLastError()
		{
			return (self::access()->_error);
		}

	Private
		/**
		 * Constructor
		 *
		 * @return	Users
		 * @access	private
		 */
		Function __construct() {}

	Private
		/**
		 * Clone constructor
		 *
		 * @return	Users
		 * @access	private
		 */
		Function __clone() {}

	Private
		/**
		 * Get the User instance
		 *
		 * @return	Users
		 * @access	private
		 */
		static Function access()
		{
			if(self::$_instance == null) { self::$_instance = new self(); }
			return (self::$_instance);
		}
}