<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Helper
 * @package		core.helpers.enska
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Services;
 
final class Utils
{
	/**
	 * Constantes Boolean type
	 * @type	CONSTANTE
	 */
	const	_BOOL_ = 0;
	
	/**
	 * Constantes Array type
	 * @type	CONSTANTE
	 */
	const	_ARRAY_ = 1;
	
	/**
	 * Constantes String type
	 * @type	CONSTANTE
	 */
	const	_STRING_ = 2;
	
	/**
	 * Constantes NUMERIC type
	 * @type	CONSTANTE
	 */
	const	_NUMERIC_ = 3;
	
	/**
	 * Constantes Datetime type
	 * @type	CONSTANTE
	 */
	const	_DATETIME_ = 4;
	
	/**
	 * Constantes Email type
	 * @type	CONSTANTE
	 */
	const	_EMAIL_ = 5;
	
	/**
	 * Constantes Domain type
	 * @type	CONSTANTE
	 */
	const	_DOMAIN_ = 6;

	/**
	 * Constantes Timestamp add type
	 * @type	CONSTANTE
	 */
	const	_ADD = 0;
	
	/**
	 * Constantes Timestamp remove type
	 * @type	CONSTANTE
	 */
	const	_REM = 1;
	
	/**
	 * Constantes Timestamp none type
	 * @type	CONSTANTE
	 */
	const	_NOW = 2;
	
	/**
	 * Constantes Timestamp miliseconds type
	 * @type	CONSTANTE
	 */
	const	_MS = 0;
	
	/**
	 * Constantes Timestamp microsecond type
	 * @type	CONSTANTE
	 */
	const	_ML = 1;
	
	/**
	 * Constantes Filters 
	 * @type	CONSTANTE
	 */
	const			_HTML = 0;
	const			_HTML_ENT_QUOTE = 1;
	const			_URL_ENC = 2;
	const			_URL_DEC = 6;
	const			_HTML_DECODE = 3;
	const			_HTML_SPE_DECODE = 10;
	const			_HTML_SPE = 4;
	const			_UTF_8 = 5;
	const			_BASE_64 = 7;
	const			_B64_ENC = 8;
	const			_B64_DEC = 9;
	
	Public
		/**
		 * Create a ramdom string
		 *
		 * @param	int $size=5
		 * @return	int | null
		 * @access	public
		 */
		static Function createId($size=5, $strong=false, $remd5=false)
		{
			$str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
			if ($strong === true) {
				$str .= '.[]=&^$~{}()!%<>?|/:-_`*;+,#èéàùç';
			}
			
			if ($size) {
				$id = null;
				srand((double)microtime()*1000000);
				for($i=0; $i<$size; $i++) {
					$id .= $str[rand()%strlen($str)];
				}
				return(($id = ($remd5 === true) ? md5($id) : $id));
			}
			
			return (null);
		}
		
		/**
		 * Check if a variable is NOT empty
		 *
		 * @param	string $str
		 * @param	int $type = null
		 * @param	bool $force = false
		 * @param	string $expr = null
		 * @return	bool
		 * @access	public
		 */
		static Function isEmpty($str, $type=null, $force=false, $expr=null)
		{
			if (!empty($type)) {

				if ($type == self::_BOOL_) {
					if (is_bool($str)) {
						if (!empty($str)) {
							return (false);
						}
					}
				}

				if ($type == self::_ARRAY_) {
					if (!empty($str)) {
						if (is_array($str)) {
							if (!self::isEmpty($force, self::_BOOL_)) {
								if (count($str) > 0) {
									return (false);
								}
								return (true);
							}
							return (false);
						}
					}
				}

				if ($type == self::_STRING_) {
					if (!empty($str)) {
						if (is_string($str)) {
							if ((int)$force > 0) {
								if (strcmp($expr, '>') == 0) {
									if (strlen($str) > (int)$force) {
										return (false);
									}
								}
								elseif (strcmp($expr, '>=') == 0) {
									if (strlen($str) >= (int)$force) {
										return (false);
									}
								}
								elseif (strcmp($expr, '<') == 0) {
									if (strlen($str) < (int)$force) {
										return (false);
									}
								}
								elseif (strcmp($expr, '<=') == 0) {
									if (strlen($str) <= (int)$force) {
										return (false);
									}
								}
								else {
									if (strlen($str) == (int)$force) {
										return (false);
									}
								}
							}
							else {
								return (false);
							}
						}
					}
				}

				if ($type == self::_NUMERIC_) {
					if (!empty($str)) {
						if (is_numeric($str)) {
							return (false);
						}
					}
				}

				if ($type == self::_DATETIME_) {
					if (!empty($str)) {
						if (self::checkCondition('#([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})#', $str)) {
							return (false);
						}
					}
				}
				
				if ($type == self::_EMAIL_) {
					if (!empty($str)) {
						if (self::checkCondition('#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,5}$#', $str)) {
							return (false);
						}
					}
				}
				
				if ($type == self::_DOMAIN_) {
					if (!empty($str)) {
						if (self::checkCondition('#^[\w.-]+\.[a-zA-Z]{2,5}$#', $str)) {
							return (false);
						}
					}
				}

			}
			else {
				if (!empty($str)) {
					return (false);
				}
			}

			return (true);
		}

		/**
		 * Check if a variable is numeric and positive
		 *
		 * @param	int $nb = null
		 * @return	bool
		 * @access	public
		 */
		static Function isPositive($nb=null)
		{
			if (!self::isEmpty($nb, self::_NUMERIC_)) {
				if ($nb > 0) {
					return (true);
				}
			}

			return (false);
		}
		
		/**
		 * Check if a variable is an array
		 *
		 * @param	mixed	$tab
		 * @param	Bool	$isEmpty = true
		 * @return	bool
		 * @access	public
		 */
		static Function isArray($key, $isEmpty=true)
		{
			return (!self::isEmpty($key, self::_ARRAY_, $isEmpty));
		}
		
		/**
		 * Check if a condition is verified
		 *
		 * @param	string $condition = null
		 * @param	mixed $str = null
		 * @return	bool
		 * @access	public
		 */
		static Function checkCondition($condition=null, $str=null)
		{
			if(!self::isEmpty($condition) && !self::isEmpty($str)) {
				if(@preg_match($condition, $str)) {
					return (true);
				}
			}
			return (false);
		}

		/**
		 * Create a new timestamp
		 *
		 * @param	int $interval = 0
		 * @param	int $format = self::_MS
		 * @param	int $mode = self::_NOW
		 * @return	timestamp
		 * @access	public
		 */
		static Function timestamp($interval=0, $format=self::_MS, $mode=self::_NOW)
		{
			$eTime = ($format == self::_ML) ? time() : microtime();
			if ($mode == self::_NOW) { return ($eTime); }
			elseif ($mode == self::_ADD) { return ($eTime + $interval); }
			elseif ($mode == self::_REM) { return ($eTime - $interval); }
			return (null);
		}
		
		/**
		 * Return a datetime from timestamp
		 *
		 * @param	int	$timestamp
		 * @return	string
		 * @access	public
		 */
		static Function timestampToDatetime($timestamp, $format='Y-m-d H:i:s')
		{
			$date = date('Y-m-d H:i:s', $timestamp);
			return (self::formatDatetime($date, $format));
		}
		
		/**
		 * Return a datetime format to string
		 *
		 * @param	datetime $datetime
		 * @param	string $format = 'Y-m-d H:i:s'
		 * @return	string
		 * @access	public
		 * @todo	li18n
		 */
		static Function formatDatetime($datetime=null, $format='Y-m-d H:i:s')
		{
			$separators = array('/', '-', ':', ' ', '.');
			$month = array('none', 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
			
			if (self::isEmpty($datetime, self::_DATETIME_)) {
				$datetime = date('Y-m-d H:i:s');
			}

			$date_time = explode(' ', $datetime);
			$date = explode('-', $date_time[0]);
			$time = explode(':', $date_time[1]);

			$d['Y'] = $date[0];
			$d['m'] = $date[1];
			$d['M'] = $month[(int)$date[1]];
			$d['d'] = $date[2];
			$d['H'] = $time[0];
			$d['i'] = $time[1];
			$d['s'] = $time[2];
			
			$nb = strlen($format);
			$i = 0;
			$res = null;
			while($i < $nb) {
				if (isset($d[$format[$i]])) {
					$res .= $d[$format[$i]];
				}
				if (in_array($format[$i], $separators)) {
					$res .= $format[$i];
				}
				$i++;
			}
			return ($res);
		}
		
		/**
		 * Get a string part between delimiters
		 *
		 * <sample>
		 *		- get_str_between("My name is {John Travolta}. I'm an actor!", '{', '}');
		 *		- return: John Travolta
		 * </sample>
		 * 
		 * @param	string $str
		 * @param	string $bet_start
		 * @param	string $bet_end
		 * @return	string | null
		 * @access	public
		 */
		static Function getStrBetween($str, $bet_start, $bet_end)
		{
			$i = 0;
			$len = strlen($str);
			$res = null;

			while ($i < $len) {
				if (strcmp(substr($str, $i, strlen($bet_start)), $bet_start) == 0) {
					$i = $i + strlen($bet_start);
					while (substr($str, $i, strlen($bet_end)) != $bet_end) {
						$res .= $str[$i];
						$i++;
					}
				}
				else {
					$i++;
				}
			}
			return ($res);
		}
		
		/**
		 * Concat some files
		 *
		 * @param	String	$path
		 * @param	Array	$sources
		 * @param	String	$target
		 * @param	Array	$replace
		 * @return	bool
		 * @todo	implement Logs::getLastError();
		 * @access	public
		 */
		static Function concatFiles($path, $sources, $target, $replace=null)
		{
			if (is_dir(DOCUMENTROOT.$path)) {
				if (self::isArray($sources)) {
					$files = new DataStore();
					foreach($sources as $source) {
						$src = new File(trim($path, '/').'/'.$source);
						$files->append($src);
					}
					$_buff = null;
					$t = new File($target, 'w'); 
					foreach($files->gets() as $_file) {
						$t->setBuffer($_file->getBuffer(), File::_ADD);
					}
					if(!$t->save(true)) {
						Logs::errors(E_USER_WARNING, 'Unable to save the concatened target file');
					}
					if (self::isArray($replace)) {
						self::strReplaceInFile($target, $replace);
					}
				}
				else {
					Logs::errors(E_USER_WARNING, 'Unable to read the sources array');
				}
			}
			else {
				Logs::errors(E_USER_WARNING, 'The sources path: '.$path.' does not exist');
			}
		}

		/**
		 * Replace string in a template string
		 * 
		 * @param	String	$str
		 * @param	mixed	$search
		 * @param	String	$value = null
		 * @return	string
		 * @access	private
		 */
		static Function strReplace($str, $search, $value=null)
		{
			if ((!(is_array($search)))) {
				$i = 0;
				$len = strlen($str);
				$res = '';
				while ($i < $len) {
					if (strcmp(substr($str, $i, strlen('{{ '.$search.' }}')), '{{ '.$search.' }}') == 0) {
						if ($value) {
							$res .= $value;
							$i += strlen('{{ '.$search.' }}');
						}
						else {
							$i += strlen('{{ '.$search.' }}');
						}
					}
					else {
						$res .= $str[$i];
						$i++;
					}
				}
			}
			else {
				$res = $str;
				if (is_array($search)) {
					foreach($search as $key => $val) {
						if (!is_array($val)) { $res = self::strReplace($res, trim($key), trim($val)); }
						else { $res = self::strReplace($res, $val); }
					}
				}
			}
			return ($res);
		}

		/**
		 * Copy a file then replace string in the template file
		 *
		 * @param	$source
		 * @param	$target
		 * @param	$replace
		 * @return	bool
		 * @access	public
		 */
		static Function strReplaceInFile($source, $target, $replace=null, $point=true)
		{
			$move = false;
			if (file_exists(DOCUMENTROOT.$source)) {
			
				if (self::isArray($target)) {
					$replace = $target;
					$move = true;
					$target = $source.'.'.self::createId(5);
				}
			
				if (($fs = fopen(DOCUMENTROOT.$source, 'r'))) {
					if (($fd = fopen(DOCUMENTROOT.$target, 'w'))) {
						$buff = null;
						while (!(feof($fs))) {
							$buff .= fgets($fs);
						}
						fclose($fs);

						foreach($replace as $key => $val) {
							$buff = self::strReplace($buff, $key, $val);
						}

						fputs($fd, $buff);
						fclose($fd);
				
						if ($move) {
							if (!rename(DOCUMENTROOT.$target, DOCUMENTROOT.$source)) {
								return (false);
							}
						}
						
						return (true);
					}
					else {
						Logs::errors(E_USER_WARNING, 'Unable to open the target file: '.$target);
						fclose($fs);
					}
				}
				else {
					Logs::errors(E_USER_WARNING, 'Unable to open the source file: '.$source);
				}
			}
			else {
				Logs::errors(E_USER_WARNING, 'The source file: '.$source.', doess not exist');
			}

			return (false);
		}

		/**
		 * Reverse a string
		 *
		 * @param	string $str
		 * @return	string
		 * @access	public
		 */
		static Function revStr($str)
		{
			$res = null;
			if (!self::isEmpty($str, self::_STRING_)) {
				$nb = strlen($str) + 1;
				while ($nb > -1) {
					if (isset($str[$nb])) { $res .= $str[$nb]; }
					$nb--;
				}
			}
			return ($res);
		}

		/**
		 * Reverse a string and apply a md5 filter
		 *
		 * @param	string $str
		 * @return	string
		 * @access	public
		 */
		static Function revStrMD5($str)
		{
			if (!self::isEmpty($str, self::_STRING_)) {
				return (md5(self::revStr($str)));
			}
			return (null);
		}
		
		/**
		 * Set filters on a buffer
		 * 
		 * @param	$buffer
		 * @param	$formats
		 * @return	string
		 * @access	public
		 */
		static Function filter($buffer, $formats)
		{
			if (!self::isEmpty($buffer, self::_STRING_) && self::isArray($formats)) {
				$result = $buffer;
				foreach ($formats as $format) {
					if ($format == Utils::_HTML) { $result = htmlentities($result); }
					elseif ($format == Utils::_HTML_ENT_QUOTE) { $result = htmlentities($result, ENT_QUOTES); }
					elseif ($format == Utils::_URL_ENC) { $result = urlencode($result); }
					elseif ($format == Utils::_URL_DEC) { $result = urldecode($result); }
					elseif ($format == Utils::_HTML_DECODE) { $result = html_entity_decode($result); }
					elseif ($format == Utils::_HTML_SPE_DECODE) { $result = htmlspecialchars_decode($result); }
					elseif ($format == Utils::_HTML_SPE) { $result = htmlspecialchars($result); }
					elseif ($format == Utils::_UTF_8) { $result = utf8_encode($result); }
					elseif ($format == Utils::_BASE_64) { $result = base64_encode($result); }
					elseif ($format == Utils::_B64_ENC) { $result = base64_encode($result); }
					elseif ($format == Utils::_B64_DEC) { $result = base64_decode($result); }
					else { $result = $format($result); }
				}
				return ($result);
			}
			return ($buffer);
		}
		
		/**
		 * Get the folder list in a path
		 *
		 * @param 	$path
		 * @return 	array | null
		 */
		static Function getFolderList($path)
		{
			if ((self::isEmpty($path)) || (!is_dir($path))) {
				Logs::error(E_USER_WARNING, 'The given path is not a directory: '.$path, __FILE__, 0, '003x10002', 'lib_files');
				return (null);
			}

			if (substr($path, -1) != '/') { $path .= '/'; }
			$files = null;
			$results = self::getFolderContent($path);
			if (is_array($results)) {
				foreach ($results as $result) { if ((is_dir($path.$result)) && (file_exists($path.$result))) { $files []= $result; }}
			}

			return ($files);
		}

		/**
		 * Get the file list in a path
		 *
		 * @param 	$path
		 * @return 	array | null
		 */
		static Function getFileList($path)
		{
			if ((self::isEmpty($path)) || (!is_dir($path))) {
				Logs::error(E_USER_WARNING, 'The given path is not a directory: '.$path, __FILE__, 0, '003x10003', 'lib_files');
				return (null);
			}

			if (substr($path, -1) != '/') { $path .= '/'; }
			$files = null;
			$results = self::getFolderContent($path);
			if (is_array($results)) {
				foreach ($results as $result) { if ((is_file($path.$result)) && (file_exists($path.$result))) { $files []= $result; }}
			}

			return ($files);
		}

		/**
		 * Get the content folder like the unix command "ls"
		 *
		 * @param 	$path
		 * @return 	array | null
		 */
		static Function getFolderContent($path)
		{
			if ((self::isEmpty($path)) || (!is_dir($path))) {
				Logs::errors(E_USER_WARNING, 'The given path is not a directory: '.$path, __FILE__, 0, '003x10001', 'lib_files');
				return (null);
			}

			if (substr($path, -1) != '/') { $path .= '/'; }
			$res = null;
			$files = scandir($path);
			foreach ($files as $file) { if ((strcmp($file, '.') != 0) && (strcmp($file, '..') != 0)) { $res []= $file; }}

 			return($res);

		}

		/**
		 * Delete a folder like the unix command "rm -rf"
		 *
		 * @param string $dir
		 * @return bool
		 */
		static Function rollBackFolders($dir)
		{
			if (!is_dir($dir)) { return (true); }
			$handle = opendir($dir);
			$exit = false;

			while ($exit == false) {
				$file = @readdir($handle);
				if (!trim($file)) { $exit = true; }
				else {
					if ((strncmp($file[$i], '.', 1) != 0) && (strncmp($file[$i], '..', 2) != 0)) {
						chmod($dir.'/'.$file, 0777);
						if (is_dir($dir.'/'.$file)) { self::rollBackFolders($dir.'/'.$file); }
						else { unlink($dir.'/'.$file); }
					}
				}
			}

			if (rmdir($dir.'/')) { return (true); }
			else{ return (self::rollBackFolders($dir)); }
		}

		/**
		 * Create a new file like the unix command "touch"
		 *
		 * @param	string $file
		 * @return	bool
		 * @access	public
		 */
		static Function touch($file)
		{
			if (!file_exists($file)) {
				if (($fd = fopen($file, 'w'))) {
					fclose($fd);
					return (true);
				}
			}
			return (false);
		}

		/**
		 * Copy a folder like the unix command "cp -r"
		 *
		 * @param string $source
		 * @param string $target
		 * @return bool
		 */
		static Function copyDir($source, $target, $chmod=0755)
		{
			if (is_dir($source)) {
				$dir = opendir($source);
				mkdir($target, (int)$chmod, true);
				while (false !== ($file = readdir($dir))) {
					if ((strncmp($file, '.', 1) != 0) && (strncmp($file, '..', 2) != 0)) {
						if (is_dir(rtrim($source, '/').'/'.$file)) {
							self::copyDir(rtrim($source, '/').'/'.$file, rtrim($target, '/').'/'.$file);
						}
						else {
							chmod(rtrim($source, '/').'/'.$file, (int)$chmod);
							copy(rtrim($source, '/').'/'.$file, rtrim($target, '/').'/'.$file);
						}
					}
				}
				closedir($dir);
			}
			else {
				Logs::errors(E_USER_WARNING, 'No source directory: '.$source, __FILE__, 0, '003x10004', 'lib_files');
				return (false);
			}

			return (true);
		}
		
		/**
		 * Get the file extension
		 *
		 * @param 	$file
		 * @return 	string
		 */
		static Function getExtension($file)
		{
			$tmp = explode('.', $file);
			return ($tmp[count($tmp) - 1]);
		}
		
	Private
		/**
		 * Constructor
		 *
		 * @return	Utils
		 * @access	private
		 */
		Function __construct() {}
		
	Private
		/**
		 * Clone constructor
		 *
		 * @return	Utils
		 * @access	private
		 */
		Function __clone() {}

	Private
		/**
		 * Get the System instance
		 *
		 * @return	System
		 * @access	private
		 */
		static Function access()
		{
			if(self::$_instance == null) {
				self::$_instance = new self();
			}
			return (self::$_instance);
		}
}