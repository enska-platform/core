<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Service
 * @package		core.engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 * @todo		DEFINE the config IO (JSON, INI, ...)
 */
 
namespace Enska\Services;
use \Enska\Helpers\DataStore;
use \Enska\IO\JSON;

final class Config
{

	/**
	 * Config instance
	 * @type	Config
	 */
	Private	static	$_instance = null;
	
	/**
	 * Configs container
	 * @type	DataStore
	 */
	Private			$configs = null;
	
	Public
		/**
		 * Load a configuration file
		 *
		 * @param	string $id
		 * @param	string $file
		 * @return	void
		 * @access	public
		 */
		static Function load($id, $file)
		{
			self::access()->_addConfig($id, $file); 
		}
		
		/**
		 * Get value from a configuration file
		 *
		 * @param	string $id
		 * @param	string $key
		 * @param	string $attr = null
		 * @return	string
		 * @access	public
		 */
		static Function get($id)
		{
			return (self::access()->_getConfig($id));
		}
		
		/**
		 * Remove a configuration file from stack
		 *
		 * @param	string $id
		 * @return	void
		 * @access	public
		 */
		static Function remove($id)
		{
			self::access()->_removeConfig($id);
		}

	Private
		/**
		 * Get value from a configuration file
		 *
		 * @param	string $id
		 * @param	string $head
		 * @param	string $attr = null
		 * @return	string
		 * @access	private
		 */
		Function _getConfig($id)
		{
			return (self::access()->configs->$id);
		}
		
	Private
		/**
		 * Remove a configuration file from stack
		 *
		 * @param	string $id
		 * @return	void
		 * @access	private
		 */
		Function _removeConfig($id)
		{
			self::access()->configs->delete($id);
		}
		
	Private
		/**
		 * Load a configuration file
		 *
		 * @param	string $id
		 * @param	string $file
		 * @return	void
		 * @access	private
		 */
		Function _addConfig($id, $file)
		{
			$io = new JSON($file, array('connector' => 'FS'));
			self::access()->configs->$id = DataStore::create($io->receive());
		}
		
	Private
		/**
		 * Constructor
		 *
		 * @return	Config
		 * @access	private
		 */
		Function __construct()
		{
			$this->configs = new DataStore('Config_Store');
		}
		
	Private
		/**
		 * Clone constructor
		 *
		 * @return	Config
		 * @access	private
		 */
		Function __clone()
		{
			$this->configs = new DataStore('Config_Store');
		}
		
	Private
		/**
		 * Get the Config instance
		 *
		 * @return	Config
		 * @access	private
		 */
		static Function access()
		{
			if(self::$_instance == null) {
				self::$_instance = new self();
			}
			return (self::$_instance);
		}
}

?>