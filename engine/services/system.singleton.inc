<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Service
 * @package		core.engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Services;
use \Enska\Interfaces\ISystem;
use \Enska\Helpers\UserAgent;
 
final class System implements ISystem
{
	/**
	 * Enska system instance
	 * @type System instance
	 */
	Private	static	$_instance = null;
	
	/**
	 * Engine execution time
	 * @type	int
	 */
	Private	$_execTime = 0;

	Public
		/**
		 * Class loader
		 *
		 * @return	void
		 * @access	public
		 */
		static Function load()
		{
			$env = Context::enska('env');
			$phpini = (Config::get('settings') && Config::get('settings')->$env && Config::get('settings')->$env->system && Config::get('settings')->$env->system->ini) ? Config::get('settings')->$env->system->ini : null;
			Logs::add('system');
			
			self::access()->_regNavigator();
			self::access()->_regHeaders();
			
			if (Context::enska('HTTP_REQUEST') === true) {
				if (!Utils::isEmpty($phpini) && Utils::isArray($phpini->gets())) {
					foreach($phpini->gets() as $key => $value) {
						if (!self::set($key, $value)) {
							Logs::error(E_WARNING, 'Unable to set the PHP INI Value: '.$key.':'.$value, 'System');
						}
					}
				}
			}
		}

		/**
		 * Set a PHP option
		 *
		 * @param	mixed	$attr
		 * @param	mixed	$value = 0
		 * @return	bool
		 * @access	public
		 */
		static Function set($attr, $value=0)
		{
			if (ini_set($attr, $value)) {
				return (true);
			}

			if (!Utils::isEmpty($value, Utils::_STRING_)) {
				if (@constant($value)) {
					if (ini_set($attr, constant($value))) {
						return (true);
					}
				}
			}
			
			return (false);
		}

		/**
		 * Get a PHP option
		 *
		 * @param	mixed	$attr
		 * @return	mixed
		 * @access	public
		 */
		static Function get($attr)
		{
			return (ini_get($attr));
		}

		/**
		 * Check if is the default called system
		 *
		 * @return	bool
		 * @access	public
		 */
		static Function isDefaultSystem()
		{
			return (Kernel::isDefaultApp() && Kernel::isDefaultPool());
		}

		/**
		 * Check if is the default called pool
		 *
		 * @return	bool
		 * @access	public
		 */
		static Function isDefaultPool()
		{
			return (Kernel::isDefaultPool());
		}

		/**
		 * Check if is the default called application
		 *
		 * @return	bool
		 * @access	public
		 */
		static Function isDefaultApp()
		{
			return (Kernel::isDefaultApp());
		}

		/**
		 * Enska autoload
		 *
		 * @param	string $class
		 * @param	int $type = null
		 * @return	void
		 * @access	public
		 * @move	Import
		 */
		static Function autoLoad($class, $type=null) {}

	Private
		/**
		 * Get and register user navigator infos
		 *
		 * @return	void
		 * @access	private
		 */
		Function _regNavigator()
		{
			if (Context::enska('HTTP_REQUEST') === true) {
				Context::set('userNavInfos', new UserAgent());
			}
		}
		
	Private
		/**
		 * Register default HTTP headers
		 *
		 * @return	void
		 * @access	private
		 */
		Function _regHeaders()
		{
			$env = Context::enska('env');
			$headers = (Config::get('settings') && Config::get('settings')->$env && Config::get('settings')->$env->headers) ? Config::get('settings')->$env->headers : null;
			
			if (!Utils::isEmpty($headers) && Utils::isArray($headers->gets())) {
				foreach($headers->gets() as $key => $value) {
					Headers::add($key, $value);
				}
				Headers::process();
			}
		}
		
	Private
		/**
		 * Constructor
		 *
		 * @return	System
		 * @access	private
		 */
		Function __construct() {}
		
	Private
		/**
		 * Clone constructor
		 *
		 * @return	System
		 * @access	private
		 */
		Function __clone() {}

	Private
		/**
		 * Get the System instance
		 *
		 * @return	System
		 * @access	private
		 */
		static Function access()
		{
			if(self::$_instance == null) {
				self::$_instance = new self();
			}
			return (self::$_instance);
		}
}

 ?>