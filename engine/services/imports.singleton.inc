<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Service
 * @package		core.engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

use	\Enska\Helpers\DataStore;
use \Enska\Services\Logs;
use \Enska\Services\Context;
use \Enska\Services\Config;
use	\Enska\Services\Utils;
use	\Enska\IO\JSON;
 
class Imports
{
	const			_STYLE_SHEET = 'css';
	const			_JAVASCRIPT = 'js';
	const			_JAVASCRIPT_APP = 'app';
	const			_IMAGE_B64 = 'img';
	const			_HELPER = 'engine';
	const			_MODEL = 'engine';
	
	/**
	 * Enska Imports instance
	 * @type	Imports
	 */
	Private	static	$_instance = null;

	Private			$_model = null;
	Private			$_resources = null;
	Private			$_path = null;
	Private			$_vars = null;

	Public
		/**
		 * Class loader
		 *
		 * @return	void
		 * @access	public
		 */
		static Function load() 
		{
			Logs::add('imports');
			$env = Context::enska('env');
			$protocol = (Config::get('settings') && Config::get('settings')->$env && Config::get('settings')->$env->protocol) ? Config::get('settings')->$env->protocol : 'http';
			$resources = (Config::get('settings') && Config::get('settings')->$env && Config::get('settings')->$env->resources) ? Config::get('settings')->$env->resources : 'enskacloud.local';
			$dmz = (Config::get('settings') && Config::get('settings')->$env && Config::get('settings')->$env->dmz) ? Config::get('settings')->$env->dmz : 'WWW';
			self::access()->_resources = $protocol.'://'.$resources;
			self::access()->_models = Context::enska('appPath');
			self::access()->_dmz = $dmz.'/';
		}
		
		/**
		 * Imports an application model
		 * 
		 * @param	string 	$name
		 * @param	string	$app = null
		 * @return	bool
		 * @access	public
		 */
		static Function model($name, $app=null)
		{
			$app = (!empty($app)) ? $app : Context::enska('app');	
			self::access()->_models = Context::enska('poolPath').$app.'/';
			self::access()->_path = self::access()->_models.'models/'.$name.INCLUDE_EXT;
			return (self::access()->_imports());
		}
		
		/**
		 * Imports an application service
		 * 
		 * @param	string 	$name
		 * @param	string	$app = null
		 * @return	bool
		 * @access	public
		 */
		static Function service($name, $app=null)
		{
			$app = (!empty($app)) ? $app : Context::enska('app');
			self::access()->_models = Context::enska('poolPath').$app.'/';
			self::access()->_path = self::access()->_models.'services/'.$name.ENSKA_SERVICE_EXT;
			return (self::access()->_imports());
		}
		
		/**
		 * Imports application templates
		 * 
		 * @param	string 	$name
		 * @param	string	$app = null
		 * @return	bool
		 * @access	public
		 */
		static Function templates($app=null, $vars=null)
		{
			self::access()->_vars = DataStore::create($vars);
			$app = (!empty($app)) ? $app : Context::enska('app');
			self::access()->_models = Context::enska('poolPath').$app.'/';
			$path = self::access()->_models.'views/';
			$files = Utils::getFileList(DOCUMENTROOT.$path);
			if (is_array($files)) {
				foreach($files as $file) {
					self::access()->_path = $path.$file;
					self::access()->_imports();
				}
			}
		}
		
		/**
		 * Get a pair item
		 *
		 * @param	$key
		 * @return	mixed
		 * @access	public
		 */
		Function get($key)
		{
			return ($this->_vars->$key);
		}
		
		/**
		 * Get the DMZ application url
		 * 
		 * @return	String
		 * @access	Public
		 */
		Function url()
		{
			$env = Context::enska('env');
			return ('http://'.Config::get('settings')->$env->resources.'/webapps/'.Context::enska('app').'/resources/');
		}

		/**
		 * Imports the current application controller
		 * 
		 * @param	string 	$name
		 * @param	const	$ext
		 * @return	bool
		 * @access	public
		 */
		static Function controller()
		{
			self::access()->_path = Context::enska('appPath').'controller.php';
			return (self::access()->_imports());
		}
		
		/**
		 * Imports a JavaScript file
		 * 
		 * @param	string 	$name
		 * @param	string	$appName = null
		 * @return	bool
		 * @access	public
		 */
		static Function javascript($name, $appName=null)
		{
			$resources = self::access()->_resources.'/webapps/';
			self::access()->_path = (!Utils::isEmpty($appName)) ? $resources.$appName.'/'.$name.'.js' : $resources.Context::enska('app').'/'.$name.'.js';
			return (self::access()->_importsResources('js'));
			
		}
		
		/**
		 * Imports a CSS file
		 * 
		 * @param	string 	$name
		 * @param	string	$appName = null
		 * @return	bool
		 * @access	public
		 */
		static Function css($name, $appName=null)
		{
			self::access()->_path = (!Utils::isEmpty($appName)) ? self::access()->_resources.'/webapps/'.$appName.'/'.$name.'.css' : self::access()->_resources.'/webapps/'.Context::enska('app').'/'.$name.'.css';
			return (self::access()->_importsResources('css'));
			
		}
		
		/**
		 * Imports the Enskaui bootstrap
		 * 
		 * @return	void
		 * @access	public
		 */
		static Function enskaui()
		{
			$libs = null;
			$env = Context::enska('env');
			$file = Config::get('settings')->$env->dmz.'/opajs/app.json';
			$hdl = new JSON($file, array('connector' => 'FS'));
			if (!\debug::jsonErr() && !$hdl->getLastError()){
				self::access()->_app($hdl->read(), 'opajs', 'opajs');
			}
			else {
				// ERROR ON APP INCLUDE
			}
		}
		
		/**
		 * Imports a javascript app
		 * 
		 * @param	string	$appName = null
		 * @return	bool
		 * @access	public
		 */
		static Function app($appName=null)
		{
			$libs = null;
			$env = Context::enska('env');
			$app = (!Utils::isEmpty($appName)) ? $appName : Context::enska('app');
			$file = Config::get('settings')->$env->dmz.'/webapps/'.$app.'/app.json';
			$hdl = new JSON($file, array('connector' => 'FS'));
			if (!\debug::jsonErr() && !$hdl->getLastError()){
				self::access()->_app($hdl->read(), $app, 'webapps/'.$app);
			}
			else {
				// ERROR ON APP INCLUDE
			}
		}

	Private
		/**
		 * Launch resources imports
		 * 
		 * @param	DatStore	$libs
		 * @param	string		$appName
		 * @param	string		$path
		 * @return	void
		 * @access	private
		 */
		Function _app($libs, $appName, $path)
		{
			if ($libs->flags && $libs->flags->minimize) {
				if (!file_exists(DOCUMENTROOT.self::access()->_dmz.'/build/'.$appName.'-min.css') || !file_exists(DOCUMENTROOT.self::access()->_dmz.'/build/'.$appName.'-min.js')) {
					self::access()->_minimize($libs, $path, $appName);
				}
				self::access()->_path = self::access()->_resources.'/build/'.$appName.'-min.css';
				self::access()->_importsResources('css');
				self::access()->_path = self::access()->_resources.'/build/'.$appName.'-min.js';
				self::access()->_importsResources('js');
			}
			else {
				if ($libs->libs && $libs->libs->css && Utils::isArray($libs->libs->css->gets())){
					foreach ($libs->libs->css->gets() as $css) {
						self::access()->_path = self::access()->_resources.'/'.$path.'/'.$css;
						if (!strncmp($css, 'http', strlen('http'))) {
							self::access()->_path = $css;
						}
						self::access()->_importsResources('css');
					}
				}
				
				if ($libs->libs && $libs->libs->js && Utils::isArray($libs->libs->js->gets())){
					foreach ($libs->libs->js->gets() as $js) {
						self::access()->_path = self::access()->_resources.'/'.$path.'/'.$js;
						if (!strncmp($js, 'http', strlen('http'))) {
							self::access()->_path = $js;
						}
						self::access()->_importsResources('js');
					}
				}
			}
		}
		
	Private
		/**
		 * Minimize a JavaScrip app
		 * 
		 * @param	DataStore	$libs
		 * @param	string		$path
		 * @param	string		$name
		 * @return	void
		 * @access	public
		 */
		Function _minimize($libs, $path, $name)
		{
			$jsBuffer = $cssBuffer = '';
			$env = Context::enska('env');
			
			if ($libs->libs && $libs->libs->css && Utils::isArray($libs->libs->css->gets())){
				foreach ($libs->libs->css->gets() as $css) {
					self::access()->_path = DOCUMENTROOT.self::access()->_dmz.'/'.$path.'/'.$css;
						if (!strncmp($css, 'http', strlen('http'))) {
							self::access()->_path = $css;
						}
						$cssBuffer .= CssMin::minify(file_get_contents(self::access()->_path));
					}
					file_put_contents(DOCUMENTROOT.self::access()->_dmz.'build/'.$name.'-min'.'.css', $cssBuffer);
			}
			
			if ($libs->libs && $libs->libs->js && Utils::isArray($libs->libs->js->gets())){
				foreach ($libs->libs->js->gets() as $js) {
					self::access()->_path = DOCUMENTROOT.self::access()->_dmz.'/'.$path.'/'.$js;
					if (!strncmp($js, 'http', strlen('http'))) {
						self::access()->_path = $js;
					}
					$jsBuffer .= JSMin::minify(file_get_contents(self::access()->_path));
				}
				file_put_contents(DOCUMENTROOT.self::access()->_dmz.'build/'.$name.'-min'.'.js', $jsBuffer);
			}
		}
		
	Private
		/**
		 * Do a simple import
		 * 
		 * @return	bool
		 * @access	private
		 */
		Function _imports()
		{
			if (!file_exists(DOCUMENTROOT.self::access()->_path)) { return (false); }
			if (!include_once(DOCUMENTROOT.self::access()->_path)) { return (false); }
			return (true);
		}
		
	Private
		/**
		 * Do a resource import
		 * 
		 * @type	string	$type = 'js'
		 * @return	bool
		 * @access	private
		 */
		Function _importsResources($type='js')
		{
			if (!strcmp($type, 'js')) { echo '<script type="text/javascript" src="'.self::access()->_path.'"></script>'; }
			elseif (!strcmp($type, 'css')) { echo '<link href="'.self::access()->_path.'" rel="stylesheet">'; }
			return (true);
		}

	Private
		/**
		 * Constructor
		 * 
		 * @return	Imports
		 * @access	private
		 */
		Function __construct() {}
		
	Private
		/**
		 * Clone constructor
		 * 
		 * @return	Imports
		 * @access	private
		 */
		Function __clone() {}

	Private
		/**
		 * Access to the singleton internal instance
		 * 
		 * @return	Imports
		 * @access	private
		 */
		static Function access()
		{
			if(self::$_instance == null) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}
}

?>