<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Service
 * @package		core.engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Services;
use \Enska\Interfaces\ISystem;
use	\Enska\Helpers\Error;
use	\Enska\Helpers\Observer;
use \Enska\Helpers\DataStore;
use \Enska\Abstracts\IO;

DEFINE ('EXCEPTION', 1025);

final class Logs implements ISystem
{	
	/**
	 * Enska logs instance
	 * @type	Logs
	 */
	Private	static	$_instance = null;
	
	/**
	 * Logs io
	 * @type	IO
	 */
	Private			$io;
	
	/**
	 * Logs observer instance
	 * @type	Observer
	 */
	Private			$observer;
	
	/**
	 * Logs store
	 * @type	DataStore
	 */
	Private			$logs;
	
	Public
		/**
		 * Load logs configuration and set Errors/Exceptions handlers
		 *
		 * @return	void
		 * @access	public
		 */
		static Function load()
		{
			$env = Context::enska('env');
			
			if ($env && Config::get('settings') && Config::get('settings')->$env && Config::get('settings')->$env->logs) {
				ini_set('display_errors', 1);
				error_reporting(E_ALL);
				set_error_handler(array(self::access(),'errorsHandler'));
				set_exception_handler(array(self::access(),'exceptionsHandler'));
				register_shutdown_function(array(self::access(),'exitHandler'));
				self::access()->logs = new DataStore();
			}
		}
		
		/**
		 * Add a new error subject
		 *
		 * @param	string $name
		 * @return	void
		 * @access	public
		 */
		static Function add($name)
		{
			$observer = self::access()->getObserver();
			new Error($name, array('observer' => &$observer));
		}
		
		/**
		 * Send a global error to log
		 *
		 * @param	mixed $throw = 0
		 * @param	string $message = 'Unknown error'
		 * @param	int $line = '-'
		 * @param	string $file = '-'
		 * @param	string $code = '000x00000'
		 * @param	mixed $parent = '-'
		 * @return	void
		 * @access	public
		 */
		static Function error($throw=0, $message='Unknown error', $line='-', $file='-', $code='000x00000', $parent='default')
		{
			$datas = (Utils::isArray($throw) || ($throw instanceof DataStore)) ? DataStore::create($throw) : DataStore::create(array('throw' => $throw, 'message' => $message, 'file' => $file, 'line' => $line, 'code' => $code, 'parent' => $parent));
			$parent = ($datas->parent) ? $datas->parent : 'default';
			self::access()->getObserver()->fire(($evt = new DataStore(array('subject' => $parent, 'argv' => $datas))));
		}
		
		/**
		 * Exception handler
		 *
		 * @param	Exception $e
		 * @return	void
		 * @access	public
		 */
		static Function exceptionsHandler($e)
		{
			if ($e->getPrevious()){
				$e = $e->getPrevious();
			}

			self::error(DataStore::create(array(
				'throw'		=> EXCEPTION,
				'message' 	=> $e->getMessage(),
				'file' 		=> $e->getFile(),
				'line' 		=> $e->getLine(),
				'code' 		=> $e->getCode().' -- '.get_class($e)
			)));
		}
		
		/**
		 * Errors handler
		 *
		 * @param	int $throw = 0
		 * @param	string $message = 'Unknown error'
		 * @param	string $file = '-'
		 * @param	int $line = '-'
		 * @param	string $code = '000x00000'
		 * @param	mixed $parent = '-'
		 * @return	void
		 * @access	public
		 */
		static Function errorsHandler($throw=0, $message='Unknown error', $file='-', $line='-', $code='000x00000', $parent='default')
		{
			if (!(self::access()->logs instanceof DataStore)) {
				self::access()->logs = new DataStore();
			}
			
			if (self::_isThrowable($throw)) {
				self::access()->logs->push(array('throw' => self::getErrType($throw), 'message' => $message, 'file' => $file, 'line' => $line, 'code' => $code, 'parent' => $parent, 'type' => 'ERROR', 'date' => date('m-d-Y H:i:s')));
				if (($throw == E_ERROR) || ($throw == E_USER_ERROR)){
					exit(FAILED);
				}
			}
		}
		
		/**
		 * Exit handler
		 *
		 * @param	int $throw = 0
		 * @return	void
		 * @access	public
		 */ 
		static function exitHandler($throw=FAILED){			
			$env = Context::enska('env');
			$path = Config::get('settings')->$env->logs->exitPath.'/'.Config::get('settings')->$env->logs->filename;
			
			if (Utils::isArray(self::access()->logs->gets())) {
				$buff = '';
				foreach (self::access()->logs->gets() as $err) {
					$err = DataStore::create($err);
					$dbuff .= '['.$err->type.' : '.$err->throw.']'."\n".'['.$err->message.']'."\n".'[in '.$err->file.', line: '.$err->line.']'."\n".'[with error code: '.$err->code.' in parent: '.$err->parent.']'."\n".'['.$err->date."]\n".'----------------------------------------------------------------------------------------'."\n";
					//$buff .= '['.$err->type.' : '.$err->throw.']<br>['.$err->message.']<br>[in '.$err->file.', line: '.$err->line.']<br>[with error code: '.$err->code.' in parent: '.$err->parent.']'."\n".'['.$err->date.']<br>'.'----------------------------------------------------------------------------------------'.'<br>';
				}
				$buff = $dbuff;
				if (!strcmp(Config::get('settings')->$env->logs->display, 'on')) {
					echo $dbuff;
				}
			}
			
			if (file_exists($path) && is_writable($path) && !Utils::isEmpty($buff, Utils::_STRING_)) {
				file_put_contents($path, $buff, FILE_APPEND);
			}
		}
		
		/**
		 * Get the _toString throwing error
		 *
		 * @param	int	$eCode
		 * @return	string
		 * @access	public
		 */
		static Function getErrType($eCode)
		{
			switch ($eCode) {
				case 1:
					return('E_ERROR');
				case 2:
					return('E_WARNING');
				case 4:
					return('E_PARSE');
				case 8:
					return('E_NOTICE');
				case 256:
					return('E_ERROR');
				case 512:
					return('E_WARNING');
				case 1024:
					return('E_NOTICE');
				case 1025:
					return('EXCEPTION');
				case 2048:
					return('E_STRICT');
				case 8192:
					return('E_DEPRECATED');
			}
			return('UNKNOWN');
		}
		
	Private
		/**
		 * Constructor
		 * 
		 * @return	Logs
		 * @access	private
		 */
		Function __construct() {}
		
	Private
		/**
		 * Clone constructor
		 * 
		 * @return	Logs
		 * @access	private
		 */
		Function __clone() {}
		
	Private
		/**
		 * Get the Logs instance
		 *
		 * @return	Logs
		 * @access	private
		 */
		static Function access()
		{
			if(self::$_instance == null) {
				self::$_instance = new self();
				self::add('default');
			}
			return (self::$_instance);
		}
		
	Private
		/**
		 * Get if an error is throwable
		 *
		 * @param	int $throw
		 * @return	bool
		 * @access	private
		 */
		static Function _isThrowable($throw)
		{
			$env = Context::enska('env');
			if (!Config::get('settings')->$env){
				return (false);
			}
			
			$levels = Config::get('settings')->$env->logs->levels;
			if (!$levels) {
				return (false);
			}
			
			$levels = $levels->gets();
			if (Utils::isArray($levels)) {
				foreach ($levels as $level) {
					if (constant(trim($level)) === E_ALL) {
						return (true);
					}
					if (constant(trim($level)) === $throw) {
						return (true);
					}
				}
			}
			return (false);
		}
		
	Private
		/**
		 * Add a new error subject
		 *
		 * @return	Observer
		 * @access	private
		 */
		Function getObserver()
		{
			if (!($this->observer instanceof Observer)) {
				$this->observer = new Observer('Logs');
			}
			return ($this->observer);
		}
}
 
?>