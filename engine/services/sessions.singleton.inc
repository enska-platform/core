<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Service
 * @package		core.engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Services;
use \Enska\Interfaces\ISystem;
 
final class Sessions implements ISystem
{	
	/**
	 * Sessions instance
	 * @type	Sessions
	 */
	Private	static	$_instance = null;
	
	/**
	 * Session helper
	 * @type	Object
	 */
	Private			$session = null;
	
	/**
	 * Token
	 * @type	string
	 */
	Private			$token = null;
	
	Public
		/**
		 * Service loader
		 * @return	void
		 * @access	public
		 */
		static Function load()
		{
			Logs::add('sessions');
			self::access()->_loader();
			self::access()->start();
		}
		
	Private
		/**
		 * Session loader
		 * 
		 * @return	void
		 * @access	private
		 */
		Function _loader()
		{
			$env = Context::enska('env');
			if (Config::get('settings') && Config::get('settings')->$env && Config::get('settings')->$env->sessions) {
				if (Config::get('settings')->$env->sessions->scope) {
					$scope = (!Utils::isEmpty(Config::get('settings')->$env->sessions->scope)) ? Config::get('settings')->$env->sessions->scope : 'php';
					try {
						include_once(ENSKA_HELPERS.'sessions/'.$scope.'.class.inc');
						$class = new \ReflectionClass('\Enska\\Helpers\\Sessions\\'.$scope);
						$this->session = $class->newInstance();
						self::getToken();
					}
					catch(Exception $e) {
						//\debug::p($e->getMessage());
						// TODO: ADD to logs
					}
				}
			}
		}
		
		/**
		 * Set a session variable
		 * @param	String	$attr
		 * @param	mixed	$value = null
		 * @return	void
		 * @access	public
		 */
		static Function setSession($attr, $value=null)
		{
			if (is_object(self::access()->session)) { self::access()->session->setSession($attr, $value); }
		}
		
		/**
		 * Get a session variable
		 * @param	String	$attr
		 * @return	Mixed
		 * @access	public
		 */
		static Function getSession($attr)
		{
			if (is_object(self::access()->session)) { return (self::access()->session->getSession($attr)); }
			return (null);
		}
		
		/**
		 * Get all session variables
		 * @return	Array
		 * @access	public
		 */
		static Function gets()
		{
			if (is_object(self::access()->session)) { return (self::access()->session->gets()); }
			return (null);
		}
		
		/**
		 * Delete a session variable
		 * @param	String	$attr
		 * @return	void
		 * @access	public
		 */
		static Function deleteSession($attr)
		{
			if (is_object(self::access()->session)) { self::access()->session->delete($attr); }
		}
		
		/**
		 * Destroy the current
		 * @return	void
		 * @access	public
		 */
		static Function destroySession()
		{
			if (is_object(self::access()->session)) { self::access()->session->destroy(); }
		}
		
		/**
		 * Get the current token
		 *
		 * @return	string
		 * @access	public
		 */
		static Function getToken()
		{
			$token = self::access()->token;

			if (!Utils::isEmpty(trim($token))) { return ($token); } // Memory
			if (isset($_GET['token'])) { $token = $_GET['token']; } // GET
			elseif (isset($_POST['token'])) { $token = $_POST['token']; } // POST
			//elseif (Utils::isEmpty(($token = self::getSession('token')))) { $token = null; } // First time

			return ($token);
		}
		
		/**
		 * Get the current token
		 *
		 * @param	string	$token = null
		 * @return	void
		 * @access	public
		 */
		static Function setToken($token=null)
		{
			/*$remember = false;
			if (isset($_POST['remember'])) { $remember = $_POST['remember']; }
			self::setCookie('__token__', $token, $remember);*/
			self::access()->token = $token;
		}
		
		/**
		 * Delete the current token
		 *
		 * @return	void
		 * @access	public
		 */
		static Function delToken()
		{
			//self::delCookie('__token__');
			self::access()->token = null;
		}
		
		/**
		 * Set a cookie
		 * @param	String	$attr
		 * @param	String	$value = null
		 * @return	void
		 * @access	public
		 */
		static Function setCookie($attr, $value=null, $remember=false)
		{
			if (!Utils::isEmpty($attr)) {
				setcookie($attr, $value, (time() + self::_getCookieTime($remember)), '/', Config::get('settings')->default->domain);
			}
		}
		
		/**
		 * Delete a cookie
		 * @param	String	$attr
		 * @return	void
		 * @access	public
		 */
		static Function delCookie($attr)
		{
			if (!Utils::isEmpty($attr)) {
				setcookie($attr, $value, (time() - self::_getCookieTime(true)), '/', Config::get('settings')->default->domain);
			}
		}
		
		/**
		 * Get a cookie
		 * @param	String	$attr
		 * @return	void
		 * @access	public
		 */		
		static Function getCookie($attr=null)
		{
			if ($attr) { if (isset($_COOKIE[$attr])) { return($_COOKIE[$attr]); } }
			return (null);
		}
		
	Private
		static Function _getCookieTime($remember=false)
		{
			$env = Context::enska('env');
			$rType = ($remember) ? 'auth' : 'pub';			
			$timeType = Config::get('settings')->$env->sessions->cookie->$rType->timeType;
			$cookieTime = Config::get('settings')->$env->sessions->cookie->$rType->cookietime;
			$cookieTime =  ($timeType === "d") ? $cookieTime*24*60*60 : (($timeType === "h") ? $cookieTime*60*60 : (($timeType === "i") ? $cookieTime*60 : 0));
			return ($cookieTime);
		}
		
	Private
		/**
		 * Session starter
		 * 
		 * @return	void
		 * @access	private
		 */
		Function start()
		{
			if (is_object($this->session)) { $this->session->start(); }
		}
		
	Private
		/**
		 * Constructor
		 * 
		 * @return	Sessions
		 * @access	private
		 */
		Function __construct() {}
		
	Private
		/**
		 * Clone constructor
		 * 
		 * @return	Sessions
		 * @access	private
		 */
		Function __clone() {}

	Private
		/**
		 * Access to the singleton internal instance
		 * 
		 * @return	Sessions
		 * @access	private
		 */
		static Function access()
		{
			if(self::$_instance == null) { self::$_instance = new self(); }
			return self::$_instance;
		}
}

?>