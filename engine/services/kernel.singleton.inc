<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Service
 * @package		core.engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Services;
 
final class Kernel implements \Enska\Interfaces\ISystem
{	
	/**
	 * Enska kernel instance
	 * @type	Kernel
	 */
	Private	static	$_instance = null;
	
	/**
	 * Kernel time
	 * @type	array
	 */
	Private $_kernelTime = null;
	
	/**
	 * Engine execution time
	 * @type	int
	 */
	Private	$_execTime = 0;
	
	/**
	 * Current request is default pool
	 * @type	bool
	 */
	Private			$_isDefaultPool = true;
	
	/**
	 * Current request is default app
	 * @type	bool
	 */
	Private			$_isDefaultApp = true;
	
	Public
		/**
		 * Load class configuration
		 *
		 * @return	void
		 * @access	public
		 */
		static Function load() {}
		
		/**
		 * Time to load
		 *
		 * @param	string $mark = load
		 * @return	mixed
		 * @access	public
		 */
		static Function kernelTime($mark='__init')
		{
			if (!strcmp($mark, '__init')){
				self::access()->_kernelTime = array();
				self::access()->_kernelTime['__init'] = self::execTime();
				self::access()->_kernelTime['__old'] = null;
			}
			elseif (!strcmp($mark, '__end')){
				if (self::access()->_kernelTime['__old']) {
					$lastMark = self::access()->_kernelTime['__old'];
					self::access()->_kernelTime[$lastMark] = self::execTime();
					unset(self::access()->_kernelTime['__init']);
					unset(self::access()->_kernelTime['__old']);
				}
				
				$totalSecs = 0;
				if (Utils::isArray(self::access()->_kernelTime)) { foreach (self::access()->_kernelTime as $key => $value) { $totalSecs = $totalSecs + $value; } }
				self::access()->_kernelTime['__total_s'] = $totalSecs;
				self::access()->_kernelTime['__total_ms'] = $totalSecs * 1000;
			}
			elseif (!strcmp($mark, '__get')){ return (self::access()->_kernelTime); }
			else {
				if (!Utils::isEmpty($mark)) {
					if (!self::access()->_kernelTime['__old']) { self::access()->_kernelTime['__init'] = self::execTime(); }
					else {
						$lastMark = self::access()->_kernelTime['__old'];
						self::access()->_kernelTime[$lastMark] = self::execTime();
					}
					self::access()->_kernelTime['__old'] =  $mark;
					self::access()->_kernelTime[$mark] = self::execTime();
				}
			}
		}
		
		/**
		 * Enska execution time
		 *
		 * @return	int
		 * @access	public
		 */
		static Function execTime()
		{
			if (self::access()->_execTime === 0) { $execT = self::access()->_execTime = Utils::timestamp(); }
			else {
				$execT = round(Utils::timestamp() - self::access()->_execTime, 3);
				self::access()->_execTime = 0;
				
			}
			return ($execT);
		}
		
		/**
		 * Get the current applications pool
		 * 
		 * @param	string $uri = null
		 * @return	string
		 * @access	public
		 */
		static Function getPool($uri=null)
		{
			self::access()->_isDefaultPool = true;
			$uri = (Utils::isEmpty($uri)) ? Context::enska('uri') : $uri;
			$tmp = explode('/', $uri);
			$pool = null;
			if (Config::get('settings')->alias) { $pool = Config::get('settings')->alias->$tmp[0]; }
			
			
			if (!Utils::isEmpty($pool)) {
				self::access()->_isDefaultPool = false;
				return($pool);
			}

			$pool = (Config::get('settings') && Config::get('settings')->default && Config::get('settings')->default->pool) ? Config::get('settings')->default->pool : 'app';
			if (!Utils::isEmpty($pool)) { return ($pool); }
			
			throw new \Enska\Exceptions\CoreException('Unable to get the applications pool: The default pool definition does not exist', '0022002');
		}
		
		/**
		 * Get the current application controller
		 * 
		 * @param	string $uri = null
		 * @param	string $pool = null
		 * @return	string
		 * @access	public
		 */
		static Function getApplication($uri=null, $_pool=null)
		{
			self::access()->_isDefaultApp = true;
			$uri = (Utils::isEmpty($uri)) ? Context::enska('uri') : $uri;
			$_pool = (Utils::isEmpty($_pool)) ? Context::enska('pool') : $_pool;
			$nbAddress = count(($address = explode('/', Utils::filter(ltrim($uri, '/'), Array(Utils::_HTML)))));
			$env = Context::enska('env');
			$home = (Config::get('settings')->$env) ? trim(Config::get('settings')->$env->home, '/') : null;
			
			if (!Utils::isEmpty($home)) {
				$nbHome = count(explode('/', $home));
				$i = 1;
				$res = array();
				$res[0] = $address[0];
				while ($i < $nbAddress) {
					if ($i > $nbHome) { $res[$i - $nbHome] = $address[$i]; }
					$i++;
				}
				$address = $res;
			}

			if (isset($address[1]) && !empty($address[1])) {
				if (!Utils::isEmpty($address[1])) {
					self::access()->_isDefaultApp = false;
					return ($address[1]);
				}
			}
			
			$pools = null;
			if (Config::get('settings')->alias instanceof DataStore) { $pools = Config::get('settings')->alias->gets(); }
			
			if (Utils::isArray($pools)) {
				foreach ($pools as $address => $apps) {
					$pool = explode('.', $address);
					if (strncmp($pool[0], $_pool, strlen($pool[0])) == 0) {
						$nb = count($apps = explode('.', $apps));
						if ($nb == 2) {
							$app = $apps[1];
							return ($app);
						}
						break;
					}
				}
			}
			
			$app = (Config::get('settings') && Config::get('settings')->default && Config::get('settings')->default->application) ? Config::get('settings')->default->application : 'main';
			if (!Utils::isEmpty($app)) {
				return ($app);
			}
			
			throw new \Enska\Exceptions\CoreException('The default application does not exist', '0022003');
		}

		/**
		 * Get the subdoamin name
		 * 
		 * @param	string $uri = null	
		 * @return	string
		 * @access	public
		 */
		static Function getSubdomain($uri=null)
		{
			$uri = (Utils::isEmpty($uri)) ? Context::enska('uri') : $uri;
			$res = (count(($tmp = explode('.', $uri))) > 1) ? $tmp[0] : null;
			return $res;
		}

		/**
		 * Check if the default applications pool
		 * 
		 * @return	bool
		 * @access	public
		 */
		static Function isDefaultPool()
		{
			return (self::access()->_isDefaultPool);
		}
		
		/**
		 * Check if the default application
		 * 
		 * @return	bool
		 * @access	public
		 */
		static Function isDefaultApp()
		{
			return (self::access()->_isDefaultApp);
		}
		
		/**
		 * Set the default pool to true
		 * 
		 * @return	void
		 * @access	public
		 */
		static Function setDefaultPool()
		{
			self::access()->_isDefaultPool = true;
		}
		
		/**
		 * Set the default app to true
		 * 
		 * @return	void
		 * @access	public
		 */
		static Function setDefaultApp()
		{
			self::access()->_isDefaultApp = true;
		}
		
		/**
		 * Terminate the application
		 *
		 * @param	int $exit = EnsKa::FAILED;
		 * @return	void
		 * @access	public
		 */
		static Function terminate($exit=FAILED)
		{
			EXIT ($exit);
		}
		
		
	Private
		/**
		 * Constructor
		 * 
		 * @return	Kernel
		 * @access	private
		 */
		Function __construct() {}
		
	Private
		/**
		 * Clone constructor
		 * 
		 * @return	Kernel
		 * @access	private
		 */
		Function __clone() {}
		
	Private
		/**
		 * Get the Kernel instance
		 *
		 * @return	Kernel
		 * @access	private
		 */
		static Function access()
		{
			if(self::$_instance == null) { self::$_instance = new self(); }
			return (self::$_instance);
		}
}
 
 ?>