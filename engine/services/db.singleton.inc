<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Service
 * @package		core.engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Services;
use \Enska\Interfaces\ISystem;
use	\Enska\Helpers\DataStore;
use \Enska\Drivers\MysqlDriver;

final class DB implements ISystem
{	
	/**
	 * DB instance
	 * @type	DB
	 */
	Private	static	$_instance = null;
	
	/**
	 * Drivers instances
	 * @type	array
	 */
	Private	static	$_drivers = array();
	
	/**
	 * Default driver type
	 * @type	string
	 */
	Private			$driverType = 'mysql';
	
	Public
		/**
		 * Load class configuration
		 *
		 * @return	void
		 * @access	public
		 */
		static Function load() {}
		
		/**
		 * Access to driver
		 *
		 * @param	string	$driver
		 * @param	steing	$driverType = null
		 * @return	Driver
		 * @access	public
		 */
		static Function access($driver, $driverType=null)
		{
			$driverType = (Utils::isEmpty($driverType)) ? self::_access()->driverType : $driverType;
			if(!isset(self::$_drivers[$driver])) {
				$driverClass = '\\Enska\\Drivers\\'.ucfirst($driverType).'Driver';
				self::$_drivers[$driver] = new $driverClass($driver);
			}
			return (self::$_drivers[$driver]);
		}
		
	Private
		/**
		 * Constructor
		 * 
		 * @return	DB
		 * @access	private
		 */
		Function __construct() {}
		
	Private
		/**
		 * Clone constructor
		 * 
		 * @return	DB
		 * @access	private
		 */
		Function __clone() {}
		
	Private
		/**
		 * Get the DB instance
		 *
		 * @return	DB
		 * @access	private
		 */
		static Function _access()
		{
			if(self::$_instance == null) {
				self::$_instance = new self();
			}
			return (self::$_instance);
		}
}
 
 ?>