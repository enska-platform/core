<?php

/**
 *Copyright (C) 2008 - 2015 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Service
 * @package		core.engine.service
 * @copyright	Copyright (c) 2008 - 2015 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Services;
use \Enska\Helpers\DataStore;
 
final class Redis
{
	/**
	 * Redis instance
	 * @type	Redis
	 */
	Private	static	$_instance = null;
	
    /**
     * DataStore result
     * @type DataStore
     */
    Private    $result;
    
    /**
     * Redis connexion
     * @type Redis
     */
    Private    $redis;
    
    Public
		/**
		 * Load Redis server default connexion
		 *
		 * @return	void
		 * @access	public
		 */
		static Function load()
		{
			$env = Context::enska('env');
			if ($env && Config::get('settings') && Config::get('settings')->$env && Config::get('settings')->$env->master && (strncmp(Config::get('settings')->$env->master->host, '@', 1))) {
                $port = (Config::get('settings')->$env->redis && Config::get('settings')->$env->redis->port) ? Config::get('settings')->$env->redis->port : 6379;
                self::access()->redis = new \Redis();
                self::access()->redis->connect(Config::get('settings')->$env->master->host, $port);
			}
		}
    
	Public
		/**
		 * Set a REDIS value
		 *
		 * @param	string $attr
		 * @param	mixed $value = null
		 * @return 	void
		 * @access	public
		 */
		static Function set($attr, $value=null)
		{
            $value = DataStore::create($value);
			self::access()->redis->set($attr, $value->gets(true));
		}
		
		/**
		 * Get a REDIS value
		 *
		 * @param	mixed $attr
		 * @return 	DataStore
		 * @access	public
		 */
		static Function get($attr)
		{
			self::access()->result = DataStore::create(self::access()->redis->get($attr));
            return (self::access()->result);
		}
		
	Private
		/**
		 * Constructor
		 *
		 * @return 	Redis
		 * @access	private
		 */
		Function __construct() {}
		
	Private
		/**
		 * Clone constructor
		 *
		 * @return 	Redis
		 * @access	private
		 */
		Function __clone() {}
		
	Private
		/**
		 * Get the Redis instance
		 *
		 * @return	Redis
		 * @access	private
		 */
		static Function access()
		{
			if (self::$_instance == null) {
				self::$_instance = new self();
			}
			return (self::$_instance);
		}
}

?>