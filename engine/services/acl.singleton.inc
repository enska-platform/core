<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Abstract
 * @package		core.engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Services;
use \Enska\Helpers\DataStore;
use	\Enska\Connectors\FS;

final class ACL
{
	/* mode constantes */
	const			_POOL = 'pool';
	const			_APP = 'app';
	const			_ROLE = 'role';
	
	/**
	 * ACL instance
	 * @type	ACL
	 */
	Private	static	$_instance;
	
	Public
		/**
		 * Check a user ACL
		 *
		 * @param	array $role
		 * @param	int $mode = ACL::_ROLE
		 * @return	bool
		 * @access	public
		 */
		static Function check($role, $mode=self::_ROLE)
		{
			if (!self::isEnable() || Utils::isEmpty($role)) { return (true); }
			if (!is_array($role)) { $role = array('role' => $role, 'type' => 'uid', 'id' => Sessions::getSession('uid')); }
			if (strcmp($role['type'], 'uid') == 0) { if (Users::isRoot($role['id'])) { return (true); }}
			if (!Utils::isArray(($rights = self::access()->getRole($role['role'], $mode)))) { return (true); }
			
			// Check GID && UID rights
			if (!strcmp($role['type'], 'uid')) {
				if (self::access()->_checkAUID($role, $rights) && self::access()->_checkUUID($role, $rights)) {
					return (true);
				}
				elseif (self::access()->_checkAGID($role, $rights)) {
					return (true);
				}
			}
			elseif (!strcmp($role['type'], 'gid')) {
				if (self::access()->_checkAGID($role, $rights) && self::access()->_checkUGID($role, $rights)) {
					$userRole = array('role' => $role['role'], 'type' => 'uid', 'id' => Sessions::getSession('uid'));
					if (self::access()->_checkUUID($userRole, $rights)) {
						return (true);
					}
				}
			}
			
			return (false);
		}
		
		/**
		 * Create an user ACL
		 *
		 * <sample param='role'>
		 * $role = new DataStore(
		 *		array(
		 *			'type'			=> \Enska\Services\ACL::_APP,
		 *			'role'			=> 'sys',
		 *			'acl'			=> 'monRole', 'poolName', 'poolName/appName',
		 *			'description'	=> 'Ma description',
		 *			
		 *			'rights'		=> array(
		 *			
		 *				'gid' => array(
		 *					'auth' => array(
		 *						1, 2, 3
		 *					),
		 *					'unauth' => array(
		 *						1, 2, 3
		 *					)
		 *				),
		 *				
		 *				'uid' => array(
		 *					'auth' => array(
		 *						1, 2, 3
		 *					),
		 *					'unauth' => array(
		 *						1, 2, 3
		 *					)
		 *				)
		 *			)
		 *		)
		 *	);
		 * </sample>
		 * @param	DataStore $role
		 * @return	bool
		 * @access	public
		 */
		static Function create($role)
		{	
			if (!($role instanceof DataStore)) { return (false); }
			
			if (!strcmp($role->type, self::_POOL) || !strcmp($role->type, self::_APP)) {
				$file = new FS(DOCUMENTROOT.Config::get('settings')->default->pools.'/'.$role->acl	.'/auth');
				$file->send(serialize($role->rights)	, array('save' => true, 'force' => true));
			}
			elseif (!strcmp($role->type, self::_ROLE)) {
				// Check ACL
				if (strcmp($role->role, 'sys') && strcmp($role->role, 'usr')) {
					Logs::errors(E_USER_NOTICE, 'Role must be "sys" or "usr"', 'acl.inc.class', '-', '-', 'acl');
				}
				if (Utils::isEmpty($role->acl, Utils::_STRING_)) {
					Logs::errors(E_USER_NOTICE, 'ACL must be a string', 'acl.inc.class', '-', '-', 'acl');
				}
				if (!Utils::isArray($role->rights)) {
					Logs::errors(E_USER_NOTICE, 'Rights must be an array', 'acl.inc.class', '-', '-', 'acl');
				}
				
				// Set ACL
				$datas = array('role' => $role->role, 'acl' => $role->acl, 'description' => $role->description, 'rights' => serialize($role->rights));
				
				// Write ACL in database
				$db = new mysql(Context::enska('authDB'));
				$db->table->insert('acls', $datas);
			}
			else { Logs::errors(E_USER_NOTICE, 'Unknown right mode', 'acl.inc.class', '-', '-', 'acl'); }
		}
		
		/**
		 * Check if the ACLs system is enable
		 *
		 * @param	array $role
		 * @param	array $rights
		 * @return	bool
		 * @access	private
		 */
		static Function isEnable() 
		{
			return ((Config::get('settings') && Config::get('settings')->default && Config::get('settings')->default->auth) ? Config::get('settings')->default->auth : false);
		}
		
	Private
		/**
		 * Get an ACL information
		 *
		 * @param	string	$role
		 * @param	int $mode = ACL::_ROLE
		 * @return	string
		 * @access	private
		 */
		Function getRole($role, $mode=self::_ROLE)
		{
			$res = null;
			
			if ((!Utils::isEmpty($role, Utils::_STRING_)) && (!Utils::isEmpty($mode, Utils::_STRING_))) {
				if (!strcmp($mode, self::_ROLE)) {
					$res = DB::access(Context::enska('authDSN'))->select('acls', array('fields' => array('rights'), 'where' => array('acl' => $role)));
					if (!is_object($res) || !$res->count() > 0) { return (null); }
					$res = $res->rights;
				}
				elseif (!strcmp($mode, self::_POOL)) {
					$acl = new FS(Context::enska('poolPath').'/auth');
					$res = Utils::strReplace(trim($acl->receive()), '\\"', '"');
				}
				elseif (!strcmp($mode, self::_APP)) {
					$acl = new FS(Context::enska('appPath').'/auth');
					$res = Utils::strReplace(trim($acl->receive()), '\\"', '"');
				}
				else { return (null); }
			}
			
			$res = (!Utils::isEmpty($res, Utils::_STRING_)) ? unserialize($res) : $res;
			return ($res);
		}
		
	Private
		/**
		 * Check rights for authorized group
		 *
		 * @param	array $role
		 * @param	array $rights
		 * @return	bool
		 * @access	private
		 */
		Function _checkAGID($role, $rights)
		{
			$right = false;
			
			if(isset($rights['gid'])) {
				$tab = $rights['gid'];
				if(isset($tab['auth'])) {
					foreach($tab['auth'] as $rid) {
						if (!strcmp($role['type'], 'uid')) {
							if (((int)$rid == 0) || (Users::inGroup($role['id'], $rid))) {
								$right = true;
								break;
							}
						}
						elseif (!strcmp($role['type'], 'gid')) {
							if (((int)$rid == 0) || ((int)$rid == (int)$role['id'])) {
								$right = true;
								break;
							}
						}
					}
				}
			}
			
			return($right);
		}

		/**
		 * Check rights for unauthorized group
		 *
		 * @param	array $role
		 * @param	array $rights
		 * @return	bool
		 * @access	private
		 */
		Function _checkUGID($role, $rights)
		{		
			$right = true;
			
			if(isset($rights['gid'])) {
				$tab = $rights['gid'];
				if(isset($tab['unAuth'])) {
					foreach($tab['unAuth'] as $rid) {
						if (strcmp($role['type'], 'uid') == 0) {
							if (((int)$rid == 0) || (Users::inGroup($role['id'], $rid))) {
								$right = false;
								break;
							}
						}
						elseif (strcmp($role['type'], 'gid') == 0) {
							if (((int)$rid == 0) || ((int)$rid == (int)$role['id'])) {
								$right = false;
								break;
							}
						}
					}
				}
			}
			
			return ($right);
		}
		
	Private
		/**
		 * Check rights for authorized user
		 *
		 * @param	array $role
		 * @param	array $rights
		 * @return	bool
		 * @access	private
		 */
		Function _checkAUID($role, $rights)
		{
			$right = false;
			
			if(isset($rights['uid'])) {
				$tab = $rights['uid'];
				if(isset($tab['auth'])) {
					foreach($tab['auth'] as $rid) {
						if (strcmp($role['type'], 'uid') == 0) {
							if (((int)$rid == 0) || ($role['id'] == $rid)) {
								$right = true;
								break;
							}
						}
					}
				}
			}
			
			return ($right);
		}

	Private
		/**
		 * Check rights for authorized user
		 *
		 * @param	array $role
		 * @param	array $rights
		 * @return	bool
		 * @access	private
		 */
		Function _checkUUID($role, $rights) 
		{
			$right = true;
			
			if(isset($rights['uid'])) {
				$tab = $rights['uid'];
				if(isset($tab['unAuth'])) {
					foreach($tab['unAuth'] as $rid) {
						if (strcmp($role['type'], 'uid') == 0) {
							if (($rid == 0) || ($rid == $role['id'])) {
								$right = false;
								break;
							}
						}
					}
				}
			}
			
			return ($right);
		}
		
	Private
		/**
		 * Constructor
		 *
		 * @return	ACL
		 * @access	private
		 */
		Function __construct() {}
	
	Private
		/**
		 * Clone constructor
		 *
		 * @return	ACL
		 * @access	private
		 */
		Function __clone() {}
		
	Private
		/**
		 * Get the ACL instance
		 *
		 * @return	ACL
		 * @access	private
		 */
		static Function access()
		{
			if (self::$_instance == null) { self::$_instance = new self(); }
			return (self::$_instance);
		}
}
 
 ?>