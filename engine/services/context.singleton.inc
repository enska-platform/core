<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Service
 * @package		core.engine.service
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Services;
use \Enska\Helpers\DataStore;
 
final class Context
{
	/**
	 * Configuration type
	 * @type	int
	 */
	const	_ENSKA = 1;
	
	/**
	 * Configuration type
	 * @type	int
	 */
	const	_USER = 2;
	
	/**
	 * Context instance
	 * @type	Context
	 */
	Private	static	$_instance = null;
	
	/**
	 * User store
	 * @type	DataStore
	 */
	Private	static	$_userStore = null;
	
	/**
	 * Enska store
	 * @type	DataStore
	 */
	Private	static	$_enskaStore = null;
	
	Public
		/**
		 * Set a Context value
		 *
		 * @param	string $attr
		 * @param	mixed $value = null
		 * @param	int $type = Context::_USER
		 * @return 	bool
		 * @access	public
		 */
		static Function set($attr, $value=null, $type=self::_ENSKA)
		{
			self::access();
			if ($type == self::_USER) {
				return (self::$_userStore->set($attr, $value));
			}
			else {
				return (self::$_enskaStore->set($attr, $value));
			}
		}
		
		/**
		 * get an user Context value
		 *
		 * @param	mixed $attr
		 * @return 	mixed
		 * @access	public
		 */
		static Function get($attr)
		{
			self::access();
			return (self::$_userStore->get($attr));
		}
		
		/**
		 * get an EnsKa Context value
		 *
		 * @param	mixed $attr
		 * @return 	mixed
		 * @access	public
		 */
		static Function enska($attr)
		{
			self::access();
			return (self::$_enskaStore->get($attr));
		}
		
		/**
		 * Debug the context object
		 *
		 * @param	string $store = 'enska'
		 * @return 	void
		 * @access	public
		 */
		static Function debug($store='enska')
		{
			self::access();
			if ($store === 'enska') {
				\debug::pr(self::$_enskaStore->gets());
			}
			elseif ($store === 'user') {
				\debug::pr(self::$_userStore->gets());
			}
		}
		
	Private
		/**
		 * Constructor
		 *
		 * @return 	Context
		 * @access	private
		 */
		Function __construct() {}
		
	Private
		/**
		 * Clone constructor
		 *
		 * @return 	Context
		 * @access	private
		 */
		Function __clone() {}
		
	Private
		/**
		 * Get the Context instance
		 *
		 * @return	Context
		 * @access	private
		 */
		static Function access()
		{
			if (self::$_instance == null) {
				self::$_instance = new self();
				self::$_userStore = new DataStore('Context_userStore');
				self::$_enskaStore = new DataStore('Context_enskaStore');
			}
			return (self::$_instance);
		}
}

?>