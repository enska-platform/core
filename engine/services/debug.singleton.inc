<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Helper
 * @package		core.engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

class debug
{
	const			_VAR_DUMP_ = 0;
	const			_ECHO_ = 1;
	
	Private	static	$_instance;
	
	Public
		static Function p($datas=null, $dType=debug::_VAR_DUMP_)
		{
			echo '<br><pre>';
			if ($dType == debug::_VAR_DUMP_) {
				var_dump($datas);
			}
			else {
				echo $datas;
			}
			echo '</pre><br>';
		}
		
		static Function pr($datas=null, $var=false)
		{
			if (!$var) {
				echo '<br><pre>';
				print_r($datas, false);
				echo '</pre><br>';
				return;
			}
			return(print_r($datas, true));
		}
	
		static Function pn($datas=null, $var=false)
		{
			if (!$var) {
				echo "\n";
				print_r($datas, false);
				echo "\n";
				return;
			}
			return(print_r($datas, true));
		}

		static Function jsonErr()
		{
			$constants = get_defined_constants(true);
			$json_errors = array();
			foreach ($constants['json'] as $name => $value) {
			    if (!strncmp($name, 'JSON_ERROR_', 11)) {
			        $json_errors[$value] = $name;
			    }
			}
			
			$res = null;
			if (isset($json_errors[json_last_error()]) && json_last_error() !== JSON_ERROR_NONE) {
				$res = $json_errors[json_last_error()];
			}
			return ($res);
		}
	
	Private
		static Function access()
		{
			if(self::$_instance == null) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}
		
	Private
		Function __construct() {}
		
	Private
		Function __clone() {}
}

Function debug($data=null, $var=false) { \debug::pn($data, $var); }
Function ln($data=null) { print($data."\n"); }

?>