<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Services
 * @package		core.engine.services
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Services;

use \Enska\Helpers\DataStore;

final class Users
{
	/* constantes */
	const			_ADD = 1;
	const			_REM = 2;
	const			_UPD = 3;
	
	const			_GROUP_USER = 'user';
	const			_GROUP_ADMIN = 'admin';
	const			_GROUP_OWNER = 'owner';
	const			_GROUP_BANNED = 'banned';
	
	/**
	 * Users instance
	 * @type	Users
	 */
	Private	static	$_instance;
	
	/**
	 * Users last error
	 * @type	string
	 */
	Private			$_error = null;
	
	Public
		/**
		 * Check if a user is a root user
		 *
		 * @param	mixed	$uid
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function isRoot($uid, $access=null)
		{
			if (ACL::isEnable()) {
				$access = ($access) ? $access : Context::enska('authDSN');
				$req = DB::access($access)->select('users_groups', array('fields' => array('id'), 'where' => array('uid' => self::getUid($uid, $access), 'AND' => array('gid' => self::getGid('roots', $access)))));
				if (!is_object($req) || !($req->count() > 0)) {
					return (false);
				}
			}
			return (true);
		}
		
		/**
		 * Check if a user own a group
		 *
		 * @param	mixed	$uid
		 * @param	mixed 	$gid
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function isOwner($uid, $gid, $access=null)
		{
			if (ACL::isEnable()) {
				$access = ($access) ? $access : Context::enska('authDSN');
				$req = DB::access($access)->select('users_groups', array('fields' => array(id), 'where' => array('uid' => self::getUid($uid, $access), 'AND' => array('gid' => self::getGid($gid, $access)))));
				if (is_object($req) && $req->count() > 0) {
					$req = DB::access($access)->select('users_groups',array('fields' => array('id'), 'where' => array('id' => $req->id, 'AND' => array('status' => 'owner'))));
					if (!is_object($req) || !($req->count() > 0)) {
						return (false);
					}
				}
			}
			return (true);
		}

		/**
		 * Get group gid by name
		 *
		 * @param	string	$group
		 * @param	string	$access = null
		 * @return	int
		 * @access	public
		 */
		static Function getGid($group, $access=null)
		{
			if (ACL::isEnable()) {
				if (Utils::isPositive((int)$group)) {
					return ((int)$group);
				}
				if (!Utils::isEmpty($group, Utils::_STRING_)) {
					$access = ($access) ? $access : Context::enska('authDSN');
					$req = DB::access($access)->select('groups', array('fields' => array('gid'), 'where' => array('name' => (string)$group)));
					if (is_object($req) && $req->count() > 0) {
						return ($req->gid);
					}
				}
			}
			return (0);
		}
		
		/**
		 * Get group name by id
		 *
		 * @param	mixed 	$group
		 * @param	string	$access = null
		 * @return	string
		 * @access	public
		 */
		static Function getGroup($gid, $access=null)
		{
			if (ACL::isEnable()) {
				if (!Utils::isEmpty($gid, Utils::_STRING_)) {
					return ((string)$gid);
				}
				$access = ($access) ? $access : Context::enska('authDSN');
				$req = DB::access($access)->select('groups', array('fields' => 'name', 'where' => array('gid' => (int)$gid)));
				if (is_object($req) && $req->count() > 0) {
					return ($req->name);
				}
			}
			return (null);
		}

		/**
		 * Get user uid by login
		 *
		 * @param	mixed 	$login
		 * @param	string	$access = null
		 * @return	int
		 * @access	public
		 */
		static Function getUid($login, $access=null)
		{
			//if (ACL::isEnable()) {
				if (Utils::isPositive((int)$login)) {
					return ((int)$login);
				}
				if (!Utils::isEmpty($login, Utils::_STRING_)) {
					$access = ($access) ? $access : Context::enska('authDSN');
					$req = DB::access($access)->query('SELECT `uid` FROM `users_authentification` WHERE `login` = '.(string)$login);
					if (is_object($req) && $req->count() > 0) {
						return ($req->uid);
					}
				}
			//}
			return (0);
		}

		/**
		 * Get user login by uid
		 *
		 * @param	mixed 	$uid
		 * @param	string	$access = null
		 * @return	string
		 * @access	public
		 */
		static Function getLogin($uid, $access=null)
		{
			//if (ACL::isEnable()) {
				if (!Utils::isEmpty($uid, Utils::_STRING_)) {
					return ((string)$uid);
				}
				$access = ($access) ? $access : Context::enska('authDSN');
				$req = DB::access($access)->select('users_authentification', array('fields' => 'login', 'where' => array('uid' => (int)$uid)));
				if (is_object($req) && $req->count() > 0) {
					return ($req->login);
				}
			//}
			return (null);
		}

		/**
		 * Check if the user is in a specific group
		 * 
		 * @param	mixed	$uid
		 * @param	mixed	$gid
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function inGroup($uid, $gid, $access=null)
		{
			if (ACL::isEnable()) {
				$access = ($access) ? $access : Context::enska('authDSN');
				$req = DB::access($access)->select('users_groups', array('fields' => array('id'), 'where' => array('uid' => self::getUid($uid, $access), 'AND' => array('gid' => self::getGid($gid, $access), 'AND' => array('status' => 'owner', 'OR' => array('status' => 'admin'), 'OR' => array('status' => 'user'))))));
				if (!is_object($req) || !($req->count() > 0)) {
					return (false);
				}
			}
			return (true);
		}
		
		/**
		 * Alias to addUser
		 *
		 * @param	mixed	$user
		 * @param	bool	$force = false
		 * @param	bool	$generatePassword = true
		 * @param	string	$access = null
		 * @return	int
		 * @access	public
		 */
		static Function userAdd($user, $force=false, $generatePassword=true, $access=null)
		{
			return (self::addUser($user, $force, $generatePassword, $access));
		}
		
		/**
		 * Create a new user
		 *
		 * @param	array 	$user
		 * @param	bool 	$force = false
		 * @param	bool 	$generatePassword = true
		 * @param	string	$access = null
		 * @return	int
		 * @access	public
		 */
		static Function addUser($user, $force=false, $generatePassword=true, $access=null)
		{
			self::error('You don\'t have enough rights to add users / ACLs are not enabled');
			if (ACL::isEnable() && ACL::check('user_add')) {
				$user = DataStore::create($user);
				$access = ($access) ? $access : Context::enska('authDSN');
				self::error('The user e-mail address is unvalid');
				if (!Utils::isEmpty($user->mail, Utils::_EMAIL_)) {
					if (!Utils::isEmpty($generatePassword, Utils::_BOOL_)) {
						$user->password = Utils::createId(50, true);
					}
					self::error('Password must be a 6 lenght string');
					if (!Utils::isEmpty($user->password, Utils::_STRING_, 6, '>=')) {
						$user->password = Utils::revStrMD5($user->password);
						if (!$user->login) { $user->login = $user->mail; }
						self::error('Login must be a 4 lenght string');
						if (!Utils::isEmpty($user->login, Utils::_STRING_, 4, '>=')) {
							self::error('commons.users.errors.exists');
							if (!self::getUid($user->login, $access)) {
								if (!Utils::isEmpty($force, Utils::_BOOL_) && ACL::check('user_add_advance')) {
									if (!$user->status) { $user->status = 'enable'; }
								}
								elseif (!Utils::isEmpty($force, Utils::_BOOL_) && ACL::check('user_add')) {
									if (!$user->status) { $user->status = 'waiting'; }
								}
								else { $user->status = 'disable'; }

								$insert = DataStore::create();
								$insert->login = $user->login;
								$insert->password = $user->password;
								$insert->mail = $user->mail;
								$insert->status = $user->status;
								$insert->user = 'usr';

								$uid = DB::access($access)->insert('users_authentification', $insert->gets());
								self::error('Unable to create the user: '.DB::access($access)->getLastError());
								if (Utils::isPositive($uid)) {
									$gid = DB::access($access)->insert('users_groups', array('uid' => $uid, 'gid' => 3, 'status' => 'user'));
									self::error('Unable to add the user in group: '.DB::access($access)->getLastError());
									if (Utils::isPositive($gid)) {
										DB::access($access)->insert('users', array('uid' => $uid));
										self::error('Unable to add the user informations: '.DB::access($access)->getLastError());
										//if (Utils::isPositive($uuid)) {
											DB::access($access)->insert('users_tokens', array('uid' => $uid, 'token' => Sessions::getToken()));
											DB::access($access)->insert('users_trace', array('uid' => $uid));
											$res = new DataStore(array('uid' => $uid, 'gid' => $gid));
											self::error();
											return ($uid);
										/*}
										else {
											//DB::access($access)->delete('users_authentification', array('uid' => $uid));
											//DB::access($access)->delete('users_groups', array('uid' => $uid));
											//DB::access($access)->delete('users', array('uid' => $uid));
										}*/
									}
									else {
										DB::access($access)->delete('users_authentification', array('uid' => $uid));
									}
								}
							}
						}
					}
				}
			}
			return (0);
		}

		/**
		 * Alias to delUser
		 *
		 * @param	mixed	$login
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function userDel($login, $access=null)
		{
			return (self::delUser($login, $access));
		}
		
		/**
		 * Delete an user
		 *
		 * @param	mixed	$login
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function delUser($login, $access=null)
		{
			self::error('You don\'t have enough rights to delete users / ACLs are not enabled');
			if (ACL::isEnable() && ACL::check('user_delete')) {
				$access = ($access) ? $access : Context::enska('authDSN');
				$uid = self::getUid($login, $access);
				self::error('Unknown user');
				if (Utils::isPositive($uid)) {
					self:error('You can\'t delete a system user');
					if ($uid > 5) {
						self::error('You can\'t delete your own user');
						if (Sessions::getSession('uid') != $uid) {
							self::error();
							if (!ACL::check('user_delete_advance')) {
								DB::access($access)->update('users_authentification', array('fields' => array('status' => 'disable'), 'where' => array('uid' => $uid)));
								return (ture);
							}
							DB::access($access)->delete('users_authentification', array('uid' => $uid));
							DB::access($access)->delete('users_groups', array('uid' => $uid));
							return (true);
							
						}
					}
				}
			}
			return (false);
		}

	/**
	 * Edit a user
	 *
	 * @param	mixed	$user
	 * @param	string	$access = null
	 * @return	bool
	 * @access	public
	 */
	static Function editUser($user, $access=null)
	{
		self::error('You don\'t have enough rights to edit users / ACLs are not enabled');
		if (ACL::isEnable() && ACL::check('user_edit')) {
			$user = DataStore::create($user);
			$access = ($access) ? $access : Context::enska('authDSN');
			self::error('UID / Login is needed');
			if ($user->uid) {
				$uid = self::getUid($user->uid);
				$login = self::geLogin($user->uid);
				$edit = new DataStore();

				if ($user->mail) {
					self::error('The user e-mail address is unvalid');
					if (!Utils::isEmpty($user->mail, Utils::_EMAIL_)) {
						return (false);
					}
					$edit->mail = $user->mail;
				}

				if ($user->login) {
					self::error('The user login is unvalid');
					if (Utils::isEmpty($user->login, Utils::_STRING_, 4, '>=')) {
						return (false);
					}
					$edit->login = $user->login;
				}

				if ($user->status) {
					self::error('You don\'t have enought rights to edit this user status');
					if (!ACL::check('user_edit_advance')) {
						return (false);
					}
					self::error('The user status is unvalid');
					if (strcmp($user->status, 'enable') && strcmp($user->status, 'disable')) {
						return (false);
					}
					$edit->status = $user->status;
				}

				if ($user->password) {
					self::error('Password must be a 6 lenght string');
					if (Utils::isEmpty($user->password, Utils::_STRING_, 5, '>')) {
						return (false);
					}
					$edit->password = String::revStrMD5($user->password);
				}

				self::error('You don\'t have enought rights to edit this user (1)');
				if ((!ACL::check('user_edit_advance')) && (Sessions::getSession('uid') != $uid)) {
					return (false);
				}

				if ($uid < 5) {
					self::error('You don\'t have enought rights to edit this user (2)');
					if (Sessions::getSession('uid') != $uid || !self::isRoot(Sessions::getSession('uid'), $access)) {
						return (false);
					}
				}

				$fields = $edit->gets();
				$where = array('uid' => $uid);
				DB::access($access)->update('users_authentification', array('fields' => $fields, 'where' => $where));
				self::error(DB::access($access)->getLastError());
				return (true);
			}
		}
		return (false);
	}

		/**
		 * Alias to Edit a user
		 *
		 * @param	mixed	$user
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function userEdit($user, $access=null)
		{
			return (self::editUser($user, $access));
		}

		/**
		 * Alias for changeGroup
		 * 
		 * @param	mixed	$uid
		 * @param	mixed	$gid
		 * @param	int		$mode
		 * @param	string	$status = null
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function groupChange($uid, $gid, $mode=self::_ADD, $status=self::_GROUP_USER, $access=null)
		{
			return (self::changeGroup($uid, $gid, $mode, $status, $access));
		}
		
		/**
		 * Add an user in a specific group
		 * 
		 * @param	mixed	$uid
		 * @param	mixed	$gid
		 * @param	int		$mode
		 * @param	string	$status = null
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function changeGroup($uid, $gid, $mode=self::_ADD, $status=self::_GROUP_USER, $access=null)
		{
			self::error('You don\'t have enough rights to edit users / ACLs are not enabled');
			if (ACL::isEnable() && ACL::check('group_change')) {
				$access = ($access) ? $access : Context::enska('authDSN');
				$uid = self::getUid($uid);
				$gid = self::getGid($gid);
				self::error('Unknown uid/gid');
				if (Utils::isPositive($uid) && Utils::isPositive($gid)) {
					self::error('Unknown change mode');
					if ($mode == self::_ADD) {
						self::error('User already in target group');
						if (self::inGroup($uid, $gid, $access)) {
							return (false);
						}
						
						self::error('You don\'t have enought rights to change this user');
						if (self::isRoot($uid, $access) && !ACL::check('group_change_advance')) {
							return (false);
						}
						
						if (Utils::isPositive(DB::access($access)->insert('users_groups', array('uid' => $uid, 'gid' => $gid, 'status' => $status)))) {
							self::error();
							return (true);
						}
						self::error('Error on user change: '.DB::access($access)->getLastError());
					}
					elseif ($mode == self::_REM) {
						self::error('User not present in target group');
						if (!self::inGroup($uid, $gid, $access)) {
							return (false);
						}
						
						self::error('You don\'t have enought rights to change this user');
						if (self::isRoot($uid, $access) && !ACL::check('group_change_advance')) {
							return (false);
						}
						
						DB::access($access)->delete('users_groups', array('uid' => $uid, 'gid' => $gid));
						self::error(DB::access($access)->getLastError());
						return (true);
					}
				}
			}
			return (false);
		}
	
		/**
		 * Create a new group
		 *
		 * @param	mixed	$group
		 * @param	string	$access = null
		 * @return	int
		 * @access	public
		 */
		static Function groupAdd($group, $access=null)
		{
			self::error('You don\'t have enough rights to add groups / ACLs are not enabled');
			if (ACL::isEnable() && ACL::check('group_add')) {
				$group = DataStore::create($group);
				$access = ($access) ? $access : Context::enska('authDSN');
				$insert = new DataStore();
				self::error('Group name needed');
				if (!Utils::isEmpty($group->name, Utils::_STRING_, 3, '>=')) {
					if (!$group->visibility && !in_array($group->visibility, array('private', 'protected', 'public'))) {
						$group->visibility = 'private';
					}
					$insert->name = $group->name;
					$insert->visibility = $group->visibility;
					$insert->group = 'usr';
					self::error();
					return (DB::access($access)->insert('groups', $insert->gets()));
				}
			}
			return (0);
		}
		
		/**
		 * Edit a group
		 *
		 * @param	mixed	$group
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function groupEdit($group, $access=null)
		{
			self::error('You don\'t have enough rights to edit groups / ACLs are not enabled');
			if (ACL::isEnable() && ACL::check('group_edit')) {
				$access = ($access) ? $access : Context::enska('authDSN');
				$group = DataStore::create($group);
				$edit = new DataStore();
				self::error('GID/Group is needed');
				if ($group->gid) {
					$gid = self::getGid($group->gid, $access);
					self::erro('Group name must be a 3 lenght string');
					if (Utils::isEmpty($group->name, Utils::_STRING_, 3, '>=')) {
						return (false);
					}
					$edit->name = $group->name;
					
					self::error('You don\'t have enought rights to edit this group');
					if (!ACL::check('group_edit_advance')) {
						return (false);
					}
					self::error('The group visibility is unvalid');
					if ($group->visibility && !in_array($group->visibility, array('private', 'protected', 'public'))) {
						return (false);
					}
					$edit->visibility = $group->visibility;
					if ($gid < 5) {
						self::error('You don\'t have enought rights to edit this group');
						if (!ACL::check('group_edit_advance') && !isOwner(Sessions::getSession('uid'), $gid, $access)) {
							return (false);
						}
					}
					DB::access($access)->update('groups', array('fields' => $edit->gets(), 'where' => array('gid' => $gid)));
					self::error(DB::access($access)->getLastError());
					return (true);
				}
			}
			return (false);
		}

		/**
		 * Delete a group
		 *
		 * @param	mixed	$name
		 * @param	string	$access = null
		 * @return	bool
		 * @access	public
		 */
		static Function groupDel($name, $access=null)
		{
			self::error('You don\'t have enough rights to delete groups / ACLs are not enabled');
			if (ACL::isEnable() && ACL::check('group_delete')) {
				$access = ($access) ? $access : Context::enska('authDSN');
				$gid = self::getGid($name, $access);
				if (Utils::isPositive($gid)) {
					self::error('You can\'t delete a system group');
					if ($gid > 5) {
						DB::access($access)->delete('groups', array('gid' => $gid));
						self::error(DB::access($access->getLastError()));
						return (true);
					}
				}
			}
			return (false);
		}
	
		/**
		 * Get the latest users error
		 *
		 * @param	$error = null
		 * @return	string
		 * @access	public
		 */
		static Function error($error=null)
		{
			self::access()->_error = $error;
		}
	
		/**
		 * Get the latest users error
		 *
		 * @return	string
		 * @access	public
		 */
		static Function getLastError()
		{
			return (self::access()->_error);
		}

	Private
		/**
		 * Constructor
		 *
		 * @return	Users
		 * @access	private
		 */
		Function __construct() {}

	Private
		/**
		 * Clone constructor
		 *
		 * @return	Users
		 * @access	private
		 */
		Function __clone() {}

	Private
		/**
		 * Get the Users instance
		 *
		 * @return	Users
		 * @access	private
		 */
		static Function access()
		{
			if(self::$_instance == null) {
				self::$_instance = new self();
			}
			return (self::$_instance);
		}
}