<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Abstract
 * @package		engine.abstracts
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Abstracts;
use \Enska\Helpers\Subject;
 
abstract class Sessions extends Subject
{
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null;
		 * @param	array 	$datas = null;
		 * @return	Sessions
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
		}
		
	/************/
	/* ABSTRACT */
	/************/
	
		/**
		 * Session starter
		 * 
		 * @return	void
		 * @access	private
		 */
		Abstract Public Function start();
		
		/**
		 * Set a session variable
		 * 
		 * @param	string	$attr
		 * @param	mixed	$value = null
		 * @return	void
		 * @access	public
		 */
		Abstract Public Function setSession($attr, $value=null);
		
		/**
		 * Get a session variable
		 * 
		 * @param	string	$attr
		 * @return	mixed
		 * @access	public
		 */
		Abstract Public Function getSession($attr);
		
		/**
		 * Get all session variables
		 * 
		 * @return	mixed
		 * @access	public
		 */
		Abstract Public Function gets();
		
		/**
		 * Delete a session variable
		 * 
		 * @param	string	$attr
		 * @return	mixed
		 * @access	public
		 */
		Abstract Public Function delete($attr);
		
		/**
		 * Destroy the current session
		 * 
		 * @return	void
		 * @access	public
		 */
		Abstract Public Function destroy();
}

?>