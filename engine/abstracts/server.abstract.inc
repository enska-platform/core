<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Abstract
 * @package		engine.abstracts
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Abstracts;

use \Enska\Helpers\Model;
use \Enska\Helpers\DataStore;
use	\Enska\Connectors\Socket;

abstract class Server extends Model
{
	/**
	 * Socket instance
	 * @var	Socket
	 */
	Public	$socket = null;
	 
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null
		 * @param	mixed 	$datas = null
		 * @return	Server
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
		}
		
		/**
		 * Start the server
		 * 
		 * @return	{this}
		 * @access	public
		 */
		Function start()
		{
			$timeout = ($this->store()->timeout) ? (int)$this->store()->timeout : 10;
			ini_set('default_socket_timeout', $timeout);
			$this->store()->set('api', DataStore::create(array(
				'onConnect'		=> function ($socket) { \Enska\Services\Server::connect($socket); },
				'onDisconnect'	=> function ($socket) { \Enska\Services\Server::disconnect($socket); },
				'handler'		=> function ($socket, $buffer) { \Enska\Services\Server::handler($socket, $buffer); }
			)));
			$this->socket = new Socket('RTServer', $this->store());
			return ($this);
		}
		
		/**
		 * Stop the current server instance and disconnect all clients
		 * 
		 * @return	void
		 * @access	public
		 */
		Function stop()
		{
			$this->socket->close();
		}
		
		/**
		 * Turn the socket to listen mode
		 * 
		 * @return	void
		 * @access	public
		 */
		Function listen()
		{
			$this->socket->receive();
			while(true) { $this->socket->select(); }
		}
		
		/**
		 * Add a client to server sockets
		 * 
		 * @param	resource	$socket
		 * @return	void
		 * @access	public
		 */
		Function connect($socket)
		{
			$this->socket->addSocket($socket);
		}
		
		/**
		 * Remove a client from server sockets
		 * 
		 * @param	resource	$socket
		 * @return	void
		 * @access	public
		 */
		Function disconnect($socket)
		{
			$this->socket->remSocket($socket);
		}
		
		/**
		 * Broadcast an event to all clients
		 * 
		 * @param	mixed	$buffer
		 * @return	void
		 * @access	public
		 */
		Function broadcast($buffer)
		{
			Server::broadcast($buffer);
		}
		
	/************/
	/* ABSTRACT */
	/************/
		/**
		 * Server main handler
		 * 
		 * @param	Client	$client
		 * @param	string	$buffer
		 * @return	void
		 * @access	public
		 */
		Abstract Public Function handler($client, $buffer);
}

?>