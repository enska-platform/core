<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Abstract
 * @package		engine.abstracts
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Abstracts;

use \Enska\Connectors\CMD;
use \Enska\Helpers\Model;
use \Enska\Helpers\Route;
use \Enska\Helpers\Reponse;
use \Enska\Helpers\App;
use	\Enska\Helpers\Login;
use \Enska\Helpers\cmdp;
use \Enska\Services\Config;
use \Enska\Services\Sessions;
use \Enska\Services\ACL;
use \Enska\Services\Utils;
use \Enska\Services\Context;

abstract class Controller extends Model
{
	/**
	 * Route instance
	 * @type	Route
	 */
	Protected	$route = null;
	
	/**
	 * Model instance
	 * @type	Object
	 */
	Protected	$instance = null;
	
	/**
	 * JSON instance
	 * @type	JSON
	 */
	Protected	$stream = null;
	
	/**
	 * View instance
	 * @type	APP
	 */
	Protected	$app = null;
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null;
		 * @param	array 	$datas = null;
		 * @return	Controller
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
			
			$this->route = new Route('mainRoute', $datas);
			$this->stream = new Reponse($id, $datas);
			$this->app = new APP($id, $datas);
			
			$this->route->route();
			$this->route->sanitize();
			if ($this->_isLogged()) {
				if (!$this->route->route->login && !$this->route->route->logout && !$this->route->route->islogged) { $this->execute(); }
			}
			else { $this->_login(); }
			if ($this->store()->options){ if ($this->store()->options->debug || $this->store()->flags->d) { return; }}
			
			$this->stream->write();
		}
		
	Private
		Function _login()
		{
			if (Context::enska('CLI_REQUEST')) { $this->stream->push('LOGIN:::Not logged-in Appcooker cloud, please use the following token'."\n\n".Sessions::getToken()."\n\n"); return; }
			$this->stream->push(array('auth' => 'LOGIN', 'token' => Sessions::getToken()));
		}
		
	Protected
		Function console($str)
		{
			if (!strcmp($this->getId(), 'cli') && ($this->store()->flags->d || $this->store()->options->debug)) {
				$this->stream->setVisible(false);
				CMD::console("$str"); 
			}
		}
		
		Function usage($str)
		{
			if (!strcmp($this->getId(), 'cli') && (!$this->store()->flags->q && !$this->store()->options->quiet)) {
				$this->stream->setVisible(false);
				CMD::usage("$str");
			}
		}
		
	Private
		/**
		 * Check if the current user is already logged
		 * 
		 * @return	bool
		 * @access	private
		 */
		Function _isLogged()
		{
			$auth = (Config::get('settings') && Config::get('settings')->default && Config::get('settings')->default->auth) ? Config::get('settings')->default->auth : false;
			if (!$auth) { return (true); }

			$auth = new Login();
			if ($this->route->route->login) {
				if ($auth->authenticate($this->route->route->post->login, $this->route->route->post->password)) {
					$this->stream->push(array('auth' => 'SUCCESS')); return (true);
				}
				else { $this->stream->push(array('auth' => 'FAILED')); return (false); }
			}
			elseif ($this->route->route->logout) {
				$auth->disconnect();
				$this->stream->push(array('auth' => 'SUCCESS')); return (false);
			}
			elseif ($this->route->route->islogged) {
				if ($auth->login()) { $this->stream->push(array('auth' => 'SUCCESS')); return (true); }
			}
			else {
				$auth->login();
				if (((ACL::check(Context::enska('pool'), ACL::_POOL)) && (ACL::check(Context::enska('app'), ACL::_APP)))) {
					return (true);
				}
			}
			
			return (false);
		}
		
	Private
		/**
		 * Execute the current route
		 * 
		 * @return	bool
		 * @access	private
		 */ 
		Function execute()
		{
			if (ACL::check($this->route->route->acl)) {
				if (strcmp($this->route->route->class, 'crud')) {
					$method = $this->route->route->method;
					if (!$this->route->route->class) {
						if (method_exists($this, $method)) { $this->$method(); return; }
					}
					else {
						try {
							if($this->route->route->method) { if (method_exists($this, $method)) { $this->$method(); return;}}
							if (\Imports::model($this->route->route->class)) {
								$_class = new \ReflectionClass($this->route->route->class);
								$this->instance = $_class->newInstance(Utils::createId(16, true, true), $this->route->route);
								if (method_exists($this->instance, $method)) { $this->instance->$method($this->route->route->argv, $this->route->route, $this->stream); return; }
								else {
									if (method_exists($this, $this->route->route->class)) {
										$this->{$this->route->route->class}($this->route->route); }
									return;
								}
							}
						}
						catch(Exception $e) { /* MODEL EXCEPTION */ return; }
					}
					$this->Main();
				}
				else { /* CRUD ACTIONS */ return; }
			}
			else { /* ERROR ACLS */ return; }
		}

	Protected
		/**
		 * Display template
		 * 
		 * @access	protected
		 */
		Function templatize()
		{
			$section = ($this->route->route->class) ? $this->route->route->class : null;
			$tpl = ($this->route->route->method) ? $this->route->route->method : null;
			if ($section && $tpl) { $this->app->add($section.'/'.$tpl); }
			$this->app->display();
			$this->stream->setVisible(false);
		}
		
	/************/
	/* ABSTRACT */
	/************/
		/**
		 * Main controller entry point
		 *
		 * @return	void
		 * @access	public
		 */
		Abstract Public Function Main();
}

?>