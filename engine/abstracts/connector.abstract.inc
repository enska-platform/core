<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Abstract
 * @package		engine.abstracts
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Abstracts;
use \Enska\Helpers\Model;
 
abstract class Connector extends Model
{
	/**
	 * IO String stream
	 * @type	string
	 */
	Protected	$_buffer = null;
	
	/**
	 * Object connector
	 * @type	Connector
	 */
	Protected	$auth = null;
		
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null
		 * @param	array 	$datas = null
		 * @return	IO
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
		}
		
	/************/
	/* ABSTRACT */
	/************/
		/**
		 * Open stream
		 *
		 * @return	bool
		 * @access	public
		 */
		Abstract Public Function open();
		
		/**
		 * Close stream
		 *
		 * @return	void
		 * @access	public
		 */
		Abstract Public Function close();
		
		/**
		 * Get stream datas
		 *
		 * @param	array $filter = null
		 * @return	string
		 * @access	public
		 */
		Abstract Public Function receive($filters=null);
		
		/**
		 * Set datas stream
		 *
		 * @param	string	$buffer = null
		 * @param	array 	$options = null
		 * @return	bool
		 * @access	public
		 */
		Abstract Public Function send($buffer=null, $options=null);
}

?>