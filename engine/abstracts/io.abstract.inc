<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Abstract
 * @package		engine.abstracts
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Abstracts;
use \Enska\Helpers\Model;
use \Enska\Helpers\DataStore;
use \Enska\Services\Utils;

abstract class IO extends Model
{
	/**
	 * IO modes
	 * @type	int
	 */
	const		_NONE = -1;
	const		_PRINT = 0;
	const		_RETURN = 1;
	const		_DEBUG = 2;
	const		_SEND = 3;
	
	/**
	 * IO datas stream
	 * @type	DataStore
	 */
	Public	$stream = null;
	
	/**
	 * IO String stream
	 * @type	string
	 */
	Protected	$_buffer = null;
	
	/**
	 * IO mode stream
	 * @type	int
	 */
	Protected	$_mode = -1;
	
	/**
	 * Object connector
	 * @type	Connector
	 */
	Protected	$connector = null;
		
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null
		 * @param	array 	$datas = null
		 * @return	IO
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
			$this->_init();
		}
		
		/**
		 * Magic alias for set
		 *
		 * @param	mixed $attr
		 * @param	mixed $value = null
		 * @return	DataStore
		 * @access	public
		 */
		Function __set($attr, $value=null)
		{
			$this->stream->$attr = $value;
			return $this->stream->$attr;
		}
		
		/**
		 * Magic alias for get
		 *
		 * @param	mixed $attr
		 * @return	mixed
		 * @access	public
		 */
		Function __get($attr)
		{
			return ($this->stream->$attr);
		}
		
		/**
		 * Send datas to stream
		 *
		 * @param	mixed $options = null
		 * @return	mixed
		 * @access	public
		 */
		Function send($options=null)
		{
			if ($this->_mode == IO::_PRINT) { echo $this->_buffer; }
			elseif ($this->_mode == IO::_RETURN) { return ($this->_buffer); }
			elseif ($this->_mode == IO::_DEBUG) { debug::p($this->_buffer); }
			elseif ($this->_mode == IO::_SEND) {
				$mtd = isset($options['send']) ? $options['send'] : 'send';
				$mtd = isset($this->store()->overrides['send']) ? $this->store()->overrides['send'] : $mtd;
				$res = $this->connector->$mtd($this->_buffer, $options);
				if (!Utils::isEmpty(($err = $this->connector->getLastError()))) { $this->error($err); }
				return ($res);
			}
		}
		
		/**
		 * Receive datas from stream
		 *
		 * @param	mixed $options = null
		 * @return	mixed
		 * @access	public
		 */
		Function receive($options=null)
		{
			$mtd = isset($options['receive']) ? $options['receive'] : 'receive';
			$mtd = isset($this->store()->overrides['receive']) ? $this->store()->overrides['receive'] : $mtd;
			$res = $this->connector->$mtd($this->getId());
			if (!Utils::isEmpty(($err = $this->connector->getLastError()))) { $this->error($err); }
			return ($res);
		}
		
	Private
		/**
		 * Init the class
		 *
		 * @return	void
		 * @access	private
		 */
		final Function _init()
		{
			$this->stream = new DataStore();
			if ($this->store()->mode) { $this->_mode = $this->store()->mode; }
			
			if ($this->store()->connector) {
				if ($this->_mode == IO::_NONE) { $this->_mode = IO::_SEND; }
				$_class = new \ReflectionClass('\Enska\Connectors\\'.$this->store()->connector);
				$this->connector = $_class->newInstance($this->getId(), $this->store()->gets());
			}
			elseif (!$this->store()->connector && ($this->_mode == IO::_NONE)) { $this->_mode = IO::_PRINT; }
		}
		
	/************/
	/* ABSTRACT */
	/************/
		/**
		 * Read from stream
		 *
		 * @return	string
		 * @access	public
		 */
		Abstract Public Function read($options=null);
		
		/**
		 * Write into stream
		 *
		 * @param	array $options = null
		 * @return	void
		 * @access	public
		 */
		Abstract Public Function write($options=null);
}

?>