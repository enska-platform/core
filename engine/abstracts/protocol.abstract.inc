<?php

/**
 *Copyright (C) 2015 Appcooker.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Abstract
 * @package		engine.abstracts
 * @copyright	Copyright (c) 2015 AppcookerIO. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\Abstracts;

use \Enska\Helpers\Model;
 
abstract class Protocol extends Model
{
	/**
	 * Client is Handshaked
	 * @var	bool
	 */
	Public	$isHandshaked = true;
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null;
		 * @param	array 	$datas = null;
		 * @return	Protocol
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
		}
		
	/************/
	/* ABSTRACT */
	/************/
	
		/**
		 * Do handshake
		 * 
		 * @param	Socket	$socket
		 * @param	String	$buffer
		 * @return	bool
		 * @access	public
		 */
		Abstract Public Function doHandshake($socket, $buffer);
	
		/**
		 * Encode object for outgoing transmition
		 * 
		 * @param	mixed	$payload
		 * @return	string
		 */
		Abstract Public Function encode($data);
		
		/**
		 * Decode object for incoming transmition
		 * 
	  	 * @param	mixed	$payload
		 * @return	string
		 * @access	public
		 */
		Abstract Public Function decode($data);
}

?>