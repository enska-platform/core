<?php

/**
 *Copyright (C) 2008 - 2014  EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Bootstrap
 * @package		core.bootstrap
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Bootstrap;
use \Enska\Exceptions\BootstrapException;
use \Enska\Helpers\Bootstrap;
use \Enska\Services\Context;
use \Enska\Services\Kernel;

final class Enska extends Bootstrap
{	
	Public
		/**
		 * System powerOn
		 *
		 * @param	array 	$argv = null
		 * @return	void
		 * @access	public
		 */
		static Function powerOn($argv=null)
		{
			Context::set('HTTP_REQUEST', true);
			self::start()->load();
			self::run();
		}
	
		/**
		 * After load
		 *
		 * @return	void
		 * @access	protected
		 */
		static Function run()
		{
			if (!\Imports::controller()) {
				throw new BootstrapException('## Platform ERROR ## : Unable to load the application controller', '1001');
			}
			
			$controller = '\Applications\\'.Context::enska('pool').'\\'.Context::enska('app').'\\ApplicationController';
			$application = new $controller('http');
		}
}

?>