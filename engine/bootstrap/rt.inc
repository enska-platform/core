<?php

/**
 *Copyright (C) 2008 - 2014  EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Bootstrap
 * @package		core.bootstrap
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Bootstrap;

use \Enska\Helpers\Bootstrap;
use \Enska\Helpers\CMDP;
use \Enska\Services\Context;
use \Enska\Services\Config;
use \Enska\Services\Utils;
use \Enska\Services\Server;

final class Enska extends Bootstrap
{	
	Public
		/**
		 * System powerOn
		 *
		 * @param	array 	$argv = null
		 * @return	void
		 * @access	public
		 */
		static Function powerOn($argv=null)
		{
			array_shift($argv);
			Context::set('RTServer', true);
			Context::set('ARGVS', $argv);
			self::start()->load();
			self::run();
		}
	
		/**
		 * After load
		 *
		 * @return	void
		 * @access	protected
		 */
		static Function run()
		{
			$cmd = new cmdp();
			$cmd = $cmd->cmd;
			$argv = $cmd->arguments->gets();
			$config = (!Utils::isEmpty($argv[0])) ? $argv[0] : 'default';
			$env = Context::enska('env');
			
			if (!($conf = Config::get('settings')->$env->rt->$config)) { throw new \Exception("RTServerEngine is not enable ! (Review platform settings, section rt) ", 1); }
			Context::set('rt', $conf); Context::set('cmd', $cmd);
			Server::start();
		}
}

?>