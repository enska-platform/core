<?php

/**
 *Copyright (C) 2008 - 2014  EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Bootstrap
 * @package		core.bootstrap
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Bootstrap;

use \Enska\Helpers\Bootstrap;
use \Enska\Helpers\CMDP;
use \Enska\Services\Context;
use \Enska\Services\Utils;
use \Enska\Services\Config;

final class Enska extends Bootstrap
{
	Public
		/**
		 * System powerOn
		 * 
		 * @param	array	$argv = null
		 * @return	void
		 * @access	public
		 */
		static Function powerOn($argv=null)
		{
			array_shift($argv);
			Context::set('CLI_REQUEST', true);
			Context::set('ARGVS', $argv);
			
			self::start()->load();
			self::run();
		}
	
		/**
		 * After load
		 *
		 * @return	void
		 * @access	public
		 */
		static Function run()
		{
			$cmd = new cmdp();
			$cmd = $cmd->cmd;
			$argv = $cmd->arguments->gets();
			
			if (isset($argv[0]) && !Utils::isEmpty($argv[0])) {
				$argv = explode('/', $argv[0]); $len = count($argv); $i = 0;
				if ($len > 1) {
					while ($i < $len) {
						if ($i == 0) { Context::set('app', $argv[0]); }
						elseif($i == 1) { $cmd->vars->mod = $argv[1]; }
						elseif($i == 2) { $cmd->vars->target = $argv[2]; }
						$i++;
					}
				}
			}
			
			$env = Context::enska('env');
			$ns = (Config::get('settings') && Config::get('settings')->$env && Config::get('settings')->$env->namespace) ? Config::get('settings')->$env->namespace : 'Agent';
			Context::set('appPath', strtolower($ns).'/'.Context::enska('pool').'/'.Context::enska('app'));
			$controller = $ns.'\\'.Context::enska('pool').'\\'.Context::enska('app').'\\ApplicationController';			
			
			try {
				$path = self::_access()->getCurrentAppPath().'/controller.php';
				if (include_once($path)) {
					$domain = (Config::get('settings') && Config::get('settings')->default && Config::get('settings')->default->domain) ? Config::get('settings')->default->domain : 'enskacloud.local';
					$uri = Context::enska('pool').'.'.$domain.'/'.Context::enska('app').'/'.$cmd->vars->mod.'/'.$cmd->vars->target;
					Context::set('uri', rtrim($uri, '/').'/'); Context::set('cmd', $cmd);
					$application = new $controller('cli', $cmd);
				}
			}
			catch (\Exception $e) { debug($e); throw new \Exception("Error on prcess application files :: ".$e->getMessage()); }
		}
}

?>