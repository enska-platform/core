<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Interface
 * @package		core.engine.interfaces
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskaCloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Interfaces;

interface IModel
{
	Public
		/**
		 * Get the last model error
		 *
		 * @return	mixed
		 * @access	public
		 */
		Function getLastError();
		
		/**
		 * Get all errors triggered by child class or a specific error
		 *
		 * @param	string 	$err = null
		 * @return	array
		 * @access	public
		 */
		Function getErrors($err=null);
		
		/**
		 * Trigger a model error
		 *
		 * @param	mixed $data = null
		 * @return	void
		 * @access	public
		 */
		Function error($data = null);
		
		/**
		 * Set the model id
		 *
		 * @param	string id = null
		 * @return	void
		 * @access	public
		 */
		Function setId($id = null);
		
		/**
		 * Get the model id
		 *
		 * @return	string
		 * @access	public
		 */
		Function getId();
		
		/**
		 * Get the model store instance
		 *
		 * @return	DataStore
		 * @access	public
		 */
		Function store();
		
		/**
		 * Apply object datas to object store
		 * 
		 * @param	mixed $datas = null
		 * @return	string
		 * @access	public
		 */
		Function apply($datas = null);
}

?>