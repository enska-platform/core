<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Interface
 * @package		core.engine.interfaces
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.Enskacloud.com)
 * @license		http://www.Enskacloud.com/license
 */

namespace Enska\Interfaces;
 
interface IDataStore
{
	Public
		/**
		 * Magic alias for set
		 *
		 * @param	mixed $attr
		 * @param	mixed $value = null
		 * @return	IDataStore
		 * @access	public
		 */
		Function __set($attr, $value = null);

		/**
		 * Set / Edit a store item
		 *
		 * @param	mixed $attr
		 * @param	mixed $value = null
		 * @return	IDataStore
		 * @access	public
		 */
		Function set($attr, $value = null);
		
		/**
		 * Push an object in store
		 *
		 * @param	mixed $attr
		 * @return	IDataStore
		 * @access	public
		 */
		Function push($attr);

		/**
		 * Format a store
		 *
		 * @param	mixed $attr
		 * @return	IDataStore
		 * @access	public
		 */
		static Function create($attr);

		/**
		 * Set / Edit store items
		 *
		 * @param	array $datas
		 * @param	bool $clr = false
		 * @return	bool
		 * @access	public
		 */
		Function sets($datas, $clr = false);

		/**
		 * Magic alias for get
		 *
		 * @param	mixed $attr
		 * @return	mixed
		 * @access	public
		 */
		Function __get($attr);

		/**
		 * Get a store item
		 *
		 * @param	mixed $attr
		 * @return	mixed
		 * @access	public
		 */
		Function get($attr);

		/**
		 * Get store items
		 *
		 * @param	bool $jsEncode = false
		 * @return	array
		 * @access	public
		 */
		Function gets($jsEncode = false);

		/**
		 * Add store items
		 *
		 * @param	array $datas
		 * @param	int	$indexStart
		 * @return	DataStore
		 * @access	public
		 */
		Function adds($datas, $indexStart = 1);
		
		/**
		 * Delete a store item
		 *
		 * @param	mixed $attr
		 * @return	void
		 * @access	public
		 */
		Function delete($attr);

		/**
		 * Clear store items
		 *
		 * @return	void
		 * @access	public
		 */
		Function clear();

		/**
		 * Get store length
		 *
		 * @return	int
		 * @access	public
		 */
		Function count();
		
		/**
		 * Foreach iterator with callback
		 * 
		 * @param	Function $__callback($key = null, $value = null)
		 * @param	mixed	$obj = null
		 * @return	void
		 * @access	public
		 */
		Function each($__callback, $obj = null);

		/**
		 * Set the store id information
		 *
		 * @param	string $id
		 * @return	void
		 * @access	public
		 */
		Function setId($id);

		/**
		 * Get the store ID information
		 *
		 * @return	string
		 * @access	public
		 */
		Function getId();
		
		/**
		 * Get the latest error
		 * 
		 * @return	array
		 * @access	public
		 */
		Function getLastError();
		
		/**
		 * Set an error
		 * 
		 * @param	mixed $datas
		 * @return	void
		 * @access	public
		 */
		Function error($datas);
}

?>