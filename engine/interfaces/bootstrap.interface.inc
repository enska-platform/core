<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Interface
 * @package		core.engine.interfaces
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */
 
namespace Enska\Interfaces;

interface IBootstrap
{
	/**
	 * Bootstrap constante
	 * @type	int
	 */
	const 			_SELF = 0;
	
	/**
	 * Bootstrap constante
	 * @type	int
	 */
	const 			_ALL = 1;
	
	/**
	 * Bootstrap constante
	 * @type	int
	 */
	const			SUCCESS = 0;
	
	/**
	 * Bootstrap constante
	 * @type	int
	 */
	const			FAILED = 1;
	
	/**
	 * Bootstrap constante
	 * @type	int
	 */
	const 			_DEVT = 0;
	
	/**
	 * Bootstrap constante
	 * @type	int
	 */
	const 			_TEST = 1;
	
	/**
	 * Bootstrap constante
	 * @type	int
	 */
	const 			_PROD = 2;
	
	Public
		/**
		 * System powerOn
		 *
		 * @param	array 	$argv = null
		 * @return	void
		 * @access	public
		 */
		static Function powerOn($argv = null);
		
		/**
		 * System powerOff
		 *
		 * @return	void
		 * @access	public
		 */
		static Function powerOff();
		
		/**
		 * System start
		 *
		 * @return	IBootstrap
		 * @access	public
		 */
		static Function start();
		
		/**
		 * System load
		 *
		 * @return	IBootstrap
		 * @access	public
		 */
		Function load();
		
		/**
		 * System process
		 *
		 * @return	void
		 * @access	public
		 */
		static Function run();
}	

?>