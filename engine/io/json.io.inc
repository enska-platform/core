<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	IO
 * @package		core.engine.io
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\IO;
use \Enska\Abstracts\IO;
use \Enska\Helpers\DataStore;
use \Enska\Services\Utils;

class JSON extends IO
{	
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null;
		 * @param	array 	$datas = null;
		 * @return	IO
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
			if ($id) { $this->read(); }
		}
		
		/**
		 * Read from stream
		 * 
		 * @param	array option = null
		 * @return	mixed
		 * @access	public
		 */
		Function read($options=null)
		{
			if ($this->connector instanceof \Enska\Abstracts\Connector) {
				if ($this->connector->getLastError()) { $this->error($this->connector->getLastError()); return; }
				$this->stream = new DataStore($this->receive($options));
				return ($this->stream);
			}
		}
		
		/**
		 * Write pushed datas into stream
		 * 
		 * @param	array $options = null
		 * @return	mixed
		 * @access	public
		 */
		Function write($options=null)
		{	
			if (!Utils::isArray($options)) { $options = array('save' => true, 'force' => true, 'mode' => \Enska\Connectors\FS::_NEW); }
			$this->_buffer = $this->stream->gets(true);
			return ($this->send($options));
		}
		
		/**
		 * Delete source connector from disk
		 * 
		 * @return	void
		 * @access	public
		 */
		Function delete()
		{
			if ($this->connector instanceof \Enska\Abstracts\Connector && method_exists($this->connector, 'delete')) { $this->connector->delete(); }
		}
		
		/**
		 * Magic alias for get
		 *
		 * @param	mixed $attr
		 * @return	mixed
		 * @access	public
		 */
		Function __get($attr)
		{
			return ($this->stream->get($attr));
		}
		
		/**
		 * Magic alias for set
		 *
		 * @param	mixed $attr
		 * @param	mixed $value = null
		 * @return	DataStore
		 * @access	public
		 */
		Function __set($attr, $value=null)
		{
			return ($this->stream->set($attr, $value));
		}
		
		/**
		 * Apply an object
		 *
		 * @param	mixed $value
		 * @access	public
		 */
		Function applyObject($value)
		{
			return ($this->stream->apply($value));
		}
}

?>