<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	IO
 * @package		core.engine.io
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

namespace Enska\IO;
use \Enska\Abstracts\IO;

class RAW extends IO
{
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null;
		 * @param	array 	$datas = null;
		 * @return	IO
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			parent::__construct($id, $datas);
		}
		
		/**
		 * Read from stream
		 * 
		 * @param	mixed $options
		 * @return	mixed
		 * @access	public
		 */
		Function read($options=null)
		{
			return ($this->connector->receive($options));
		}
		
		/**
		 * Write pushed datas into stream
		 * 
		 * @param	array $options = null
		 * @return	IO
		 * @access	public
		 */
		Function write($options=null)
		{
			return ($this->send($options));
		}
}

?>