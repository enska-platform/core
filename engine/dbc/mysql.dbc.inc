<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud.
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	DBDriver
 * @package		core.engine.dbc
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 * @todo		Implements MySQL (with handler::__call)
 */
 
namespace Enska\Drivers;

use \Enska\Connectors\DB;
use \Enska\Services\Utils;
use \Enska\Helpers\DataStore;
use \Enska\Helpers\ORM\mysql\mysqlDb;
use \Enska\Helpers\ORM\mysql\mysqlTable;

class MysqlDriver extends DB
{
	/**
	 * mysqlDb instance
	 * @type	mysqlDb
	 */
	Private	$base = null;
	
	/**
	 * mysqlTable instance
	 * @type	mysqlTable
	 */
	Private	$table = null;
	
	/**
	 * Class DSN
	 * @type	string
	 */
	Protected	$dsn = 'mysql:host={{ server }};port={{ port }};dbname={{ name }}';
	
	Public
		/**
		 * Constructor
		 * 
		 * @param	string 	$id = null;
		 * @param	array 	$datas = null;
		 * @return	IO
		 * @access	public
		 */ 
		Function __construct($id=null, $datas=null)
		{
			$datas = DataStore::create($datas);
			$datas->dsn = $this->dsn;
			
			parent::__construct($id, $datas);
			$this->base = new mysqlDb();
			$this->table = new mysqlTable();
		}
		
		/**
		 * Query caller
		 * 
		 * @param	string	$method
		 * @param	array 	$argv = null
		 * @return	mixed
		 * @access	public
		 */
		Function __call($method, $argv=null)
		{
			if (method_exists($this, $method)) {
				if (isset($argv[0]) && !isset($argv[1])) {
					$query = $argv[0];
					return ($this->query($query));
				}
				elseif (isset($argv[0]) && isset($argv[1])) {
					$table = $argv[0];
					$query = $argv[1];
					return ($this->$method($table, $query));
				}
				$this->error('Error on query format');
			}
			else {
				$this->error('Driver method not exist');
			}
		}
		
	Private
		/**
		 * Do a DB QUERY
		 * 
		 * @param	string	$request
		 * @return 	mixed
		 * @access	private
		 */
		Function query($request)
		{
			$this->table->clean();
			$this->table->query($request);
			$this->error($this->table->getLastError());
			if (Utils::isEmpty($this->getLastError())) {
				try{
					$res = $this->send($this->table->getQueries());
					if (Utils::isEmpty($this->getLastError())) {
						return ($res);
					}
				}
				catch(\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			return (false);
		}
		
	Private
		/**
		 * Do an INSERT action
		 * 
		 * @param	array 	$argv = null
		 * @return 	bool
		 * @access	private
		 */
		Function insert($table, $request)
		{
			$this->table->clean();
			$this->table->insert($table, $request);
			$this->error($this->table->getLastError());
			if (Utils::isEmpty($this->getLastError())) {
				try{
					$res = $this->send($this->table->getQueries());
					if (Utils::isEmpty($this->getLastError())) {
						return ($res);
					}
				}
				catch(\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			return (false);
		}
		
	Private
		/**
		 * Do a SELECT action
		 * 
		 * @param	string	$table
		 * @param	string	$request
		 * @return 	mixed
		 * @access	private
		 */
		Function select($table, $request)
		{
			$this->table->clean();
			$result = null;
			$this->table->select($table, $request);
			$this->error($this->table->getLastError());
			if (Utils::isEmpty($this->getLastError())) {
				$result = new DataStore();
				if (Utils::isArray(($res = $this->send($this->table->getQueries())))) {
					if (count($res) > 1) {
						$result->sets($res, true);
					}
					else {
						$result->sets($res[1], true);
					}
				}
			}
			return ($result);
		}
		
	Private
		/**
		 * Do an UPDATE action
		 * 
		 * @param	string	$table
		 * @param	string	$request
		 * @return 	mixed
		 * @access	private
		 */
		Function update($table, $request)
		{
			$this->table->clean();
			$this->table->update($table, $request);
			$this->error($this->table->getLastError());
			if (Utils::isEmpty($this->getLastError())) {
				try{
					$this->send($this->table->getQueries());
					return (true);
				}
				catch(\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			return (false);
		}
		
	Private
		/**
		 * Do a DELETE action
		 * 
		 * @param	string	$table
		 * @param	string	$request
		 * @return 	mixed
		 * @access	private
		 */
		Function delete($table, $request)
		{
			$this->table->clean();
			$this->table->delete($table, $request);
			$this->error($this->table->getLastError());
			if (Utils::isEmpty($this->getLastError())) {
				try{
					$this->send($this->table->getQueries());
					return (true);
				}
				catch(\Exception $e) {
					$this->error($e->getMessage());
				}
			}
			return (false);
		}
}
