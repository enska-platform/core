<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/**
 * @category	Loader
 * @package		core
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */ 

namespace Enska;

#loader

DEFINE ('INCLUDE_PATH', DOCUMENTROOT);
set_include_path(INCLUDE_PATH);
include_once ('core.load.inc');
use \Enska\Bootstrap\Enska as Platform;

try { $exec = (!strcmp(BOOTSTRAP, 'HTTP')) ? Platform::powerOn() : Platform::powerOn($argv); }
catch(\Exception $e) { debug($e->getMessage()."\n"); $exit = Platform::Excep($e); }
Platform::terminate($exit);

?>
