# EnskaIO /core #


  What is it?
  ----------------------------------------------------------------------------------------

  EnskaIO is an open source SOA platform written in PHP5 for developing and deploying complex web applications.
  It deliver standalone cloud framework services (RealTimeEngineDaemon (websocket), REST architecture, MVC system, database ready).


  Current version
  ----------------------------------------------------------------------------------------

  This is version 1.3.1


  Documentation
  ----------------------------------------------------------------------------------------

  [http://docs.enska.io/core][1]


  Installation
  ----------------------------------------------------------------------------------------

  See the EnskaIO Platform repository
  

  Licensing
  ----------------------------------------------------------------------------------------

  Please see the file called LICENSE.


  Contact
  ----------------------------------------------------------------------------------------

  support@enska.io
  
  [1]:http://docs.enska.io/core
