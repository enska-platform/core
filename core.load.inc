<?php

/**
 *Copyright (C) 2008 - 2014 EnskaCloud
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @category	Loader
 * @package		core
 * @copyright	Copyright (c) 2008 - 2014 EnskaCloud. (http://www.enskacloud.com)
 * @license		http://www.enskacloud.com/license
 */

#@loader

namespace Enska;

use Enska\Services\Kernel;
use Enska\Services\Context;

/**
 * EnsKa global defines
 */
 
if (!defined('CORE_NAME')) { DEFINE ('CORE_NAME', 'core'); }
if (!defined('BOOTSTRAP')) { DEFINE ('BOOTSTRAP', 'HTTP'); }
if (!defined('DEFAULT_ID_SIZE')) { DEFINE ('DEFAULT_ID_SIZE', 10); }
if (!defined('CONF_PATH')) { DEFINE ('CONF_PATH', 'conf/'); }

DEFINE ('FAILED', 1);
DEFINE ('SUCCESS', 0);

DEFINE ('ENSKA_CORE', CORE_NAME.'/');
DEFINE ('ENSKA_ENGINE', ENSKA_CORE.'engine/');
DEFINE ('ENSKA_SERVICES', ENSKA_ENGINE.'services/');
DEFINE ('ENSKA_INTERFACES', ENSKA_ENGINE.'interfaces/');
DEFINE ('ENSKA_ABSTRACTS', ENSKA_ENGINE.'abstracts/');
DEFINE ('ENSKA_HELPERS', ENSKA_ENGINE.'helpers/');
DEFINE ('ENSKA_CONNECTORS', ENSKA_ENGINE.'connectors/');
DEFINE ('ENSKA_DBCONNECTORS', ENSKA_ENGINE.'dbc/');
DEFINE ('ENSKA_IO', ENSKA_ENGINE.'io/');
DEFINE ('ENSKA_EXCEPTIONS', ENSKA_ENGINE.'exceptions/');
DEFINE ('ENSKA_BOOTSTRAP', ENSKA_ENGINE.'/bootstrap/');
DEFINE ('ENSKA_TMP', CORE_NAME.'/system/tmp/');
DEFINE ('ENSKA_CONF', CONF_PATH);

/**
 * Define EnsKa extension names
 */
DEFINE ('INCLUDE_EXT', '.inc');
DEFINE ('ENSKA_ENGINE_EXT', '.class.inc');
DEFINE ('ENSKA_ABSTRACT_EXT', '.abstract.inc');
DEFINE ('ENSKA_INTERFACE_EXT', '.interface.inc');
DEFINE ('ENSKA_CONNECTOR_EXT', '.connector.inc');
DEFINE ('ENSKA_DBC_EXT', '.dbc.inc');
DEFINE ('ENSKA_IO_EXT', '.io.inc');
DEFINE ('ENSKA_EXCEPTION_EXT', '.exception.inc');
DEFINE ('ENSKA_SINGLETON_EXT', '.singleton.inc');
DEFINE ('ENSKA_SERVICE_EXT', '.service.inc');

/**
 * Libs
 */
$lib_exceptions =  array('exception', 'bootstrap', 'core', 'system', 'app', 'client');
$lib_helpers_head = array('debug', 'utils');
$lib_interfaces = array('dataStore', 'model', 'bootstrap');
$lib_helpers = array(
	'dataStore', 'handler', 'bootstrap', 'subject', 'observer', 'route', 'cmdp', 'login',
	'app', 'error', 'agent', 'dbObj', 'reponse', 'controller', 'client', 'sserver',
	'minimifiers/js', 'minimifiers/css', 'protocols/ertp', 'protocols/ws'
);
$lib_abstracts = array('connector', 'io', 'controller', 'sessions');
$lib_connectors = array('fs', 'cmd', 'ssh', 'db', 'ftp', 'sftp', 'wservice', 'socket', 'git', 'docker');
$lib_orm = array('orm/mysql/manager', 'orm/mysql/db', 'orm/mysql/table');
$lib_ios = array('raw', 'json');
$lib_services = array(
	'config', 'context', 'logs', 'imports', 'system', 'headers', 'sessions', 'auth',
	'db', 'acl', 'user', 'users', 'server', 'redis'
);

/**
 * Enska exception
 */
$len = count(($libs = $lib_exceptions));
for ($i = 0; $i < $len; $i++) { include_once (ENSKA_EXCEPTIONS.$libs[$i].ENSKA_EXCEPTION_EXT); }

/**
 * Utils helper
 */
$len = count(($libs = $lib_helpers_head));
for ($i = 0; $i < $len; $i++) { include_once(ENSKA_SERVICES.$libs[$i].ENSKA_SINGLETON_EXT); }

/**
 * Enska KERNEL
 */
include_once (ENSKA_INTERFACES.'system'.ENSKA_INTERFACE_EXT);
include_once (ENSKA_SERVICES.'kernel'.ENSKA_SINGLETON_EXT);	
Kernel::kernelTime();

/**
 * Enska interfaces
 */
Kernel::kernelTime('interface');
$len = count(($libs = $lib_interfaces));
for ($i = 0; $i < $len; $i++) { include_once (ENSKA_INTERFACES.$libs[$i].ENSKA_INTERFACE_EXT); }

/**
 * Enska helpers
 */
Kernel::kernelTime('helpers');
include_once (ENSKA_HELPERS.'model'.ENSKA_ENGINE_EXT);
include_once (ENSKA_ABSTRACTS.'observable'.ENSKA_ABSTRACT_EXT);
include_once (ENSKA_ABSTRACTS.'protocol'.ENSKA_ABSTRACT_EXT);
include_once (ENSKA_ABSTRACTS.'server'.ENSKA_ABSTRACT_EXT);
$len = count(($libs = $lib_helpers));
for ($i = 0; $i < $len; $i++) { include_once (ENSKA_HELPERS.$libs[$i].ENSKA_ENGINE_EXT); }

/**
 * Enska abstracts
 */
Kernel::kernelTime('abstracts');
$len = count(($libs = $lib_abstracts));
for ($i = 0; $i < $len; $i++) { include_once (ENSKA_ABSTRACTS.$libs[$i].ENSKA_ABSTRACT_EXT); }

/**
 * Enska connectors
 */
Kernel::kernelTime('connectors');
$len = count(($libs = $lib_connectors));
for ($i = 0; $i < $len; $i++) { include_once (ENSKA_CONNECTORS.$libs[$i].ENSKA_CONNECTOR_EXT); }

/**
 * Enska MySLQ ORM
 */
Kernel::kernelTime('orm');
include_once (ENSKA_DBCONNECTORS.'mysql'.ENSKA_DBC_EXT);
include_once (ENSKA_HELPERS.'dbSearch'.ENSKA_ENGINE_EXT);
$len = count(($libs = $lib_orm));
for ($i = 0; $i < $len; $i++) { include_once (ENSKA_HELPERS.$libs[$i].ENSKA_ENGINE_EXT); }

/**
 * Enska IO
 */
Kernel::kernelTime('io');
$len = count(($libs = $lib_ios));
for ($i = 0; $i < $len; $i++) { include_once (ENSKA_IO.$libs[$i].ENSKA_IO_EXT); }

/**
 * Enska bootstrap
 */
Kernel::kernelTime('bootstrap');
include_once (ENSKA_BOOTSTRAP.strtolower( BOOTSTRAP ).INCLUDE_EXT);

/**
 * Enska services
 */
Kernel::kernelTime('services');
$len = count(($libs = $lib_services));
for ($i = 0; $i < $len; $i++) { include_once (ENSKA_SERVICES.$libs[$i].ENSKA_SINGLETON_EXT); }

Kernel::kernelTime('__end');
Context::set('kernelTime_kernel', Kernel::kernelTime('__get'));
$exit = SUCCESS;

?>